<?php

/** ============================================================================
 * functions
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/16/19
 * Modified: 12/18/19 by SR
 * ========================================================================== */



/**
 * Child Theme constants
 * You can change below constants
 */

// white label

define('WHITE_LABEL', false);
define('INCLUDES_PATH', get_stylesheet_directory() . '/inc');
define('INCLUDES_PATH_URI', get_stylesheet_directory_uri() . '/inc');
define('CONFIG_PATH', get_stylesheet_directory() . '/config');
define('CONFIG_PATH_URI', get_stylesheet_directory_uri() . '/config');
define('ASSETS_PATH', get_stylesheet_directory() . '/assets');
define('ASSETS_PATH_URI', get_stylesheet_directory_uri() . '/assets');
define('THEME_CONFIG_PATH', CONFIG_PATH . '/theme-config.json');
define('THEME_CONFIG_PATH_URI', CONFIG_PATH_URI . '/theme-config.json');
define('COMPONENT_PATH', INCLUDES_PATH . '/components');
define('COMPONENT_PATH_URI', INCLUDES_PATH_URI . '/components');
define('UTILITIES_PATH', INCLUDES_PATH . '/utilities');
define('UTILITIES_PATH_URI', INCLUDES_PATH_URI . '/utilities');
define('ADMIN_PATH', INCLUDES_PATH . '/admin');
define('ADMIN_PATH_URI', INCLUDES_PATH_URI . '/admin');
define('PAGES_PATH', INCLUDES_PATH . '/pages');
define('PAGES_PATH_URI', INCLUDES_PATH_URI . '/pages');
define('PLUGINS_PATH', INCLUDES_PATH . '/plugins');
define('PLUGINS_PATH_URI', INCLUDES_PATH_URI . '/plugins');
define('THEME_SLUG', 'betheme');
define('THEME_PREFIX', 'kp');
define('YOUTUBE_KEY', 'AIzaSyAoig2BNilZxH7h-pLj5HhZbTAVuKfWqL4');
define('GRID_SM', '375');
define('GRID_MS', '576');
define('GRID_MD', '768');
define('GRID_ML', '1024');
define('GRID_LG', '1280');
define('GRID_XL', '1440');

/**
 * Enqueue Styles
 */

function mfnch_enqueue_styles()
{

	if (is_rtl()) {
		wp_enqueue_style('mfn-rtl', get_template_directory_uri() . '/rtl.css');
	}
}
add_action('wp_enqueue_scripts', 'mfnch_enqueue_styles', 999);

function mfnch_textdomain()
{
	load_child_theme_textdomain('betheme', get_stylesheet_directory() . '/languages');
	load_child_theme_textdomain('mfn-opts', get_stylesheet_directory() . '/languages');
}
add_action('after_setup_theme', 'mfnch_textdomain');

/**
 * Implement the Theme Utility functions.
 */

require_once UTILITIES_PATH . '/utilities.php';
require_once UTILITIES_PATH . '/walkers.php';
require_once UTILITIES_PATH . '/setup.php';
require_once UTILITIES_PATH . '/assets.php';
require_once ADMIN_PATH . '/setup.php';
require_once PAGES_PATH . '/setup.php';

/**
 * Load desired components into theme
 */

//$components = glob(COMPONENT_PATH . '/*', GLOB_ONLYDIR);
$components = array(
	'content',
	'schema',
	'blog',
	'nav',
	'header',
	'social',
	'hover-box',
	'video',
	'carousel'
);
foreach ($components as $component) {
	incComponent($component);
}

$plugins = glob(PLUGINS_PATH . '/*', GLOB_ONLYDIR);
foreach ($plugins as $plugin) {
	$path = $plugin . '/setup.php';
	if (file_exists($path)) {
		require_once $path;
	}
}
