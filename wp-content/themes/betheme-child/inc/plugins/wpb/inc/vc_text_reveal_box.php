<?php

/** ===========================================================================
 * Visual Composer Setup file for the Text Reveal Box Component
 * @package Text Reveal Box
 * @version <<version>>
 * @link http://www.wpelixir.com/how-to-create-new-element-in-visual-composer/
 * @uses WPBakeryShortCode
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * ========================================================================= */

class TextRevealBox extends WPBakeryShortCode
{

    // ---------------------------- Element Init ---------------------------- //

    function __construct()
    {
        add_action('init', array($this, 'vc_text_reveal_box_mapping'));
        add_shortcode('vc_text_reveal_box', array($this, 'vc_text_reveal_box_html'));
    }

    // --------------------------- Element Mapping -------------------------- //

    public function vc_text_reveal_box_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name'        => __('Text Reveal Box', THEME_SLUG),
                'base'        => 'vc_text_reveal_box',
                'description' => __('', THEME_SLUG),
                'category'    => __('KP Components', THEME_SLUG),
                'params'      => array(
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Element ID', THEME_SLUG),
                        'param_name'  => 'el_id',
                        'description' => sprintf(__('Enter element ID (Note: make sure it is unique and valid according to <a href="%s" target="_blank">w3c specification</a>).', THEME_SLUG), 'http://www.w3schools.com/tags/att_global_id.asp'),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Extra class name', THEME_SLUG),
                        'param_name'  => 'el_class',
                        'description' => __('Style particular content element differently - add a class name and refer to it in custom CSS.', THEME_SLUG),
                    ),
                    array(
                        'type'        => 'attach_image',
                        'heading'     => __('Background Image', THEME_SLUG),
                        'param_name'  => 'image',
                        'value'       => '',
                        'description' => __('Select image from media library.', THEME_SLUG),
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => __('Image Size', THEME_SLUG),
                        'param_name'  => 'image_size',
                        'value'       => array(
                            __('Full', THEME_SLUG)      => 'full',
                            __('Large', THEME_SLUG)   => 'large',
                            __('Medium', THEME_SLUG) => 'medium',
                            __('Thumbnail', THEME_SLUG)  => 'thumbnail',

                        ),
                        'description' => __('Choose a background color from the theme colors', THEME_SLUG),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Image Class', THEME_SLUG),
                        'param_name'  => 'image_class',
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => __('Background Color', THEME_SLUG),
                        'param_name'  => 'bg_color',
                        'value'       => array(
                            __('None', THEME_SLUG)      => '',
                            __('Primary', THEME_SLUG)   => 'bg-primary',
                            __('Secondary', THEME_SLUG) => 'bg-secondary',
                            __('Tertiary', THEME_SLUG)  => 'bg-tertiary',
                            __('Success', THEME_SLUG)   => 'bg-success',
                            __('Danger', THEME_SLUG)    => 'bg-danger',
                            __('Warning', THEME_SLUG)   => 'bg-warning',
                            __('Info', THEME_SLUG)      => 'bg-info',
                            __('Light', THEME_SLUG)     => 'bg-light',
                            __('Dark', THEME_SLUG)      => 'bg-dark',
                            __('Gray 100', THEME_SLUG)  => 'bg-gray-100',
                            __('Gray 200', THEME_SLUG)  => 'bg-gray-200',
                            __('Gray 300', THEME_SLUG)  => 'bg-gray-300',
                            __('Gray 400', THEME_SLUG)  => 'bg-gray-400',
                            __('Gray 500', THEME_SLUG)  => 'bg-gray-500',
                            __('Gray 600', THEME_SLUG)  => 'bg-gray-600',
                            __('Gray 700', THEME_SLUG)  => 'bg-gray-700',
                            __('Gray 800', THEME_SLUG)  => 'bg-gray-800',
                            __('Gray 900', THEME_SLUG)  => 'bg-gray-900',
                            __('Custom', THEME_SLUG)    => 'custom',

                        ),
                        'description' => __('Choose a background color from the theme colors', THEME_SLUG),
                    ),
                    array(
                        'type'        => 'dropdown',
                        'heading'     => __('Hover Effect', THEME_SLUG),
                        'param_name'  => 'hover_effect',
                        'value'       => array(
                            __('Lily', THEME_SLUG)    => 'effect-lily',
                            __('Sadie', THEME_SLUG)   => 'effect-sadie',
                            __('Roxy', THEME_SLUG)    => 'effect-roxy',
                            __('Bubba', THEME_SLUG)   => 'effect-bubba',
                            __('Romeo', THEME_SLUG)   => 'effect-romeo',
                            __('Layla', THEME_SLUG)   => 'effect-layla',
                            __('Honey', THEME_SLUG)   => 'effect-honey',
                            __('Oscar', THEME_SLUG)   => 'effect-oscar',
                            __('Marley', THEME_SLUG)  => 'effect-marley',
                            __('Ruby', THEME_SLUG)    => 'effect-ruby',
                            __('Milo', THEME_SLUG)    => 'effect-milo',
                            __('Dexter', THEME_SLUG)  => 'effect-dexter',
                            __('Sarah', THEME_SLUG)   => 'effect-sarah',
                            __('Zoe', THEME_SLUG)     => 'effect-zoe',
                            __('Chico', THEME_SLUG)   => 'effect-chico',
                            __('Julia', THEME_SLUG)   => 'effect-julia',
                            __('Goliath', THEME_SLUG) => 'effect-goliath',
                            __('Hera', THEME_SLUG)    => 'effect-hera',
                            __('Winston', THEME_SLUG) => 'effect-winston',
                            __('Selena', THEME_SLUG)  => 'effect-selena',
                            __('Terry', THEME_SLUG)   => 'effect-terry',
                            __('Duke', THEME_SLUG)    => 'effect-duke',
                            __('Phoebe', THEME_SLUG)  => 'effect-phoebe',
                            __('Apollo', THEME_SLUG)  => 'effect-apollo',
                            __('Kira', THEME_SLUG)    => 'effect-kira',
                            __('Steve', THEME_SLUG)   => 'effect-steve',
                            __('Moses', THEME_SLUG)   => 'effect-moses',
                            __('Jazz', THEME_SLUG)    => 'effect-jazz',
                            __('Ming', THEME_SLUG)    => 'effect-ming',
                            __('Lexi', THEME_SLUG)    => 'effect-lexi',
                        ),
                        'description' => __('Choose an effect type. See https://tympanus.net/Development/HoverEffectIdeas/', THEME_SLUG),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => __('Title Tag', THEME_SLUG),
                        'param_name' => 'title_tag',
                        'value'      => array(
                            __('Paragraph', THEME_SLUG) => 'p',
                            __('Heading 1', THEME_SLUG) => 'h1',
                            __('Heading 2', THEME_SLUG) => 'h2',
                            __('Heading 3', THEME_SLUG) => 'h3',
                            __('Heading 4', THEME_SLUG) => 'h4',
                            __('Heading 5', THEME_SLUG) => 'h5',
                            __('Heading 6', THEME_SLUG) => 'h6',

                        ),
                    ),
                    array(
                        'type'       => 'textfield',
                        'heading'    => __('Box Title', THEME_SLUG),
                        'param_name' => 'title',
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Box Title Class', THEME_SLUG),
                        'param_name'  => 'title_class',
                    ),
                    array(
                        'type'       => 'textarea_html',
                        'heading'    => __('Box Text', THEME_SLUG),
                        'param_name' => 'content',
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Box Content Class', THEME_SLUG),
                        'param_name'  => 'content_class',
                    ),
                    array(
                        'type'       => 'textfield',
                        'heading'    => __('Box Icon', THEME_SLUG),
                        'param_name' => 'icon',
                    ),
                    array(
                        'type'       => 'vc_link',
                        'heading'    => __('Box Link', THEME_SLUG),
                        'param_name' => 'link',
                    ),
                    vc_map_add_css_animation(),
                ),
            )
        );
    }

    // ---------------------------- Element HTML ---------------------------- //

    public function vc_text_reveal_box_html($atts, $content)
    {
        // Params extraction
        $vc_elem = vc_map_get_defaults('vc_text_reveal_box');
        extract(
            shortcode_atts(
                $vc_elem,
                $atts
            )
        );

        // ----------------------- Element Wrapper ---------------------- //

        $wrapper_atts = array();
        if (!empty($el_id)) {
            $wrapper_atts['id'] = $el_id;
        }

        $wrapper_classes = array(
            'wpb_content_element',
            'text-reveal-box',
            $bg_color
        );

        if (!empty($el_class)) {
            $wrapper_classes = outputClasses($wrapper_classes, $el_class);
        }

        if (!empty($css_animation)) {
            $wrapper_classes = outputClasses($wrapper_classes, $css_animation);
        }

        $wrapper_vc_classes = vcClasses($wrapper_classes, 'vc_text_reveal_box', $atts);
        $wrapper_classes = outputClasses($wrapper_classes, $wrapper_vc_classes);
        $wrapper_atts['class'] = $wrapper_classes;

        // ----------------------- Element Content ---------------------- //

        $link = vc_build_link($link);

        $link['title'] = $title;

        if (isset($link['url'])) {
            $link['href'] = $link['url'];
            unset($link['url']);
        }

        $content = wpb_js_remove_wpautop($content, true);

        ob_start();

        echo $content;

        $content = ob_get_clean();

        $args = array(
            'title'     => array(
                'content' => $title,
                'tag'   => $title_tag,
                'class' => $title_class
            ),
            'subtitle' => array(
                'content' => $content,
                'tag'   => 'div',
                'class' => $content_class
            ),
            'icon'           => $icon,
            'link'           => $link,
            'image'     => array(
                'id' => $image,
                'size'     => $image_size,
                'class' => $image_class
            ),
            'hover_effect'        => $hover_effect,
            'atts'                => $wrapper_atts
        );

        // ----------------------- Element Output ----------------------- //

        $output = hoverBox($args);
        return $output;
    }
} // End Element Class

// Element Class Init
new TextRevealBox();
