<?php

/** ============================================================================
 * Functions to Customie Visual Composer
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/12/19
 * Modified: 01/09/20 by SR
 * ========================================================================== */


/**
 * Convert Column Width to Class
 *
 * @param string $width
 *
 * @return string
 */
function vcColWidthToSpan($width)
{
    $output = $width;
    preg_match('/(\d+)\/(\d+)/', $width, $matches);
    $dbp = themeConfig('desktop-bp');
    if (!empty($matches)) {
        $part_x = (int) $matches[1];
        $part_y = (int) $matches[2];
        if ($part_x > 0 && $part_y > 0) {
            $value = ceil($part_x / $part_y * 12);
            if ($value > 0 && $value <= 12) {
                $output = 'col-' . $dbp . '-' . $value;
            }
        }
    }
    if (preg_match('/\d+\/5$/', $width)) {
        $part_x = (int) $matches[1];
        $part_y = (int) $matches[2];
        if ($part_x > 0 && $part_y > 0) {
            $value = ceil($part_x / $part_y * 100);
            if ($value > 0 && $value <= 100) {
                $output = 'col-' . $dbp . '-' . $value;
            }
        }
    }

    return apply_filters('vc_translate_column_width_class', $output, $width);
}

function vcColOffsetClassMerge($column_offset, $width)
{
    // Remove offset settings if
    if ('1' === vc_settings()->get('not_responsive_css')) {
        $column_offset = preg_replace('/col\-(lg|md|xs)[^\s]*/', '', $column_offset);
    }
    if (preg_match('/col\-sm\-\d+/', $column_offset)) {
        return $column_offset;
    }

    return $width . (empty($column_offset) ? '' : ' ' . $column_offset);
}

/**
 * Update VC Shortcode Attributes
 *
 * @param string $elem
 * @param array $updated_atts
 * @link https://kb.wpbakery.com/docs/developers-how-tos/change-default-value-of-param/
 * @return void
 */
function vcUpdatedAtts($elem, $updated_atts = '')
{
    if (!empty($updated_atts)) {
        if (function_exists('vc_update_shortcode_param')) {
            foreach ($updated_atts as $att) {
                vc_update_shortcode_param($elem, $att);
            }
        }
    }
}

/**
 * Remove VC Shortcode Attributes
 *
 * @param string $elem
 * @param array $remove_atts
 * @link https://kb.wpbakery.com/docs/inner-api/vc_remove_param/
 * @return void
 */
function vcRemoveAtts($elem, $remove_atts = '')
{
    if (!empty($remove_atts)) {
        if (function_exists('vc_remove_param')) {
            foreach ($remove_atts as $att) {
                vc_remove_param($elem, $att);
            }
        }
    }
}
/**
 * Add VC Shortdcode Attributes
 *
 * @param string $elem
 * @param array $add_atts
 * @link https://kb.wpbakery.com/docs/inner-api/vc_add_param/
 * @return void
 */
function vcAddAtts($elem, $add_atts = '')
{
    if (!empty($add_atts)) {
        if (function_exists('vc_add_param')) {
            foreach ($add_atts as $att) {
                vc_add_param($elem, $att);
            }
        }
    }
}

/**
 * Generate Visual Composer Classes
 *
 * @param string|array $css_classes
 * @param string $base
 * @param array $atts
 *
 * @uses WPBakeryShortCode
 *
 * @return string
 */

function vcClasses($css_classes, $base, $atts)
{
    $classes = '';
    if (!empty($css_classes)) {
        $css_class = preg_replace(
            '/\s+/',
            ' ',
            apply_filters(
                VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG,
                implode(
                    ' ',
                    array_filter(
                        array_unique($css_classes)
                    )
                ),
                $base,
                $atts
            )
        );
        $classes = explode(' ', $css_class);
    }
    return $classes;
}

/**
 * Extract Image ID from Visual Composer CSS
 *
 * @param string $css
 *
 * @return int
 */
function vcGetBGImageID($css)
{
    $img_id = '';
    if (vc_shortcode_custom_css_has_property($css, array('background', 'background-image'))) {
        // extract url from css
        $css_url = '/url\(([^)]+)\)(\s\!important)?/m';

        preg_match_all($css_url, $css, $bg_urls, PREG_SET_ORDER, 0);

        // Make sure it's a plain url (no query)
        $img_url = http_strip_query_param($bg_urls[0][1]);

        $img_id = attachment_url_to_postid($img_url);
    }
    return $img_id;
}


/**
 * Remove background images from the css
 *
 * Remove the images from the css so they can be lazyloaded instead
 *
 * @param string $css
 *
 * @return string $css_class
 */
function vcCustomClass($css)
{
    if (vc_shortcode_custom_css_has_property($css, array('background', 'background-image'))) {
        $pattern = array(
            '/((background-image\:\s?url)\(([^)]+)\)(\s\!important)?\;)/',
            '/url\(([^)]+)\)(\s\!important)?/'
        );
        $replace = array('', '');
        $css = preg_replace($pattern, $replace, $css);
    }
    $css_class = vc_shortcode_custom_css_class($css);
    return $css_class;
}

function vcColNum($class = '')
{
    $class = preg_replace('/\D+/', '', $class);

    $bs_cols = array(
        '1'  => 12,
        '2'  => 6,
        '20' => 5,
        '3'  => 4,
        '4'  => 3,
        '40' => 3,
        '5'  => 2,
        '6'  => 2,
        '7'  => 2,
        '60' => 2,
        '8'  => 1,
        '9'  => 1,
        '10' => 1,
        '11' => 1,
        '12' => 1,
    );

    if (array_key_exists($class, $bs_cols)) {
        $cols = $bs_cols[$class];
        return $cols;
    } else {
        return NULL;
    }
}

/**
 * Function to make wrap content in the same structure as other Visual Composer
 * Items
 *
 * @param string $content
 * @param array $args
 *
 * @return string $item the HTML element
 */
function vcItem($content = '', $args = array())
{
    if (empty($content)) {
        return;
    }
    $defaults = array(
        'tag'   => 'div',
        'id'    => '',
        'class' => 'wpb_content_element',
        'style' => ''
    );

    $atts = parseArgs($defaults, $args);
    $item = itemWrapperHTML($content, array('class' => 'wpb_wrapper'));
    $item = itemWrapperHTML($item, $atts);

    return $item;
}

/**
 * Function to make wrap content in the same structure as other Visual Composer
 * Columns
 *
 * @param string $content
 * @param array $args
 *
 * @return string $col the HTML column
 */
function vcCol($content = '', $args = array())
{
    $defaults = array(
        'column' => array(
            'tag'   => 'div',
            'id'    => '',
            'class' => array(
                'wpb_column',
                'vc_column_container'
            ),
            'style' => ''
        ),
        'content_wrapper' => array(
            'tag'   => 'div',
            'id'    => '',
            'class' => 'vc_column-inner',
            'style' => ''
        ),
        'wpb_wrapper' => array(
            'tag'   => 'div',
            'id'    => '',
            'class' => 'wpb_wrapper',
            'style' => ''
        ),
        'bg_img' => array(
            'id' => '',
            'size' => 'full',
            'attr' => array(),
            'bg' => true,
        ),
    );

    $options = parseArgs($defaults, $args);
    extract($options);

    extract($bg_img);

    $col = '';

    $col = itemWrapperHTML($content, $wpb_wrapper);
    $col = itemWrapperHTML($col, $content_wrapper);
    if (!empty($bg_img['id'])) {
        $size = $attr = '';
        if (isset($bg_img['size']) && !empty($bg_img['size'])) {
            $size = $bg_img['size'];
        }
        if (isset($bg_img['attr']) && !empty($bg_img['attr'])) {
            $attr = $bg_img['attr'];
        }
        $img = displayImage($bg_img['id'], $size, $attr, true);
        $col = $img . $col;
    }
    $col = itemWrapperHTML($col, $column);

    return $col;
}

/**
 * Function to make wrap content in the same structure as other Visual Composer
 * Rows
 *
 * @param string $content
 * @param array $args
 *
 * @return string $row the HTML row
 */
function vcRow($content = '', $args = array())
{
    $defaults = array(
        'container' => array(
            'tag'   => 'div',
            'id'    => '',
            'class' => 'container',
            'style' => ''
        ),
        'row' => array(
            'tag'   => 'div',
            'id'    => '',
            'class' => 'vc_row',
            'style' => ''
        ),
        'bg_img' => array(
            'id' => '',
            'size' => 'full',
            'attr' => array(),
            'bg' => true,
        ),
    );

    $options = parseArgs($defaults, $args);
    extract($options);
    extract($bg_img);
    $row = itemWrapperHTML($content, $row);
    if (!empty($bg_img['id'])) {
        $size = $attr = '';
        if (isset($bg_img['size']) && !empty($bg_img['size'])) {
            $size = $bg_img['size'];
        }
        if (isset($bg_img['attr']) && !empty($bg_img['attr'])) {
            $attr = $bg_img['attr'];
        }
        $img = displayImage($bg_img['id'], $size, $attr, true);
        $row = $img . $row;
    }

    $row = itemWrapperHTML($row, $container);

    return $row;
}

/**
 * Function to make wrap content in the same structure as other Visual Composer
 * Sections
 *
 * @param string $content
 * @param array $args
 *
 * @return string $section the HTML section
 */
function vcSection($content = '', $args = array())
{
    $defaults = array(
        'outer' => array(
            'tag'   => 'section',
            'id'    => '',
            'class' => 'vc_section',
            'style' => '',
        ),
        'inner' => array(
            'tag'   => 'div',
            'id'    => '',
            'class' => 'vc_section_inner',
            'style' => '',
        ),
        'bg_img' => array(
            'id' => '',
            'size' => 'full',
            'attr' => array(),
            'bg' => true,
        ),
    );

    $options = parseArgs($defaults, $args);
    extract($options);
    $section = itemWrapperHTML($content, $inner);
    if (!empty($bg_img['id'])) {
        $size = $attr = '';
        if (isset($bg_img['size']) && !empty($bg_img['size'])) {
            $size = $bg_img['size'];
        }
        if (isset($bg_img['attr']) && !empty($bg_img['attr'])) {
            $attr = $bg_img['attr'];
        }
        $img = displayImage($bg_img['id'], $size, $attr, true);
        $section = $img . $section;
    }
    $section = itemWrapperHTML($section, $outer);

    return $section;
}
