<?php

/** ===========================================================================
 * Visual Composer Setup file for the Blog Grid Component
 * @package Blog Grid
 * @version <<version>>
 * @link http://www.wpelixir.com/how-to-create-new-element-in-visual-composer/
 * @uses WPBakeryShortCode
 * -----
 * @author Sam Rankin <you@you.you>
 * @copyright Copyright (c) 2019 Maat Legal
 * -----
 * ========================================================================= */

class bpBlogGrid extends WPBakeryShortCode
{

    // Element Init
    function __construct()
    {
        add_action('init', array($this, 'vc_blog_grid_bp_mapping'));
        add_shortcode('vc_blog_grid_bp', array($this, 'vc_blog_grid_bp_html'));
    }

    // Element Mapping
    public function vc_blog_grid_bp_mapping()
    {

        // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }

        // Map the block with vc_map()
        vc_map(
            array(
                'name'        => __('Blog Grid', THEME_SLUG),
                'base'        => 'vc_blog_grid_bp',
                'description' => __('', THEME_SLUG),
                'params'      => array(
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Element ID', THEME_SLUG),
                        'param_name'  => 'el_id',
                        'description' => sprintf(__('Enter element ID (Note: make sure it is unique and valid according to <a href="%s" target="_blank">w3c specification</a>).', THEME_SLUG), 'http://www.w3schools.com/tags/att_global_id.asp'),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Grid Wrapper Class', THEME_SLUG),
                        'param_name'  => 'el_class',
                        'description' => __('Style particular content element differently - add a class name and refer to it in custom CSS.', THEME_SLUG),
                    ),
                    array(
                        'type'        => 'loop',
                        'heading'     => __('Blog Loop', THEME_SLUG),
                        'param_name'  => 'blog_loop',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __('Grid Type', THEME_SLUG),
                        'param_name' => 'grid_type',
                        'value' => array(
                            __('List',   THEME_SLUG)       => 'list',
                            __('Equal Grid', THEME_SLUG)   => 'grid',
                            __('Masonry Grid', THEME_SLUG) => 'masonry',
                            __('Carousel', THEME_SLUG)     => 'carousel',
                        ),
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __('Remove Gutters?', THEME_SLUG),
                        'param_name' => 'remove_gutters',
                        'value' => array(__('Yes', THEME_SLUG) => 'yes'),
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __('Loop the Carousel', THEME_SLUG),
                        'param_name' => 'loop_carousel',
                        'value' => array(__('Yes', THEME_SLUG) => 'yes'),
                        'dependency' => array(
                            'element' => 'grid_type',
                            'value' => array('carousel'),
                        ),
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __('Add arrow nav', THEME_SLUG),
                        'param_name' => 'arrow_nav',
                        'value' => array(__('Yes', THEME_SLUG) => 'yes'),
                        'dependency' => array(
                            'element' => 'grid_type',
                            'value' => array('carousel'),
                        ),
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __('Add Dots Nav', THEME_SLUG),
                        'param_name' => 'dots_nav',
                        'value' => array(__('Yes', THEME_SLUG) => 'yes'),
                        'dependency' => array(
                            'element' => 'grid_type',
                            'value' => array('carousel'),
                        ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __('Column Span on Mobile Portrait', THEME_SLUG),
                        'param_name' => 'col_w_sm',
                        'value' => array(
                            __('Default',   THEME_SLUG)                => '',
                            __('Auto',      THEME_SLUG)                => 'col-sm-auto',
                            __('12 columns or full width', THEME_SLUG) => 'col-sm-12',
                            __('11 columns', THEME_SLUG)               => 'col-sm-11',
                            __('10 columns or 5/6', THEME_SLUG)        => 'col-sm-10',
                            __('80% column',  THEME_SLUG)              => 'col-sm-80',
                            __('9 columns or 3/4', THEME_SLUG)         => 'col-sm-9',
                            __('8 columns or 2/3', THEME_SLUG)         => 'col-sm-8',
                            __('60% column',  THEME_SLUG)              => 'col-sm-60',
                            __('7 columns', THEME_SLUG)                => 'col-sm-7',
                            __('6 columns or 1/2', THEME_SLUG)         => 'col-sm-6',
                            __('5 columns', THEME_SLUG)                => 'col-sm-5',
                            __('40% column',  THEME_SLUG)              => 'col-sm-40',
                            __('4 columns or 1/3', THEME_SLUG)         => 'col-sm-4',
                            __('3 columns or 1/4', THEME_SLUG)         => 'col-sm-3',
                            __('20% column',  THEME_SLUG)              => 'col-sm-20',
                            __('2 columns or 1/6', THEME_SLUG)         => 'col-sm-2',
                            __('1 column',  THEME_SLUG)                => 'col-sm-1',
                        ),
                        'description' => __('Choose the number of columns to display on devices 320px and up', THEME_SLUG),
                        'dependency' => array(
                            'element' => 'grid_type',
                            'value_not_equal_to' => array('list'),
                        ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __('Column Span on Mobile Landscape', THEME_SLUG),
                        'param_name' => 'col_w_ms',
                        'value' => array(
                            __('Default',   THEME_SLUG)                => '',
                            __('Auto',      THEME_SLUG)                => 'col-ms-auto',
                            __('12 columns or full width', THEME_SLUG) => 'col-ms-12',
                            __('11 columns', THEME_SLUG)               => 'col-ms-11',
                            __('10 columns or 5/6', THEME_SLUG)        => 'col-ms-10',
                            __('80% column',  THEME_SLUG)              => 'col-ms-80',
                            __('9 columns or 3/4', THEME_SLUG)         => 'col-ms-9',
                            __('8 columns or 2/3', THEME_SLUG)         => 'col-ms-8',
                            __('60% column',  THEME_SLUG)              => 'col-ms-60',
                            __('7 columns', THEME_SLUG)                => 'col-ms-7',
                            __('6 columns or 1/2', THEME_SLUG)         => 'col-ms-6',
                            __('5 columns', THEME_SLUG)                => 'col-ms-5',
                            __('40% column',  THEME_SLUG)              => 'col-ms-40',
                            __('4 columns or 1/3', THEME_SLUG)         => 'col-ms-4',
                            __('3 columns or 1/4', THEME_SLUG)         => 'col-ms-3',
                            __('20% column',  THEME_SLUG)              => 'col-ms-20',
                            __('2 columns or 1/6', THEME_SLUG)         => 'col-ms-2',
                            __('1 column',  THEME_SLUG)                => 'col-ms-1',
                        ),
                        'description' => __('Choose the number of columns to display on devices 576px and up', THEME_SLUG),
                        'dependency' => array(
                            'element' => 'grid_type',
                            'value_not_equal_to' => array('list'),
                        ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __('Column Span on Tablet Portrait', THEME_SLUG),
                        'param_name' => 'col_w_md',
                        'value' => array(
                            __('Default',   THEME_SLUG)                => '',
                            __('Auto',      THEME_SLUG)                => 'col-md-auto',
                            __('12 columns or full width', THEME_SLUG) => 'col-md-12',
                            __('11 columns', THEME_SLUG)               => 'col-md-11',
                            __('10 columns or 5/6', THEME_SLUG)        => 'col-md-10',
                            __('80% column',  THEME_SLUG)              => 'col-md-80',
                            __('9 columns or 3/4', THEME_SLUG)         => 'col-md-9',
                            __('8 columns or 2/3', THEME_SLUG)         => 'col-md-8',
                            __('60% column',  THEME_SLUG)              => 'col-md-60',
                            __('7 columns', THEME_SLUG)                => 'col-md-7',
                            __('6 columns or 1/2', THEME_SLUG)         => 'col-md-6',
                            __('5 columns', THEME_SLUG)                => 'col-md-5',
                            __('40% column',  THEME_SLUG)              => 'col-md-40',
                            __('4 columns or 1/3', THEME_SLUG)         => 'col-md-4',
                            __('3 columns or 1/4', THEME_SLUG)         => 'col-md-3',
                            __('20% column',  THEME_SLUG)              => 'col-md-20',
                            __('2 columns or 1/6', THEME_SLUG)         => 'col-md-2',
                            __('1 column',  THEME_SLUG)                => 'col-md-1',
                        ),
                        'description' => __('Choose the number of columns to display on devices 768px and up', THEME_SLUG),
                        'dependency' => array(
                            'element' => 'grid_type',
                            'value_not_equal_to' => array('list'),
                        ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __('Column Span on Tablet Landscape', THEME_SLUG),
                        'param_name' => 'col_w_ml',
                        'value' => array(
                            __('Default',   THEME_SLUG)                => '',
                            __('Auto',      THEME_SLUG)                => 'col-ml-auto',
                            __('12 columns or full width', THEME_SLUG) => 'col-ml-12',
                            __('11 columns', THEME_SLUG)               => 'col-ml-11',
                            __('10 columns or 5/6', THEME_SLUG)        => 'col-ml-10',
                            __('80% column',  THEME_SLUG)              => 'col-ml-80',
                            __('9 columns or 3/4', THEME_SLUG)         => 'col-ml-9',
                            __('8 columns or 2/3', THEME_SLUG)         => 'col-ml-8',
                            __('60% column',  THEME_SLUG)              => 'col-ml-60',
                            __('7 columns', THEME_SLUG)                => 'col-ml-7',
                            __('6 columns or 1/2', THEME_SLUG)         => 'col-ml-6',
                            __('5 columns', THEME_SLUG)                => 'col-ml-5',
                            __('40% column',  THEME_SLUG)              => 'col-ml-40',
                            __('4 columns or 1/3', THEME_SLUG)         => 'col-ml-4',
                            __('3 columns or 1/4', THEME_SLUG)         => 'col-ml-3',
                            __('20% column',  THEME_SLUG)              => 'col-ml-20',
                            __('2 columns or 1/6', THEME_SLUG)         => 'col-ml-2',
                            __('1 column',  THEME_SLUG)                => 'col-ml-1',
                        ),
                        'description' => __('Choose the number of columns to display on devices 1024px and up', THEME_SLUG),
                        'dependency' => array(
                            'element' => 'grid_type',
                            'value_not_equal_to' => array('list'),
                        ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __('Number of Columns on Small Desktops', THEME_SLUG),
                        'param_name' => 'col_w_lg',
                        'value' => array(
                            __('Default',   THEME_SLUG)                => '',
                            __('Auto',      THEME_SLUG)                => 'col-lg-auto',
                            __('12 columns or full width', THEME_SLUG) => 'col-lg-12',
                            __('11 columns', THEME_SLUG)               => 'col-lg-11',
                            __('10 columns or 5/6', THEME_SLUG)        => 'col-lg-10',
                            __('80% column',  THEME_SLUG)              => 'col-lg-80',
                            __('9 columns or 3/4', THEME_SLUG)         => 'col-lg-9',
                            __('8 columns or 2/3', THEME_SLUG)         => 'col-lg-8',
                            __('60% column',  THEME_SLUG)              => 'col-lg-60',
                            __('7 columns', THEME_SLUG)                => 'col-lg-7',
                            __('6 columns or 1/2', THEME_SLUG)         => 'col-lg-6',
                            __('5 columns', THEME_SLUG)                => 'col-lg-5',
                            __('40% column',  THEME_SLUG)              => 'col-lg-40',
                            __('4 columns or 1/3', THEME_SLUG)         => 'col-lg-4',
                            __('3 columns or 1/4', THEME_SLUG)         => 'col-lg-3',
                            __('20% column',  THEME_SLUG)              => 'col-lg-20',
                            __('2 columns or 1/6', THEME_SLUG)         => 'col-lg-2',
                            __('1 column',  THEME_SLUG)                => 'col-lg-1',
                        ),
                        'description' => __('Choose the number of columns to display on devices 1280px and up', THEME_SLUG),
                        'dependency' => array(
                            'element' => 'grid_type',
                            'value_not_equal_to' => array('list'),
                        ),
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => __('Column Span on Large Desktop', THEME_SLUG),
                        'param_name' => 'col_w_xl',
                        'value' => array(
                            __('Default',   THEME_SLUG)                => '',
                            __('Auto',      THEME_SLUG)                => 'col-xl-auto',
                            __('12 columns or full width', THEME_SLUG) => 'col-xl-12',
                            __('11 columns', THEME_SLUG)               => 'col-xl-11',
                            __('10 columns or 5/6', THEME_SLUG)        => 'col-xl-10',
                            __('80% column',  THEME_SLUG)              => 'col-xl-80',
                            __('9 columns or 3/4', THEME_SLUG)         => 'col-xl-9',
                            __('8 columns or 2/3', THEME_SLUG)         => 'col-xl-8',
                            __('60% column',  THEME_SLUG)              => 'col-xl-60',
                            __('7 columns', THEME_SLUG)                => 'col-xl-7',
                            __('6 columns or 1/2', THEME_SLUG)         => 'col-xl-6',
                            __('5 columns', THEME_SLUG)                => 'col-xl-5',
                            __('40% column',  THEME_SLUG)              => 'col-xl-40',
                            __('4 columns or 1/3', THEME_SLUG)         => 'col-xl-4',
                            __('3 columns or 1/4', THEME_SLUG)         => 'col-xl-3',
                            __('20% column',  THEME_SLUG)              => 'col-xl-20',
                            __('2 columns or 1/6', THEME_SLUG)         => 'col-xl-2',
                            __('1 column',  THEME_SLUG)                => 'col-xl-1',
                        ),
                        'description' => __('Choose the number of columns to display on devices 1440px and up', THEME_SLUG),
                        'dependency' => array(
                            'element' => 'grid_type',
                            'value_not_equal_to' => array('list'),
                        ),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Item Wrapper Classes', THEME_SLUG),
                        'param_name'  => 'wrapper_classes',
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Item Body Classes', THEME_SLUG),
                        'param_name'  => 'body_classes',
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __('Display Featured Image', THEME_SLUG),
                        'param_name' => 'add_image',
                        'value' => array(__('Yes', THEME_SLUG) => 'yes'),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Image Wrapper Classes', THEME_SLUG),
                        'param_name'  => 'image_wrapper_classes',
                        'dependency' => array(
                            'element' => 'add_image',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Image Classes', THEME_SLUG),
                        'param_name'  => 'image_classes',
                        'dependency' => array(
                            'element' => 'add_image',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Image Size', THEME_SLUG),
                        'param_name'  => 'image_size',
                        'dependency' => array(
                            'element' => 'add_image',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __('Display Post Title', THEME_SLUG),
                        'param_name' => 'add_title',
                        'value' => array(__('Yes', THEME_SLUG) => 'yes'),
                    ),
                    array(
                        'type'       => 'dropdown',
                        'heading'    => __('Title Tag', THEME_SLUG),
                        'param_name' => 'title_tag',
                        'value'      => array(
                            __('Paragraph', THEME_SLUG) => 'p',
                            __('Heading 1', THEME_SLUG) => 'h1',
                            __('Heading 2', THEME_SLUG) => 'h2',
                            __('Heading 3', THEME_SLUG) => 'h3',
                            __('Heading 4', THEME_SLUG) => 'h4',
                            __('Heading 5', THEME_SLUG) => 'h5',
                            __('Heading 6', THEME_SLUG) => 'h6',

                        ),
                        'dependency' => array(
                            'element' => 'add_title',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        'type'        => 'textarea_raw_html',
                        'heading'     => __('Text Before Title', THEME_SLUG),
                        'param_name'  => 'before_title',
                        'dependency' => array(
                            'element' => 'add_title',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        'type'        => 'textarea_raw_html',
                        'heading'     => __('Text After Title', THEME_SLUG),
                        'param_name'  => 'after_title',
                        'dependency' => array(
                            'element' => 'add_title',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Post Title Classes', THEME_SLUG),
                        'param_name'  => 'title_classes',
                        'dependency' => array(
                            'element' => 'add_title',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __('Display Excerpt', THEME_SLUG),
                        'param_name' => 'add_excerpt',
                        'value' => array(__('Yes', THEME_SLUG) => 'yes'),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Excerpt Classes', THEME_SLUG),
                        'param_name'  => 'excerpt_classes',
                        'dependency' => array(
                            'element' => 'add_excerpt',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __('Display Link Button', THEME_SLUG),
                        'param_name' => 'add_link',
                        'value' => array(__('Yes', THEME_SLUG) => 'yes'),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Link Button Classes', THEME_SLUG),
                        'param_name'  => 'link_classes',
                        'dependency' => array(
                            'element' => 'add_link',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Meta Classes', THEME_SLUG),
                        'param_name'  => 'meta_classes',
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __('Display Date', THEME_SLUG),
                        'param_name' => 'add_date',
                        'value' => array(__('Yes', THEME_SLUG) => 'yes'),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Date Classes', THEME_SLUG),
                        'param_name'  => 'date_classes',
                        'dependency' => array(
                            'element' => 'add_date',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __('Display Author', THEME_SLUG),
                        'param_name' => 'add_author',
                        'value' => array(__('Yes', THEME_SLUG) => 'yes'),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Author Classes', THEME_SLUG),
                        'param_name'  => 'author_classes',
                        'dependency' => array(
                            'element' => 'add_author',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __('Display Tags', THEME_SLUG),
                        'param_name' => 'add_tags',
                        'value' => array(__('Yes', THEME_SLUG) => 'yes'),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Tag Classes', THEME_SLUG),
                        'param_name'  => 'tag_classes',
                        'dependency' => array(
                            'element' => 'add_tags',
                            'value' => array('yes'),
                        ),
                    ),
                    array(
                        'type' => 'checkbox',
                        'heading' => __('Display Categories', THEME_SLUG),
                        'param_name' => 'add_cats',
                        'value' => array(__('Yes', THEME_SLUG) => 'yes'),
                    ),
                    array(
                        'type'        => 'textfield',
                        'heading'     => __('Category Classes', THEME_SLUG),
                        'param_name'  => 'cat_classes',
                        'dependency' => array(
                            'element' => 'add_cats',
                            'value' => array('yes'),
                        ),
                    ),
                ),
            )
        );
    }

    // Element HTML
    public function vc_blog_grid_bp_html($atts)
    {

        $vc_elem = vc_map_get_defaults('vc_blog_grid_bp');
        $atts = shortcode_atts(
            $vc_elem,
            $atts
        );

        foreach ($atts as $key => $value) {
            if ($value === 'yes') {
                $atts[$key] = 1;
            }
        }

        extract($atts);

        $columns = array(
            $col_w_sm,
            $col_w_ms,
            $col_w_md,
            $col_w_ml,
            $col_w_lg,
            $col_w_xl,
        );
        $breakpoints = array(
            GRID_SM,
            GRID_MS,
            GRID_MD,
            GRID_ML,
            GRID_LG,
            GRID_XL
        );

        $post_args = array(
            'wrapper'      => array(
                'class'     => $wrapper_classes
            ),
            'body'         => array(
                'class' => $body_classes
            ),
            'add_image'    => $add_image,
            'image'        => array(
                'size'     => $image_size,
                'attr'     => array(
                    'wrapper' => array(
                        'class' => $image_wrapper_classes
                    ),
                    'class' => $image_classes
                ),
            ),
            'add_title'    => $add_title,
            'title'        => array(
                'tag'      => $title_tag,
                'class'    => $title_classes,
            ),
            'add_excerpt'  => $add_excerpt,
            'excerpt'      => array(
                'class'    => $excerpt_classes,
            ),
            'add_link'     => $add_link,
            'link'         => array(
                'class'    => $link_classes,
            ),
            'add_meta'     => 1,
            'meta'         => array(
                'outer_atts' => array(
                    'class' =>  $meta_classes
                ),
                'add_author'   => $add_author,
                'author'       => array(
                    'class' => $author_classes
                ),
                'add_date'   => $add_date,
                'date'       => array(
                    'class' => $date_classes
                ),
                'add_cats'   => $add_cats,
                'cats'       => array(
                    'class' => $cat_classes
                ),
                'add_tags'   => $add_tags,
                'tags'       => array(
                    'class' => $tag_classes
                ),
            )
        );

        if (!empty($before_title)) {
            $before_title = rawurldecode(base64_decode(wp_strip_all_tags($before_title)));
            $before_title = wpb_js_remove_wpautop(apply_filters('vc_raw_html_module_content', $before_title));

            $post_args['before_title']['content'] = wpb_js_remove_wpautop($before_title);
        }
        if (!empty($after_title)) {

            $after_title = rawurldecode(base64_decode(wp_strip_all_tags($after_title)));
            $after_title = wpb_js_remove_wpautop(apply_filters('vc_raw_html_module_content', $after_title));

            $post_args['after_title']['content'] = wpb_js_remove_wpautop($after_title);
        }

        $column_classes = '';
        $carousel_opts = array();
        $responsive = array(
            '0' => array(
                'items' => 1
            )
        );
        $loop_args = vc_parse_multi_attribute($blog_loop);

        if (isset($loop_args['size'])) {
            $loop_args['numberposts'] = $loop_args['size'];
            unset($loop_args['size']);
        }
        if (isset($loop_args['categories'])) {
            $loop_args['cat'] = $loop_args['categories'];
            unset($loop_args['categories']);
        }

        global $post;
        $custom_posts = get_posts($loop_args);

        $row_wrapper_classes = array(
            'content-item',
            'blog-' . $grid_type,
        );

        $row_wrapper_classes = outputClasses($row_wrapper_classes, $el_class);

        $item_classes = array();
        $item_classes = outputClasses($item_classes, $wrapper_classes);

        $item_wrapper = array();


        if ($grid_type === 'grid') {
            $column_classes = $columns;
        }

        if (strpos($wrapper_classes, 'card') !== false) {
            $row_wrapper_classes[] = 'card-grid';
        }

        if ($grid_type === 'grid' || $grid_type === 'masonry') {
            $row_wrapper_classes[] = 'row';
        }

        if ($grid_type === 'carousel') {
            $row_wrapper_classes[] = 'bp-carousel';
        }

        if ($remove_gutters == 1) {
            $row_wrapper_classes[] = 'no-gutters';
        }

        foreach ($columns as $index => $column) {
            $item = vcColNum($column);
            $breakpoint = $breakpoints[$index];
            if (!is_null($item)) {
                $responsive[$breakpoint]['items'] = $item;
            }
        }

        if (count($responsive) > 1) {
            $carousel_opts['responsive'] = $responsive;
        } else {
            $carousel_opts['items'] = $responsive[0];
        }

        if ($loop_carousel == 1) {
            $carousel_opts['loop'] = 'true';
        }
        if ($arrow_nav == 1) {
            $carousel_opts['nav'] = 'true';
        }
        if ($dots_nav == 1) {
            $carousel_opts['dots'] = 'true';
        }
        $item_wrapper['class'] = $item_classes;
        $post_args['wrapper'] = $item_wrapper;
        $grid_items = '';
        $dots = '';
        foreach ($custom_posts as $post) : setup_postdata($post);
            $id = get_the_ID();
            if (($grid_type === 'grid' && count($custom_posts) > 1) || ($grid_type === 'masonry' && count($custom_posts) > 1)) {

                $grid_item = postGridItem($id, $post_args);
                $grid_items = bootpressCol($grid_item, array('column' => array('class' => $column_classes)));
            } elseif ($grid_type === 'carousel') {
                $dot = iconFont('fal fa-circle owl-dot-icon');
                $dot = itemWrapperHTML($dot, array('tag' => 'span', 'class' => 'owl-dot'));
                $post_args['wrapper']['data-dot'] = $dot;
                $grid_items .= postGridItem($id, $post_args);
            } else {
                $grid_items .= postGridItem($id, $post_args);
            }
        endforeach;

        if (count($custom_posts) > 1) {

            if ($grid_type === 'grid' || $grid_type === 'masonry') {

                $wrapper_attributes['class'][] = 'container full-width';
                $output .= bootpressRow($grid_items, array('container' => $wrapper_attributes));
            } elseif ($grid_type === 'carousel') {
                $output .= bpCarousel($grid_items . $dots, array('wrapper' => $wrapper_attributes, 'carousel' => $carousel_opts));
            } else {
                $output .= $grid_items;
            }
        } else {
            $output .= $grid_items;
        }
        wp_reset_postdata();
        return $output;
    }
} // End Element Class

// Element Class Init
new bpBlogGrid();
