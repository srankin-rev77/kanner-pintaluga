<?php

/** ============================================================================
 * setup
 * @package K&P Attorney
 * @version <<version>>
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created:  7-23-19
 * Modified: 12-12-19 at 1:25 pm by Sam Rankin <samrankin.dev@gmail.com>
 * =========================================================================== */

$components = array(
    'functions',
    'text_reveal_box',
    'blog_grid'
);
foreach ($components as $component) {
    $path = dirname(__FILE__) . '/inc/vc_' . $component . '.php';
    if (file_exists($path)) {
        include_once $path;
    }
}

$col_inner_class = array(
    'type' => 'textfield',
    'heading' => 'Column Inner Class',
    'param_name' => 'inner_class',
    'value' => '',
    'weight' => '3',
    'description' => __('Class for the inner container of a column', THEME_SLUG)
);
vc_add_param('vc_column', $col_inner_class);
vc_add_param('vc_column_inner', $col_inner_class);

$content_wrapper_class = array(
    'type' => 'textfield',
    'heading' => 'Content Wrapper Class',
    'param_name' => 'content_wrapper_class',
    'value' => '',
    'description' => __('Class for the content wrapper of a column', THEME_SLUG)
);
vc_add_param('vc_column', $content_wrapper_class);
vc_add_param('vc_column_inner', $content_wrapper_class);

$row_inner_class = array(
    'type' => 'textfield',
    'heading' => 'Row Inner Class',
    'param_name' => 'inner_class',
    'value' => '',
    'description' => __('Class for the inner container of a row', THEME_SLUG)
);
vc_add_param('vc_row', $row_inner_class);
vc_add_param('vc_row_inner', $row_inner_class);

$section_inner_class = array(
    'type' => 'textfield',
    'heading' => 'Section Inner Class',
    'param_name' => 'inner_class',
    'value' => '',
    'description' => __('Class for the inner container of a section', THEME_SLUG)
);
vc_add_param('vc_section', $row_inner_class);

$is_bg = array(
    'type' => 'checkbox',
    'heading' => 'Does this image have a specific ratio?',
    'param_name' => 'is_bg',
    'value' => array(esc_html__('Yes', 'js_composer') => 'yes'),
);
vc_add_param('vc_single_image', $is_bg);

$image_ratio = array(
    'type' => 'dropdown',
    'heading' => esc_html__('Image Ratio', 'js_composer'),
    'param_name' => 'image_ratio',
    'value' => array(
        '21:9' => '21by9',
        '16:9' => '16by9',
        '4:3' => '4by3',
        '3:4' => '3by4',
        '3:2' => '3by2',
        '2:3' => '2by3',
        '2:1' => '2by1',
        '1:2' => '1by2',
        '1:1' => '1by1',
    ),
    'dependency' => array(
        'element' => 'is_bg',
        'value' => 'yes',
    ),
    'description' => esc_html__('Select image ratio', 'js_composer'),
);
vc_add_param('vc_single_image', $image_ratio);
