<?php

/** ============================================================================
 * assets
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/16/19
 * Modified: 12/26/19 by SR
 * ========================================================================== */

/**
 * Put the main css file in the footer for faster loading
 *
 * @return void
 */
function cssInFooter()
{
    $name = 'kp-attorney-main';
    $rel = ASSETS_PATH_URI . '/css/';
    $abs = ASSETS_PATH . '/css/';
    $file = '';
    if (defined(COMPRESS_CSS) && COMPRESS_CSS === true) {
        $name = $name . '.min';
    }

    $file = $name . '.css';

    $file_rel = $rel . $file;
    $file_abs = $abs . $file;

    if (file_exists($file_abs)) {
        $mod_time = filemtime($file_abs);
        wp_register_style($name, $file_rel, '', $mod_time, 'all');
        wp_enqueue_style($name);
    }
};
add_action('wp_enqueue_scripts', 'cssInFooter', 9999);
add_action('wp_footer', 'cssInFooter');

function themeScripts()
{

    $add_scripts = array(
        'kp-attorney-vendor',
        'kp-attorney-main'
    );
    foreach ($add_scripts as $script) {
        $name = $script;
        $rel = ASSETS_PATH_URI . '/js/';
        $abs = ASSETS_PATH . '/js/';
        $file = '';
        if (defined(COMPRESS_SCRIPTS) && COMPRESS_SCRIPTS === true) {
            $name = $name . '.min';
        }

        $file = $name . '.js';

        $file_rel = $rel . $file;
        $file_abs = $abs . $file;

        if (file_exists($file_abs)) {
            $mod_time = filemtime($file_abs);
            wp_register_script($name, $file_rel, '', $mod_time, 'all');
            wp_enqueue_script($name);
        }
    }
}
add_action('wp_enqueue_scripts', 'themeScripts', 9999);

/**
 * Remove any extra stylesheets
 *
 * @return void
 */
function removeExtraStyles()
{
    $styles = array(
        'font-awesome',
        'wp-block-library',
        'style',
        'mfn-fonts',
        'vc_font_awesome_5_shims',
        'vc_font_awesome_5',
        'mfn-shortcodes',
        'gforms_reset_css',
        'gforms_formsmain_css',
        'gforms_ready_class_css',
        'gforms_browsers_css',
        'gravity-forms-wcag-20-form-fields-css',
        'mfn-dynamic-inline'
    );

    foreach ($styles as $style) {
        wp_dequeue_style($style);
        wp_deregister_style($style);
    }
}
add_action('wp_enqueue_scripts', 'removeExtraStyles', 9999);
add_action('wp_print_styles', 'removeExtraStyles', 9999);

/**
 * Insert generated favicons into head
 * @link https://realfavicongenerator.net/
 */

function addFavicon()
{
    $folder       = ASSETS_PATH . '/imgs/favicons';
    $url          = ASSETS_PATH_URI . '/imgs/favicons';

    $version     = '7k4ydwrB8A';
    $theme_color = themeConfig('primary');
    if (file_exists($folder)) {
        $favicons = '';
        $favicons .= "\n" . '<link rel="apple-touch-icon" sizes="180x180" href="' . $url . '/apple-touch-icon.png?v=' . $version . '">';
        $favicons .= "\n" . '<link rel="icon" type="image/png" sizes="32x32" href="' . $url . '/favicon-32x32.png?v=' . $version . '">';
        $favicons .= "\n" . '<link rel="icon" type="image/png" sizes="16x16" href="' . $url . '/favicon-16x16.png?v=' . $version . '">';
        $favicons .= "\n" . '<link rel="manifest" href="' . $url . '/site.webmanifest?v=' . $version . '">';
        $favicons .= "\n" . '<link rel="mask-icon" href="' . $url . '/safari-pinned-tab.svg?v=' . $version . '" color="' . $theme_color . '">';
        $favicons .= "\n" . '<link rel="shortcut icon"  href="' . $url . '/favicon.ico?v=' . $version . '">';
        $favicons .= "\n" . '<meta name="msapplication-TileColor" content="' . $theme_color . '">';
        $favicons .= "\n" . '<meta name="msapplication-TileImage" content="' . $url . '/mstile-144x144.png?v=' . $version . '">';
        $favicons .= "\n" . '<meta name="msapplication-config" content="' . $url . '/browserconfig.xml?v=' . $version . '">';
        $favicons .= "\n" . '<meta name="theme-color" content="' . $theme_color . '">';
        echo $favicons;
    }
}
add_action('wp_head', 'addFavicon');
add_action('login_head', 'addFavicon');
add_action('admin_head', 'addFavicon');

/**
 * Add google fonts and Font Awesome asynchronously via webfont.js
 */

function asyncFonts()
{
    $webFontConfig = 'WebFontConfig =
            {
                google: {
                    families: [
                        \'Playfair Display:400,400i,700\',
                        \'Work Sans:400,600,800\'
                    ]
                },
                custom: {
                    families: [\'Font Awesome 5 Pro\', \'Font Awesome 5 Pro\', \'Font Awesome 5 Brands\'],
                    urls: [
                        \'' . ASSETS_PATH_URI . '/css/font-awesome-5-pro-300.css\',
                        \'' . ASSETS_PATH_URI . '/css/font-awesome-5-pro-900.css\',
                        \'' . ASSETS_PATH_URI . '/css/font-awesome-5-brands-400.css\',
                    ]
                },
                active: function() {sessionStorage.fonts = true;}
            };';


    $webfonts = $webFontConfig . '(function(d) {var wf = d.createElement(\'script\'),s = d.scripts[0];wf.src = \'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js\';wf.async = true;s.parentNode.insertBefore(wf, s);})(document);';

    $asyncFonts = itemWrapperHTML($webfonts, array('tag' => 'script', 'type' => 'text/javascript'));

    echo $asyncFonts;
}

add_action('wp_head', 'asyncFonts');

/**
 * Function for getting custom meta for an image
 * @param int $attachment_id
 * @param string $size
 *
 * @return array [
 *     'alt',
 *     'caption',
 *     'description',
 *     'title',
 *     'width',
 *     'height',
 *     'src',
 *     'full' => [
 *         'width',
 *         'height',
 *         'src'
 *     ],
 *     'thumbnail' => [
 *         'width',
 *         'height',
 *         'src'
 *     ]
 * ]
 */
if (!function_exists('wp_get_attachment')) {
    function wp_get_attachment($attachment_id, $size = 'full')
    {
        $attachment = get_post($attachment_id);
        $meta = wp_get_attachment_metadata($attachment_id);
        extract($meta);
        $directory = dirname(wp_get_original_image_url($attachment_id));
        $img_meta = array();
        $alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true);
        $caption = $attachment->post_excerpt;
        $description = $attachment->post_content;
        $title = $attachment->post_title;
        if (!empty($alt)) {
            $img_meta['alt'] = $alt;
        }
        if (!empty($caption)) {
            $img_meta['caption'] = $caption;
        }
        if (!empty($description)) {
            $img_meta['description'] = $description;
        }
        if (!empty($title)) {
            $img_meta['title'] = $title;
        }
        $full_size = array(
            'width' => $width,
            'height' => $height,
            'src' => wp_get_original_image_url($attachment_id)
        );
        if ($size !== 'full') {
            $img_meta['width'] = $sizes[$size]['width'];
            $img_meta['height'] = $sizes[$size]['height'];
            $img_meta['src'] = $directory . '/' . $sizes[$size]['file'];
            $img_meta['full'] = $full_size;
        } else {
            $img_meta = array_merge($img_meta, $full_size);
        }

        if (isset($sizes['thumbnail'])) {
            $img_meta['thumbnail'] = array(
                'width' => $sizes['thumbnail']['width'],
                'height' => $sizes['thumbnail']['height'],
                'src' => $directory . '/' . $sizes['thumbnail']['file']
            );
        }
        return $img_meta;
    }
}


/**
 * Function to create all the image sizes for lazy loading images
 *
 * @param integer|string $id Image ID or external url
 * @param string $size Optional. The maximum size of the image
 *
 * @return array HTML data attributes
 */
function lazySizes($id = '', $size = 'full', $bg = false)
{
    if (is_admin() || empty($id)) {
        return;
    }

    $url = $width  = $height = $srcset = $img_id = '';

    $directory = wp_get_upload_dir();
    $directory = $directory['basedir'];

    if (is_object($id)) {
        $img_id = get_the_ID($id);
    } else {
        $img_id = $id;
    }

    $lazy_sizes = array(
        'data-sizes' => 'auto',
        'class'      => array(
            'lazyload',
            'img-fluid'
        )
    );

    if (is_int($img_id) || is_numeric($img_id)) {
        $img_size = wp_get_attachment_image_src($img_id, $size);
        $srcset = wp_get_attachment_image_srcset($img_id, $size);
        $url = $img_size[0];
        $width  = intval($img_size[1]);
        $height = intval($img_size[2]);
    } elseif (is_url($img_id)) {
        $url = $img_id;
        $img_size = getimagesize($url);
        $width  = intval($img_size[0]);
        $height = intval($img_size[1]);
    }

    $lazy_sizes['data-src'] = $url;

    if (!empty($srcset)) {
        $lazy_sizes['data-srcset'] = $srcset;
        if ($bg == true) {
            $lazy_sizes['wrapper']['data-bgset'] = $srcset;
        }
    }

    if (!empty($width) && !empty($height)) {
        $lazy_sizes['data-aspectratio'] = ($height / $width);
    }

    if ($bg == true) {
        $lazy_sizes['wrapper']['class'][] = 'bg-img-wrapper';
        $lazy_sizes['wrapper']['data-sizes'] = 'auto';
        $lazy_sizes['wrapper']['data-bg'] = $url;
        $lazy_sizes['data-parent-fit'] = 'cover';
        $lazy_sizes['class'][] = 'bg-img';
        $lazy_sizes['wrapper']['class'][] = 'lazyload';
        if(empty($srcset)){
            $lazy_sizes['wrapper']['data-bgset'] = $url . ' ' . $width . 'w';
        }
    }

    return $lazy_sizes;
}


/**
 * Function to output a better image structure.
 *
 * Outputs an image structure that allows for lazyloading and image schema
 *
 * @param integer $id image id
 * @param string $size Maximum size of the image
 * @param array $attr Additional attributes
 * @param boolean $display Whether or not the value should be echoed or returned
 *
 * @return string|null
 *
 */

function displayImage($id = '', $size = 'full', $attr = array(), $bg = false, $add_meta = 0, $display = false)
{
    if (is_admin() || empty($id)) {
        return;
    }

    $content = $img = $img_meta = $schema = $output = '';

    $defaults = array(
        'remove_wrapper' => false,
        'wrapper' => array(
            'class'     => array('image-wrapper'),
        ),

        'block_title' => array(
            'content' => '',
            'tag' => 'h4',
            'class' => array('widget-title'),
        ),
        'caption'   => array(
            'content' => '',
            'tag' => 'figcaption',
            'class' => array('figure-caption'),
        ),
        'class' => array()
    );

    /**
     * Add lazyloading info to default array first in case there are any
     * overrides from $args
     */
    $defaults = parseArgs($defaults, lazySizes($id, $size, $bg));
    $attr = parseArgs($defaults, $attr);

    if (is_int($id) || is_numeric($id)) {
        // If the image is found in Wordpress
        $img_meta = wp_get_attachment($id);
        $img_meta_items = array(
            array(
                'itemprop' => 'url',
                'content'  => $img_meta['src']
            ),
            array(
                'itemprop' => 'width',
                'content'  => $img_meta['width']
            ),
            array(
                'itemprop' => 'height',
                'content'  => $img_meta['height']
            )
        );

        if (isset($img_meta['thumbnail']) && !empty($img_meta['thumbnail'])) {
            $thumb = array(
                'itemprop' => 'thumbnail',
                'content'  => $img_meta['thumbnail']['src']
            );
            $img_meta_items[] = $thumb;
        }

        // Only add the title if there isn't one already set

        if ((!isset($attr['title']) || empty($attr['title'])) && (isset($img_meta['title']) || !empty($img_meta['title']))) {
            $attr['title'] = $img_meta['title'];
        }

        // Only add the alt if there isn't one already set

        if ((!isset($attr['alt']) || empty($attr['alt'])) && (isset($img_meta['alt']) || !empty($img_meta['alt']))) {
            $attr['alt'] = $img_meta['alt'];
        }

        foreach ($img_meta_items as $item) {
            $schema .= metaItem($item['content'], $item['itemprop']);
        }
    }

    $wrapper = $attr['wrapper'];
    unset($attr['wrapper']);

    $block_title = $attr['block_title'];
    unset($attr['block_title']);

    $remove_wrapper = $attr['remove_wrapper'];
    unset($attr['remove_wrapper']);

    $caption = $attr['caption'];
    unset($attr['caption']);


    // ------------------------- Widget/Block Title ------------------------- //

    if (isset($block_title['content']) && !empty($block_title['content'])) {
        $item_content = $block_title['content'];
        unset($block_title['content']);
        $content .= itemWrapperHTML($item_content, $block_title);
    }

    // ------------------------------ Meta Tags ----------------------------- //

    if ($add_meta == true) {
        $wrapper['itemprop'] = 'image';
        $wrapper['itemscope'] = '';
        $wrapper['itemtype'] = 'http://schema.org/ImageObject';
        $content .= $schema;
        $attr['itemprop'] = 'contentUrl';
    }

    // ---------------------------- Image Output ---------------------------- //

    $img = '<img' . outputHTMLData($attr) . '/>';
    if ($bg != true) {
        $content .= $img;
    }



    // ------------------------------- Caption ------------------------------ //

    if (isset($caption['content']) && !empty($caption['content'])) {
        $item_content = $caption['content'];
        unset($caption['content']);
        $content .= itemWrapperHTML($item_content, $caption);
    }

    // ------------------------------- Wrapper ------------------------------ //

    if ($remove_wrapper != true) {
        $content = itemWrapperHTML($content, $wrapper);
    }

    // ------------------------------- Display ------------------------------ //

    if ($display == true) {
        echo $content;
    } else {
        return $content;
    }
}


/**
 * Function to add schema and lazyloading to videos
 *
 * @param string $src
 * @param array $args
 * @param bool $display
 *
 * @return string
 */
function displayVideo($src = '', $args = array(), $display = false)
{
    if (is_admin() || empty($src)) {
        return;
    }

    $defaults = array(
        'remove_wrapper' => false,
        'host'  => '',
        'url'   => '',
        'width' => '',
        'height' => '',
        'thumb' => '',
        'video_wrapper' => array(
            'class'     => array(
                'embed-responsive',
                'video-wrapper'
            ),
        ),
        'video' => array(
            'class' => array(
                'embed-responsive-item',
                'lazyload'
            ),
        ),
        'wrapper' => array(
            'tag' => 'figure',
            'class'     => array(
                'video-embed',
                'd-print-none',
            ),
            'itemprop'  => 'video',
            'itemscope' => '',
            'itemtype'  => 'http://schema.org/VideoObject',
        ),
        'caption'   => array(
            'content' => '',
            'tag' => 'figcaption',
            'class' => array('figure-caption', 'sr-only'),
        ),
        'title' => array(
            'content' => '',
            'tag'   => 'h5',
            'class' => array('video-title', 'sr-only'),
            'itemprop' => 'name'
        ),
        'desc'  => array(
            'content' => '',
            'tag'   => 'p',
            'class' => array('video-description', 'sr-only'),
            'itemprop' => 'description'
        ),
        'date'  => '',
    );

    $atts = parseArgs($args, $defaults);

    extract($atts);

    if (empty($src)) {
        return;
    }

    $wrapper['class'][] = 'type-' . $host;

    if (empty($width)) $width = intval($width);
    if (empty($height)) $height = intval($height);

    $video_meta_items = array(
        array(
            'itemprop' => 'contentURL',
            'content'  => $url
        ),
        array(
            'itemprop' => 'embedURL',
            'content'  => $url
        ),
        array(
            'itemprop' => 'uploadDate',
            'content'  => $date
        ),
        array(
            'itemprop' => 'width',
            'content'  => $width
        ),
        array(
            'itemprop' => 'height',
            'content'  => $height
        ),
        array(
            'itemprop' => 'thumbnailUrl',
            'content'  => $thumb
        ),
    );

    $schema = '';

    foreach ($video_meta_items as $item) {
        $schema .= '<meta' . outputHTMLData($item) . '>';
    }

    if (!empty($width) && !empty($height)) {
        $percentage = ($height / $width) * 100;
        $percentage = round($percentage, 2);
        $percentage = strval($percentage) . '%';
        $ratio = getRatio($width, $height, 'by');
        $allowed_ratios = array(
            '21by9',
            '16by9',
            '4by3',
            '3by4',
            '3by2',
            '2by3',
            '2by1',
            '1by2',
            '1by1'
        );
        if (in_array($ratio, $allowed_ratios)) {
            $video_wrapper['class'][] = 'embed-responsive-' . $ratio;
        } else {
            $video_wrapper['style']['padding-bottom'] = $percentage;
            $video['data-aspectratio'] = getRatio($width, $height, '/');
        }
    }

    if (!empty($thumb)) {
        $thumb = displayImage($thumb, 'full', array(), true);
    }
    $content = '';
    $video_content = $schema;
    if ($host === 'youtube' || $host === 'vimeo') {
        if ($host === 'youtube') {
            $video_wrapper['data-ytparams'] = 'modestbranding=1&playsinline=1';
            $video_wrapper['data-youtube'] = $src;
        } elseif ($host === 'vimeo') {
            $video_wrapper['data-vimeoparams'] = 'byline=0&responsive=1&title=0&autoplay=0&portrait=0';
            $video_wrapper['data-vimeo'] = $src;
        }
        $video_wrapper['class'] = outputClasses($video_wrapper['class'], array('bg-image', 'bg-image-xc-yc', 'lazyload'));
        $video['class'] = outputClasses($video['class'], array('d-flex', 'flex-column', 'justify-content-center', 'align-items-center'));

        $icon = iconFont('fas fa-play-circle', 'Play Video');
        $icon = itemWrapperHTML($icon, array('tag' => 'button', 'class' => 'play-btn btn btn-link bg-transparent p-0'));
        $video_content .= itemWrapperHTML($icon, $video);
    } elseif ($host === 'iframe') {
        $video['tag'] = 'iframe';
        $video['data-src'] = $video['src'];
        $video['allowfullscreen'] = '';
        $video['data-src'] = $video['src'];
        $video['frameborder'] = 0;
        $video['class'][] = 'lazyload';
        $video_content .= itemWrapperHTML('', $video);
    } else {
        $video['tag'] = 'video';
        $video['controls'] = '';

        $srcs = '';
        if (is_array($src)) {
            foreach ($src as $video_src) {
                $srcs .= '<source data-src="' . $video_src['url'] . '" type="' . $video_src['type'] . '">';
            }
        }
        $srcs .= __('Your browser does not support the video tag.', THEME_SLUG);
        $video_content .= itemWrapperHTML($srcs, $video);
    }

    // ---------------------------- Video Output ---------------------------- //

    $video_content = itemWrapperHTML($video_content, $video_wrapper);
    $content .= $video_content;

    // ------------------------- Widget/Block Title ------------------------- //

    if (isset($title['content']) && !empty($title['content'])) {
        $item_content = $title['content'];
        unset($title['content']);
        $content .= itemWrapperHTML($item_content, $title);
    }

    // ------------------------------- Caption ------------------------------ //

    if (isset($caption['content']) && !empty($caption['content'])) {
        $item_content = $caption['content'];
        unset($caption['content']);
        $content .= itemWrapperHTML($item_content, $caption);
    }

    // ------------------------------- Wrapper ------------------------------ //

    if ($remove_wrapper != true) {
        $content = itemWrapperHTML($content, $wrapper);
    }

    // ------------------------------- Display ------------------------------ //

    if ($display == true) {
        echo $content;
    } else {
        return $content;
    }
}

/**
 * Filters the wp_calculate_image_sizes function so that is returns an array
 * instead of a string to make it compatible with lazysizes.js
 *
 * @param mixed $sizes
 * @param array $size Returns integers for width and height
 * @param string $image_src Url of the specific image size
 * @param array $image_meta Meta array from the full size image, @uses `wp_get_attachment_metadata()`;
 * @param integer $attachment_id
 *
 * @return array $sizes [0] => $width [1] => $url
 */

function filterImageSizes($sizes, $size, $image_src, $image_meta, $attachment_id)
{
    $sizes   = array();
    $sizes[] = $size[0] . 'w';
    $sizes[] = $image_src;
    return $sizes;
};

// add the filter
//add_filter('wp_calculate_image_sizes', 'filterImageSizes', 10, 5);

function addLazyloadToImages($attr, $attachment, $size)
{
    $id = $attachment->ID;
    $attr['class'] .= ' img-fluid';
    $ratio = lazySizes($id, $size);
    if ($attachment->post_mime_type !== 'image/svg+xml') {
        $attr['class'] .= ' lazyload';
        $attr['data-aspectratio'] = $ratio['data-aspectratio'];
        if (isset($attr['src'])) {
            $attr['data-src'] = $attr['src'];
            $attr['src']      = '';
            unset($attr['src']);
        }
        if (isset($attr['srcset'])) {
            $attr['data-srcset'] = $attr['srcset'];
            $attr['srcset']      = '';
            unset($attr['srcset']);
        }
        if (isset($attr['sizes'])) {
            $attr['data-sizes'] = 'auto';
            $attr['sizes']      = '';
            unset($attr['sizes']);
        } else {
            $attr['data-sizes'] = 'auto';
        }
    } else {
        $attr['class'] .= ' style-svg';
    }
    return $attr;
}

add_filter('wp_get_attachment_image_attributes', 'addLazyloadToImages', 15, 3);

function getAvatar($id, $args = null)
{
    $defaults = array(
        'size' => 60,
    );
    $atts = parseArgs($defaults, $args);
    $avatar = get_avatar_data($id, $atts);
    if (strpos($avatar['url'], 'blank') === false) {
        $img_atts = '';
        if (isset($atts['img_atts'])) {
            $img_atts = $atts['img_atts'];
        }
        $img = displayImage($avatar['url'], $img_atts);
    } else {
        $classes = outputClasses($atts['class'], 'user-avatar bg-light');
        $img = iconFont('fas fa-user text-dark user-icon', 'No Avatar');
        $img = itemWrapperHTML($img, array('class' => $classes));
    }
    return $img;
}

/** Add wrapper for post thumbnail **/
function updateFeaturedImageStructure($html, $post_id, $post_thumbnail_id, $size, $attr)
{

    if (is_admin() || empty($post_thumbnail_id)) {
        return;
    }

    $bg = false;

    if (empty($attr)) {
        $attr = array();
    }

    $alt = the_title_attribute(
        array(
            'echo' => false,
            'post' => $post_id,
        )
    );

    if (isset($attr['bg'])) {
        $bg = $attr['bg'];
        unset($attr['bg']);
    }

    if (isset($attr['parent-fit'])) {
        $attr['data-parent-fit'] = $attr['parent-fit'];
        unset($attr['parent-fit']);
    }

    if (!isset($attr['alt']) || !array_key_exists('alt', $attr)) {
        $attr['alt'] = $alt;
    }

    $html = displayImage($post_thumbnail_id, $size, $attr, $bg);
    return $html;
};

add_filter('post_thumbnail_html', 'updateFeaturedImageStructure', 10, 5);

/**
 *
 * Modified from: Sunyatasattva
 * @link https://wordpress.stackexchange.com/questions/81522/change-html-structure-of-all-img-tags-in-wordpress
 * @param $the_content
 *
 * @return string
 */

function addImgLazyMarkup($the_content)
{

    libxml_use_internal_errors(true);

    if (!empty($the_content)) {
        $post = new DOMDocument();

        $post->loadHTML($the_content);

        $imgs = $post->getElementsByTagName('img');

        // Iterate each img tag
        foreach ($imgs as $img) {

            if ($img->hasAttribute('data-src')) {
                continue;
            }

            if ($img->parentNode->tagName == 'noscript') {
                continue;
            }

            $clone = $img->cloneNode();

            $src      = $img->getAttribute('src');
            $svg      = preg_match('/\.(svg)$/', $src);
            $imgClass = $img->getAttribute('class');
            $imgClass = $imgClass . ' img-fluid';
            if ($svg != 1) {
                $img->setAttribute('class', $imgClass . ' lazyload');
            }
            $img->removeAttribute('src');
            $img->setAttribute('data-src', $src);

            $srcset = $img->getAttribute('srcset');
            $img->removeAttribute('srcset');
            if (!empty($srcset)) {
                $img->setAttribute('data-srcset', $srcset);
            }

            $img->setAttribute('data-sizes', 'auto');

            $no_script = $post->createElement('noscript');
            $no_script->appendChild($clone);
            $img->parentNode->insertBefore($no_script, $img);
        };

        return $post->saveHTML();
    }
}
add_filter('the_content', 'addImgLazyMarkup', 20);

function imgTagClass($class, $id, $align, $size)
{
    $img_meta = get_post_mime_type($id);
    if ($img_meta !== 'image/svg+xml') {
        return $class . ' lazyload';
    }
}
//add_filter('get_image_tag_class', 'imgTagClass', 30, 4);
