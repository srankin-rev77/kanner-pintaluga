<?php

/** ============================================================================
 * walkers
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 06/27/19
 * Modified: 12/26/19 by SR
 * ========================================================================== */

/**
 * Starts the submenu wrap element
 *
 * @since WP 3.0.0
 * @since WP 4.4.0 The {@see 'nav_menu_item_args'} filter was added.
 *
 * @see Walker_Nav_Menu::start_lvl()
 *
 * @param WP_Post  $parent_id Parent menu item (needed for accordian type
 *                            functionality)
 * @param int      $depth     Depth of menu item. Used for padding.
 * @param stdClass $args      An object of wp_nav_menu() arguments.
 * @param int      $item_id   Current item ID.
 *
 * @return string $output
 */

function submenu_wrap_start($item_id = '', $menu_id = '', $parent_id = '', $depth = 0, $title = '', $args, $item)
{

    if (is_object($args)) {
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
    } elseif (is_array($args)) {
        if (isset($args['item_spacing']) && 'preserve' === $args['item_spacing']) {
            $t = "\t";
            $n = "\n";
        } else {
            $t = '';
            $n = '';
        }
    }

    $sub_menu_container_indent = str_repeat($t, $depth + 2);
    $sub_menu_wrapper_indent   = str_repeat($t, $depth + 3);
    $sub_menu_inner_indent     = str_repeat($t, $depth + 4);

    /**
     * Setup for dropdown toggle
     * @link https://getbootstrap.com/docs/4.4/components/collapse/#example
     */

    $collapse_atts = array(
        'tag'           => 'button',
        'data-toggle'   => 'collapse',
        'aria-controls' => 'menu-collapse-' . $item_id, //for ADA
        'aria-expanded' => 'false', //for ADA
        'aria-haspopup' => 'true', //for ADA
        'class'         => array(
            'btn',
            'btn-link',
            'btn-toggle',
            'p-0'
        ),
        'data-target'   => '#menu-collapse-' . $item_id,
        'aria-label'    => __('Show ' . $item->title . ' Sub-Menu', THEME_SLUG), //for ADA
    );

    $container_atts = array(
        'id'              => 'menu-collapse-' . $item_id,
        'aria-labelledby' => 'menu-item-' . $item_id,
    );
    //If this is a top level element, then the parent is the menu itself
    if ($depth == 0 || $parent_id == 0 || empty($parent_id)) {
        $container_atts['data-parent'] = '#menu-' . $menu_id;
    } else {
        $container_atts['data-parent'] = '#menu-collapse-' . $parent_id;
    }

    /**
     * The surrounding container attributes
     */
    $container_classes = array(
        'menu-container',
        'collapse',
        'level-' . ($depth + 2) . '-menu',
        'sub-menu',
    );
    $container_atts['class'] = outputClasses($container_classes);

    /**
     * The dropdown icon
     */
    $icon = iconFont('fal fa-plus dropdown-icon', __('Show ' . $item->title . ' Sub-Menu', THEME_SLUG));
    $output = itemWrapperHTML($icon, $collapse_atts);
    $output .= $n . $sub_menu_container_indent . '<div' . outputHTMLData($container_atts) . '>';
    $output .= $n . $sub_menu_wrapper_indent . '<div class="menu-wrapper">';
    $output .= $n . $sub_menu_inner_indent . '<nav class="menu-inner">';

    return $output;
}
/**
 * Ends the submenu wrap element
 *
 * @since WP 3.0.0
 * @since WP 4.4.0 The {@see 'nav_menu_item_args'} filter was added.
 *
 * @see Walker_Nav_Menu::end_lvl()
 *
 * @param int      $depth     Depth of menu item. Used for padding.
 * @param stdClass $args      An object of wp_nav_menu() arguments.
 *
 * @return string $output
 */
function submenu_wrap_end($depth = 0, $args)
{

    if (is_object($args)) {
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
    } elseif (is_array($args)) {
        if (isset($args['item_spacing']) && 'preserve' === $args['item_spacing']) {
            $t = "\t";
            $n = "\n";
        } else {
            $t = '';
            $n = '';
        }
    }

    $sub_menu_container_indent = str_repeat($t, $depth + 2);
    $sub_menu_wrapper_indent   = str_repeat($t, $depth + 3);
    $sub_menu_inner_indent     = str_repeat($t, $depth + 4);

    $output = '';
    $output .= $n . $sub_menu_inner_indent . '</nav>';
    $output .= $n . $sub_menu_wrapper_indent . '</div>';
    $output .= $n . $sub_menu_container_indent . '</div>';

    return $output;
}

/**
 * Find any custom linkmod or icon classes and store in their holder
 * arrays then remove them from the main classes array.
 *
 * Supported linkmods: .disabled, .dropdown-header, .dropdown-divider, .sr-only
 * Supported iconsets: Font Awesome 4/5, Glypicons
 *
 * NOTE: This accepts the linkmod and icon arrays by reference.
 *
 * @since 4.0.0
 *
 * @param array   $classes         an array of classes currently assigned to the item.
 * @param array   $linkmod_classes an array to hold linkmod classes.
 * @param array   $icon_classes    an array to hold icon classes.
 * @param integer $depth           an integer holding current depth level.
 *
 * @return array  $classes         a maybe modified array of classnames.
 */
function separate_linkmods_and_icons_from_classes($classes, &$linkmod_classes, &$icon_classes, $depth)
{
    // Loop through $classes array to find linkmod or icon classes.
    foreach ($classes as $key => $class) {
        /*
        * If any special classes are found, store the class in it's
        * holder array and and unset the item from $classes.
        */
        if (preg_match('/disabled|sr-only/i', $class)) {
            // Test for .disabled or .sr-only classes.
            $linkmod_classes[] = $class;
            unset($classes[$key]);
        } elseif (preg_match('/dropdown-header|dropdown-divider|^dropdown-item-text/i', $class) && $depth > 0) {
            /*
            * Test for .dropdown-header or .dropdown-divider and a
            * depth greater than 0 - IE inside a dropdown.
            */
            $linkmod_classes[] = $class;
            unset($classes[$key]);
        } elseif (preg_match('/^fa-(\S*)?|^fa(s|r|l|b)?(\s?)?$/i', $class)) {
            // Font Awesome.
            $icon_classes[] = $class;
            unset($classes[$key]);
        } elseif (preg_match('/^glyphicon-(\S*)?|^glyphicon(\s?)$/i', $class)) {
            // Glyphicons.
            $icon_classes[] = $class;
            unset($classes[$key]);
        }
    }

    return $classes;
}

/**
 * Return a string containing a linkmod type and update $atts array
 * accordingly depending on the decided.
 *
 * @since 4.0.0
 *
 * @param array    $linkmod_classes array of any link modifier classes.
 *
 * @return string  empty for default, a linkmod type string otherwise.
 */
function get_linkmod_type($linkmod_classes = array())
{
    $linkmod_type = '';
    if (!empty($linkmod_classes)) {
        // Loop through array of linkmod classes to handle their $atts.
        foreach ($linkmod_classes as $link_class) {
            if (!empty($link_class)) {
                // Check for special class types and set a flag for them.
                if ('dropdown-header' === $link_class) {
                    $linkmod_type = 'dropdown-header';
                } elseif ('dropdown-divider' === $link_class) {
                    $linkmod_type = 'dropdown-divider';
                } elseif ('dropdown-item-text' === $link_class) {
                    $linkmod_type = 'dropdown-item-text';
                }
            }
        }
    }
    return $linkmod_type;
}

/**
 * Update the attributes of a nav item depending on the limkmod classes.
 *
 * @since 4.0.0
 *
 * @param array $atts            array of atts for the current link in nav item.
 * @param array $linkmod_classes an array of classes that modify link or nav item behaviors or displays.
 *
 * @return array                 maybe updated array of attributes for item.
 */
function update_atts_for_linkmod_type($atts = array(), $linkmod_classes = array())
{
    if (!empty($linkmod_classes)) {
        foreach ($linkmod_classes as $link_class) {
            if (!empty($link_class)) {
                /*
                * Update $atts with a space and the extra classname
                * so long as it's not a sr-only class.
                */
                if ('sr-only' !== $link_class) {
                    $atts['class'] .= ' ' . esc_attr($link_class);
                }
                // Check for special class types we need additional handling for.
                if ('disabled' === $link_class) {
                    // Convert link to '#' and unset open targets.
                    $atts['href'] = '#';
                    unset($atts['target']);
                } elseif ('dropdown-header' === $link_class || 'dropdown-divider' === $link_class || 'dropdown-item-text' === $link_class) {
                    // Store a type flag and unset href and target.
                    unset($atts['href']);
                    unset($atts['target']);
                }
            }
        }
    }
    return $atts;
}

/**
 * Wraps the passed text in a screen reader only class.
 *
 * @since 4.0.0
 *
 * @param string $text the string of text to be wrapped in a screen reader class.
 * @return string      the string wrapped in a span with the class.
 */
function wrap_for_screen_reader($text = '')
{
    if ($text) {
        $text = '<span class="sr-only">' . $text . '</span>';
    }
    return $text;
}


/**
 * Returns the correct opening element and attributes for a linkmod.
 *
 * @since 4.0.0
 *
 * @param string $linkmod_type a sting containing a linkmod type flag.
 * @param string $attributes   a string of attributes to add to the element.
 *
 * @return string              a string with the openign tag for the element
 *                             with attribibutes added.
 */
function linkmod_element_open($linkmod_type, $attributes = '')
{
    $output = '';
    if ('dropdown-item-text' === $linkmod_type) {
        $output .= '<span class="dropdown-item-text"' . $attributes . '>';
    } elseif ('dropdown-header' === $linkmod_type) {
        /*
        * For a header use a span with the .h6 class instead of a real
        * header tag so that it doesn't confuse screen readers.
        */
        $output .= '<span class="dropdown-header h6"' . $attributes . '>';
    } elseif ('dropdown-divider' === $linkmod_type) {
        // This is a divider.
        $output .= '<div class="dropdown-divider"' . $attributes . '>';
    }
    return $output;
}

/**
 * Return the correct closing tag for the linkmod element.
 *
 * @since 4.0.0
 *
 * @param string $linkmod_type a string containing a special linkmod type.
 *
 * @return string  a string with the closing tag for this linkmod type.
 */
function linkmod_element_close($linkmod_type)
{
    $output = '';
    if ('dropdown-header' === $linkmod_type || 'dropdown-item-text' === $linkmod_type) {
        /*
        * For a header use a span with the .h6 class instead of a real
        * header tag so that it doesn't confuse screen readers.
        */
        $output .= '</span>';
    } elseif ('dropdown-divider' === $linkmod_type) {
        $output .= '</div>';
    }
    return $output;
}


/**
 * WP Bootstrap Navwalker with ADA Compliance and Schema (also formatted to work
 * with Betheme mega menu options)
 *
 * @package WP-Bootstrap-Navwalker
 *
 * @wordpress-plugin
 * Plugin Name: WP Bootstrap Navwalker
 * Plugin URI:  https://github.com/wp-bootstrap/wp-bootstrap-navwalker
 * Description: A custom WordPress nav walker class to implement the Bootstrap 4
 * navigation style in a custom theme using the WordPress built in menu manager.
 *
 * @author: Edward McIntyre - @twittem, WP Bootstrap, William Patton - @pattonwebz
 * @version: 4.3.0
 * Author URI: https://github.com/wp-bootstrap
 * GitHub Plugin URI: https://github.com/wp-bootstrap/wp-bootstrap-navwalker
 * GitHub Branch: master
 * @license: GPL-3.0+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.txt
 */
class Bootstrap_Navwalker extends Walker_Nav_Menu
{

    // columns

    public $columns    = 0;
    public $max_columns    = 0;

    // rows

    public $rows = 1;
    public $aRows    = array();

    // mega menu
    public $has_megamenu = 0;
    public $bg_megamenu    = '';

    /**
     * Starts the list before the elements are added.
     *
     * @since WP 3.0.0
     *
     * @see Walker_Nav_Menu::start_lvl()
     *
     * @param string   $output Used to append additional content (passed by reference).
     * @param int      $depth  Depth of menu item. Used for padding.
     * @param stdClass $args   An object of wp_nav_menu() arguments.
     */

    public function start_lvl(&$output, $depth = 0, $args = array())
    {
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }

        $sub_menu_indent = ($depth > 0) ? str_repeat($t, $depth + 5) : '';
        $menu_atts = array(
            'role' => 'menu', // Adding for ADA
        );

        $menu_classes = array('nav');

        /**
         * Adding classes for mega menu
         */
        if ($depth === 0) {
            if ($this->has_megamenu) {
                $menu_classes[] = 'kp_tag_ul_class';
                $menu_classes[] = 'mfn-megamenu';
                // mega menu background image
                if ($this->bg_megamenu) {
                    $menu_classes[] = 'mfn-megamenu-bg';
                    $menu_atts['kp_tag_ul_style'] = 1;
                }
            }
        }
        /**
         * Filters the CSS class(es) applied to a menu list element.
         *
         * @since WP 4.8.0
         *
         * @param array    $classes The CSS classes that are applied to the menu
         *                          `<ul>` element.
         * @param stdClass $args    An object of `wp_nav_menu()` arguments.
         * @param int      $depth   Depth of menu item. Used for padding.
         */
        $menu_classes = apply_filters('nav_menu_submenu_css_class', $menu_classes, $args, $depth);
        $menu_atts['class'] = outputClasses($menu_classes);

        $output .= $n . $sub_menu_indent . '<ul' . outputHTMLData($menu_atts) . '>';
    }

    public function end_lvl(&$output, $depth = 0, $args = array())
    {
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = "\t";
            $n = "\n";
        } else {
            $t = '';
            $n = '';
        }

        $sub_menu_indent = ($depth > 0) ? str_repeat($t, $depth + 5) : '';

        $output .= $n . $sub_menu_indent . '</ul>';
        if ($depth === 0) {
            if ($this->has_megamenu) {

                // mega menu background image
                if ($this->bg_megamenu) {
                    $style    = ' style="background-image:url(' . $this->bg_megamenu . ');"';
                    // replaces placeholder classes and adds proper column classes
                    $output    = str_replace("kp_tag_ul_class", "mfn-megamenu-" . esc_attr($this->max_columns), $output);
                    $output    = str_replace("kp_tag_ul_style=\"1\"", $style, $output);
                } else {
                    $output = str_replace("kp_tag_ul_class", "mfn-megamenu-" . esc_attr($this->max_columns), $output);
                    $output = str_replace("kp_tag_ul_style=\"1\"", "", $output);
                }

                foreach ($this->aRows as $row => $columns) {
                    $output = str_replace("kp_tag_li_class_" . $row, "mfn-megamenu-cols-" . esc_attr($columns), $output);
                }

                $this->columns = 0;
                $this->max_columns = 0;
                $this->aRows = array();
            } else {

                $output = str_replace("kp_tag_ul_class", "", $output);
                $output = str_replace("kp_tag_ul_style", "", $output);
            }
        }
    }
    /**
     * Starts the element output.
     *
     * @since WP 3.0.0
     * @since WP 4.4.0 The {@see 'nav_menu_item_args'} filter was added.
     *
     * @see Walker_Nav_Menu::start_el()
     *
     * @param string   $output Used to append additional content (passed by reference).
     * @param WP_Post  $item   Menu item data object.
     * @param int      $depth  Depth of menu item. Used for padding.
     * @param stdClass $args   An object of wp_nav_menu() arguments.
     * @param int      $id     Current item ID.
     */
    public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {

        global $wp_query;
        $item_output = $li_text_block_class = $column_class = "";
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent           = ($depth) ? str_repeat($t, $depth) : $t;
        $list_item_indent = ($depth > 0) ? $n . str_repeat($t, $depth + 6) : $indent;
        $list_link_indent = $n . str_repeat($t, $depth + 1);

        /**
         * Filters the arguments for a single nav menu item.
         *
         *  WP 4.4.0
         *
         * @param stdClass $args  An object of wp_nav_menu() arguments.
         * @param WP_Post  $item  Menu item data object.
         * @param int      $depth Depth of menu item. Used for padding.
         */

        $args = apply_filters('nav_menu_item_args', $args, $item, $depth);

        $list_item_classes = array('nav-item');

        if ($depth === 0) {

            // 1st level for Mega Menu

            $this->has_megamenu    = get_post_meta($item->ID, 'menu-item-mfn-megamenu', true);
            $this->bg_megamenu = get_post_meta($item->ID, 'menu-item-mfn-bg', true);

            if ($this->has_megamenu) {
                $list_item_classes[] = 'has-megamenu';
            }
        }

        if ($depth === 1 && $this->has_megamenu) {
            // 2nd level Mega Menu

            $this->columns++;
            $this->aRows[$this->rows] = $this->columns;

            if ($this->max_columns < $this->columns) {
                $this->max_columns = $this->columns;
            }

            if ($item->title != "-") {
                /**
                 * Adding the mega menu column title
                 */
                $list_link_atts['class'][] = 'mfn-megamenu-title';
                $list_link_atts['class'][] = 'nav-link';
                $list_link_atts = apply_filters('nav_menu_link_attributes', $list_link_atts, $item, $args, $depth);
                $attributes = outputHTMLData($list_link_atts);
                $item_output .= $args->before;
                $item_output .= '<span' . $attributes . '>';
                $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
                $item_output .= '</span>';
                $item_output .= $args->after;
            }

            $list_item_classes[] = 'kp_tag_li_class_' . $this->rows;
        } else {

            $linkmod_classes = array();
            $icon_classes    = array('nav-link-icon');
            $title           = apply_filters('the_title', $item->title, $item->ID);
            $list_link_classes = array();
            $list_link_atts = array(
                'target' => $item->target,
                'rel'    => $item->xfn,
                'role'   => 'menuitem',
            );

            if (!empty($item->classes)) {
                $list_item_classes = outputClasses($list_item_classes, (array) $item->classes);
            }
            /*
			 * Get an updated $classes array without linkmod or icon classes.
			 *
			 * NOTE: linkmod and icon class arrays are passed by reference and
			 * are maybe modified before being used later in this function.
			 */
            $list_item_classes = separate_linkmods_and_icons_from_classes($list_item_classes, $linkmod_classes, $icon_classes, $depth);

            /*
			 * Initiate empty icon var, then if we have a string containing any
			 * icon classes form the icon markup with an <i> element. This is
			 * output inside of the item before the $title (the link text).
			 */
            $icon_html = '';

            /**
             * If there are icons in the title along with text then separate them out
             */
            if (preg_match('/(\<i\sclass=\"([^"]+)\"\>\<\/i\>)(\V+)/', $title, $matches)) {
                $icon_classes = outputClasses($icon_classes, $matches[2]);
                $title        = ltrim($matches[3], ' ');
                if (!empty($icon_classes)) {
                    $icon_html = iconFont($icon_classes, $title);
                }
            }
            // Add classes/attributes for submenu setup
            if (isset($args->has_children) && $args->has_children) {
                $list_item_classes[] = 'has-children';
                $list_link_atts['data-sub-menu'] = $item->ID;
            }


            /**
             * Adding active class to certain menu items when on certain custom
             * post types
             */

            if (in_array('current-menu-item', $list_item_classes, true) || in_array('current-menu-parent', $list_item_classes, true)) {
                $list_item_classes[] = 'active';
            }

            // active: blog

            if (get_post_type(get_the_ID()) == 'post') {
                if ($item->object_id == get_option('page_for_posts')) {
                    $list_item_classes[] = 'current-menu-item';
                    $list_item_classes[] = 'active';
                }
            }

            // active: portfolio

            if (get_post_type(get_the_ID()) == 'portfolio') {
                if ($item->object_id == mfn_opts_get('portfolio-page')) {
                    $list_item_classes[] = 'current-menu-item';
                    $list_item_classes[] = 'active';
                }
            }

            // active: shop

            if (get_post_type(get_the_ID()) == 'product') {
                if (function_exists('is_woocommerce') && is_woocommerce()) {
                    if (version_compare(WC_VERSION, '2.7', '<')) {
                        $shop_page_id = woocommerce_get_page_id('shop');
                    } else {
                        $shop_page_id = wc_get_page_id('shop');
                    }
                    if ($item->object_id == $shop_page_id) {
                        $list_item_classes[] = 'current-menu-item';
                        $list_item_classes[] = 'active';
                    }
                }
            }

            /*
			 * Set title from item to the $atts array - if title is empty then
			 * default to item title.
			 */
            if (empty($item->attr_title)) {
                $list_link_atts['title'] = !empty($item->title) ? strip_tags($item->title) : '';
            } else {
                $list_link_atts['title'] = $item->attr_title;
            }
            $list_link_atts['href'] = !empty($item->url) ? $item->url : '#';
            $list_link_classes[]    = 'nav-link';

            if (in_array('current-menu-item', $list_item_classes, true) || in_array('current-menu-parent', $list_item_classes, true)) {
                $list_link_classes[] = 'active';
            }

            $list_link_atts['class'] = $list_link_classes;

            $list_link_atts = apply_filters('nav_menu_link_attributes', $list_link_atts, $item, $args, $depth);

            // Update atts of this item based on any custom linkmod classes.
            $list_link_atts = update_atts_for_linkmod_type($list_link_atts, $linkmod_classes);

            // Build a string of html containing all the atts for the item.
            $attributes = outputHTMLData($list_link_atts);

            // Set a typeflag to easily test if this is a linkmod or not.
            $linkmod_type = get_linkmod_type($linkmod_classes);

            $item_output = isset($args->before) ? $args->before : '';

            /*
			 * This is the start of the internal nav item. Depending on what
			 * kind of linkmod we have we may need different wrapper elements.
			 */
            if ('' !== $linkmod_type) {
                // Is linkmod, output the required element opener.
                $item_output .= linkmod_element_open($linkmod_type, $attributes);
            } else {
                // With no link mod type set this must be a standard <a> tag.
                $item_output .= $list_link_indent . '<a' . $attributes . '>';
            }

            /**
             * Filters a menu item's title.
             *
             * @since WP 4.4.0
             *
             * @param string   $title The menu item's title.
             * @param WP_Post  $item  The current menu item.
             * @param stdClass $args  An object of wp_nav_menu() arguments.
             * @param int      $depth Depth of menu item. Used for padding.
             */
            $title = apply_filters('nav_menu_item_title', $title, $item, $args, $depth);

            // If the .sr-only class was set apply to the nav items text only.
            if (in_array('sr-only', $linkmod_classes, true)) {
                $title         = wrap_for_screen_reader($title);
                $keys_to_unset = array_keys($linkmod_classes, 'sr-only');
                foreach ($keys_to_unset as $k) {
                    unset($linkmod_classes[$k]);
                }
            } else {
                $title = '<span class="nav-link-text">' . $title . '</span><div class="hover-line"></div>';
            }
            $item_output .= isset($args->link_before) ? $args->link_before . $icon_html . $title . $args->link_after : '';

            if ('' !== $linkmod_type) {
                $item_output .= linkmod_element_close($linkmod_type, $attributes);
            } else {
                $item_output .= '</a>';
            }

            $item_output .= isset($args->after) ? $args->after : '';

            // Submenu wrapper setup
            if (isset($args->has_children) && $args->has_children) {
                if (($depth !== 1 && $this->has_megamenu) || !$this->has_megamenu) {

                    $menu_id = $args->menu_id;

                    if (empty($menu_id)) {
                        $menu_id = $args->menu->slug;
                    }
                    $parent_id = $item->post_parent;
                    // Outputting the start of the sub menu to the item
                    $item_output .= submenu_wrap_start($item->ID, $menu_id, $parent_id, $depth, $item->title, $args, $item);
                }
            }
        }

        $list_item_classes = apply_filters('nav_menu_css_class', array_filter($list_item_classes), $item, $args, $depth);
        /**
         * Filters the ID applied to a menu item's list item element.
         *
         * @since WP 3.0.1
         * @since WP 4.1.0 The `$depth` parameter was added.
         *
         * @param string   $menu_id The ID that is applied to the menu item's `<li>` element.
         * @param WP_Post  $item    The current menu item.
         * @param stdClass $args    An object of wp_nav_menu() arguments.
         * @param int      $depth   Depth of menu item. Used for padding.
         */
        $list_item_id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args, $depth);

        $list_item_atts = array(
            'itemscope' => '',
            'itemtype'  => 'https://www.schema.org/SiteNavigationElement',
            'role'      => 'none', // for ADA
            'id'        => esc_attr($list_item_id),
            'class'     => $list_item_classes,
        );
        // Put the item contents into $output.
        $output .= $list_item_indent . '<li ' . outputHTMLData($list_item_atts) . '>';

        // END appending the internal item contents to the output.
        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }

    /**
     * Ends the element output.
     *
     * @since WP 3.0.0
     * @since WP 4.4.0 The {@see 'nav_menu_item_args'} filter was added.
     *
     * @see Walker_Nav_Menu::end_el()
     *
     * @param string   $output Used to append additional content (passed by reference).
     * @param WP_Post  $item   Menu item data object.
     * @param int      $depth  Depth of menu item. Used for padding.
     * @param stdClass $args   An object of wp_nav_menu() arguments.
     */
    public function end_el(&$output, $item, $depth = 0, $args = array())
    {
        if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent           = ($depth) ? str_repeat($t, $depth) : $t;
        $list_item_indent = ($depth > 0) ? $n . str_repeat($t, $depth + 6) : $indent;

        if (isset($args->has_children) && $args->has_children) {
            // Append the submenu closing tags
            $output .= submenu_wrap_end($depth, $args);
        }
        $output .= $list_item_indent . '</li>';
    }

    /**
     * Traverse elements to create list from elements.
     *
     * Display one element if the element doesn't have any children otherwise,
     * display the element and its children. Will only traverse up to the max
     * depth and no ignore elements under that depth. It is possible to set the
     * max depth to include all depths, see walk() method.
     *
     * This method should not be called directly, use the walk() method instead.
     *
     * @since WP 2.5.0
     *
     * @see Walker::start_lvl()
     *
     * @param object $element           Data object.
     * @param array  $children_elements List of elements to continue traversing (passed by reference).
     * @param int    $max_depth         Max depth to traverse.
     * @param int    $depth             Depth of current element.
     * @param array  $args              An array of arguments.
     * @param string $output            Used to append additional content (passed by reference).
     */
    public function display_element($element, &$children_elements, $max_depth, $depth, $args, &$output)
    {
        if (!$element) {
            return;
        }
        $id_field = $this->db_fields['id'];
        if (is_object($args[0])) {
            $args[0]->has_children = !empty($children_elements[$element->$id_field]);
        }
        parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }

    /**
     * Menu Fallback.
     *
     * If this function is assigned to the wp_nav_menu's fallback_cb variable
     * and a menu has not been assigned to the theme location in the WordPress
     * menu manager the function with display nothing to a non-logged in user,
     * and will add a link to the WordPress menu manager if logged in as an admin.
     *
     * @param array $args passed from the wp_nav_menu function.
     */
    public static function fallback($args)
    {
        if (current_user_can('edit_theme_options')) {

            $container       = $args['container'];
            $container_id    = $args['container_id'];
            $container_class = $args['container_class'];
            $menu_class      = $args['menu_class'];
            $menu_id         = $args['menu_id'];

            $fallback_output = '';

            if ($container) {
                $fallback_output .= '<' . esc_attr($container);
                $fallback_output .= ($container_id) ? ' id="' . esc_attr($container_id) . '"' : '';
                $fallback_output .= ($container_class) ? ' class="' . esc_attr($container_class) . '"' : '';
                $fallback_output .= '>';
            }
            $fallback_output .= '<ul';
            $fallback_output .= ($menu_id) ? ' id="' . esc_attr($menu_id) . '"' : '';
            $fallback_output .= ($menu_class) ? ' class="' . esc_attr($menu_class) . '"' : '';
            $fallback_output .= '>';
            $fallback_output .= '<li><a href="' . esc_url(admin_url('nav-menus.php')) . '" title="' . esc_attr__('Add a menu', 'wp-bootstrap-navwalker') . '">' . esc_html__('Add a menu', 'wp-bootstrap-navwalker') . '</a></li>';
            $fallback_output .= '</ul>';
            $fallback_output .= ($container) ? '</' . esc_attr($container) . '>' : '';
            if (array_key_exists('echo', $args) && $args['echo']) {
                echo $fallback_output;
            } else {
                return $fallback_output;
            }
        }
    }
}

/**
 * Outputs a comment in the HTML5 format.
 *
 * @see wp_list_comments()
 *
 * @param WP_Comment $comment Comment to display.
 * @param int        $depth   Depth of the current comment.
 * @param array      $args    An array of arguments.
 */

class WP_Bootstrap_Walker_Comment extends Walker_Comment
{
    public function start_lvl(&$output, $depth = 0, $args = array())
    {
        $GLOBALS['comment_depth'] = $depth + 1;

        switch ($args['style']) {
            case 'div':
                break;
            case 'ol':
                $output .= '<ol class="children list-unstyled">' . "\n";
                break;
            case 'ul':
            default:
                $output .= '<ul class="children list-unstyled">' . "\n";
                break;
        }
    }

    protected function html5_comment($comment, $depth, $args)
    {

        $tag                = ('div' === $args['style']) ? 'div' : 'li';
        $comment_author_url = get_comment_author_url($comment);
        $comment_author     = get_comment_author($comment);

        $comment_class = array(
            'mds-mt-1',
            'media'
        );

        $comment_class[] = $this->has_children ? 'parent' : '';

        $html = '<' . $tag . ' id="comment-' . get_comment_ID() . '" ' . comment_class($comment_class, $comment, '', false) . ' itemprop="comment">';

        // Avatar

        $avatar_args = array(
            'size'          => $args['avatar_size'],
            'height'        => null,
            'width'         => null,
            'default'       => get_option('avatar_default', 'mystery'),
            'force_default' => false,
            'rating'        => get_option('avatar_rating'),
            'scheme'        => null,
            'class'         => 'lazyload img-fluid',
            'force_display' => false,
        );
        $avatar_data = get_avatar_data($comment, $avatar_args);
        $avatar_attr = array(
            'data-sizes' => 'auto',
            'data-src'   => get_avatar_url(get_comment_ID()),
            'height'     => $avatar_data['height'],
            'width'      => $avatar_data['width'],
            'alt'        => $comment_author,
            'class'      => outputClasses($avatar_data['class'], 'mds-mr-0 avatar comment-avatar'),
        );
        $avatar = '<img' . outputHTMLData($avatar_attr) . ' />';
        $html .= (0 != $args['avatar_size']) ? $avatar : '';

        // Comment Content

        $html .= '<div class="comment-content media-body">';
        $html .= '<div class="comment-text" itemprop="commentText">';
        $html .= get_comment_text();
        $html .= '</div>';

        // Comment Meta
        $comment_timestamp = sprintf(__('%1$s at %2$s', THEME_SLUG), get_comment_date('', $comment), get_comment_time());

        $html .= '<div class="comment-metadata small text-muted">Posted on ';
        $html .= '<a href="' . get_comment_link($comment, $args) . '">';

        $html .= '<time itemprop="commentTime" datetime="' . get_comment_time('c') . '" title="' . $comment_timestamp . '">' . $comment_timestamp . '</time>';
        $html .= '</a> by ';

        $html .= (!empty($comment_author_url)) ? sprintf('<a href="%s" rel="external nofollow" class="url">', $comment_author_url) : '';

        $html .= '<span itemprop="creator">' . $comment_author . '</span>';

        $html .= (!empty($comment_author_url)) ? '</a>' : '';

        $html .= ' <span class="edit-link-sep">&mdash;</span> ';

        $html .= '<a href="' . get_edit_comment_link() . '" title="' . __('Edit Comment', THEME_SLUG) . '"><span class="edit-link"><i class="fas fa-edit"></i> ' . __('Edit', THEME_SLUG) . '</span></a>';
        $html .= get_comment_reply_link(
            array_merge(
                $args,
                array(
                    'add_below' => 'comment-content',
                    'depth'     => $depth,
                    'max_depth' => $args['max_depth'],
                    'before'    => ' | <i class="fas fa-comment-dots"></i> ',
                    'after'     => '',
                )
            )
        );
        $html .= '</div> <!-- end .comment-metadata -->';

        echo $html;
    }

    public function end_el(&$output, $comment, $depth = 0, $args = array())
    {
        if (!empty($args['end-callback'])) {
            ob_start();
            call_user_func($args['end-callback'], $comment, $args, $depth);
            $output .= ob_get_clean();
            return;
        }
        if ('div' == $args['style']) {
            $output .= "</div> <!-- end .comment-content -->\n";
            $output .= "</div><!-- #comment-## -->\n";
        } else {
            $output .= "</div> <!-- end .comment-content -->\n";
            $output .= "</li><!-- #comment-## -->\n";
        }
    }
}


/**
 * WP Bootstrap Pagewalker with ADA Compliance and Schema
 *
 * @see WP-Bootstrap-Navwalker
 */
class WP_Bootstrap_Pagewalker extends Walker_page
{

    public $tree_type = 'page';

    public $db_fields = array(
        'parent' => 'post_parent',
        'id'     => 'ID',
    );
    public function start_lvl(&$output, $depth = 0, $args = array())
    {
        if (isset($args['item_spacing']) && 'preserve' === $args['item_spacing']) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }

        $sub_menu_indent = ($depth > 0) ? str_repeat($t, $depth + 5) : '';

        $menu_atts = array();

        $menu_classes = array('nav');
        $menu_classes = apply_filters('nav_menu_submenu_css_class', $menu_classes, $args, $depth);

        $menu_atts['class'] = outputClasses($menu_classes);

        $output .= $n . $sub_menu_indent . '<ul' . outputHTMLData($menu_atts) . '>';
    }

    public function end_lvl(&$output, $depth = 0, $args = array())
    {
        if (isset($args['item_spacing']) && 'preserve' === $args['item_spacing']) {
            $t = "\t";
            $n = "\n";
        } else {
            $t = '';
            $n = '';
        }
        $sub_menu_indent = ($depth > 0) ? $n . str_repeat($t, $depth + 5) : $n;

        $output .= $sub_menu_indent . '</ul>';
    }

    public function start_el(&$output, $page, $depth = 0, $args = array(), $current_page = 0)
    {

        if (isset($args['item_spacing']) && 'preserve' === $args['item_spacing']) {
            $t = "\t";
            $n = "\n";
        } else {
            $t = '';
            $n = '';
        }

        $indent           = ($depth) ? str_repeat($t, $depth) : $t;
        $list_item_indent = ($depth > 0) ? $n . str_repeat($t, $depth + 6) : $indent;
        $list_link_indent = $n . str_repeat($t, $depth + 1);

        extract($args, EXTR_SKIP);

        $list_item_classes = array(
            'level-' . ($depth + 1) . '-nav-item',
            'nav-item',
        );

        if (isset($pages_with_children[$page->ID]) || $has_children == true) {
            $list_item_classes[] = 'has-children';
        }

        if (!empty($current_page)) {
            $_current_page = get_post($current_page);
            if ($_current_page && in_array($page->ID, $_current_page->ancestors)) {
                $list_item_classes[] = 'current_page_ancestor';
                $list_item_classes[] = 'active';
            }
            if ($page->ID == $current_page) {
                $list_item_classes[] = 'current_page_item';
                $list_item_classes[] = 'active';
            } elseif ($_current_page && $page->ID == $_current_page->post_parent) {
                $list_item_classes[] = 'current_page_parent';
                $list_item_classes[] = 'active';
            }
        } elseif ($page->ID == get_option('page_for_posts')) {
            $list_item_classes[] = 'current_page_parent';
            $list_item_classes[] = 'active';
        }

        $list_item_classes = apply_filters('page_css_class', $list_item_classes, $page, $depth, $args, $current_page);

        $list_item_atts = array(
            'itemscope' => '',
            'itemtype'  => 'https://www.schema.org/SiteNavigationElement',
            'role'      => 'none',
            'id'        => 'page-item-' . $page->ID,
            'class'     => outputClasses($list_item_classes),
        );

        $output .= $list_item_indent . '<li ' . outputHTMLData($list_item_atts) . '>';

        $list_link_classes = array(
            'nav-link',
            'flex-fill',
        );

        $list_link_atts = array(
            'href'       => get_permalink($page->ID),
            'role'       => 'menuitem',
            'data-level' => ($depth + 1),
        );

        $title = $page->post_title;

        $list_link_atts['aria-current'] = ($page->ID == $current_page) ? 'page' : '';

        if ($depth == 0) {
            $list_link_atts['tabindex'] = '0';
        } else {
            $list_link_atts['tabindex'] = '-1';
        }

        $list_link_atts['class'] = outputClasses($list_link_classes);

        $link_atts = apply_filters('page_menu_link_attributes', $list_link_atts, $page, $depth, $args, $current_page);

        $list_link_atts['title'] = $page->post_title;

        if ('' === $title) {
            $title = sprintf(__('#%d (no title)'), $page->ID);
        } else {
            $link_atts['title'] = $title;
            $title              = '<span class="nav-link-text">' . $title . '</span>';
        }

        $output .= isset($args->before) ? $args->before : '';
        $output .= $list_link_indent . '<a' . outputHTMLData($link_atts) . '>' . $title . '</a>';

        if (!empty($show_date)) {
            if ('modified' == $show_date) {
                $time = $page->post_modified;
            } else {
                $time = $page->post_date;
            }

            $output .= '<span class="item-date">' . mysql2date($date_format, $time) . '</span>';
        }

        $output .= isset($args->after) ? $args->after : '';

        if (isset($pages_with_children[$page->ID]) || $has_children == true) {
            $menu_id   = $post_type . '-page';
            $parent_id = $page->post_parent;

            $output .= submenu_wrap_start($page->ID, $menu_id, $parent_id, $depth, $args);
        }
    }

    public function end_el(&$output, $page, $depth = 0, $args = array())
    {

        extract($args, EXTR_SKIP);

        if (isset($item_spacing) && 'preserve' === $item_spacing) {
            $t = "\t";
            $n = "\n";
        } else {
            $t = '';
            $n = '';
        }

        $indent           = ($depth) ? str_repeat($t, $depth) : $t;
        $list_item_indent = ($depth > 0) ? $n . str_repeat($t, $depth + 6) : $indent;
        if (isset($pages_with_children[$page->ID]) || $has_children == true) {
            $output .= submenu_wrap_end($depth, $args);
        }
        $output .= $list_item_indent . '</li>';
    }
}
