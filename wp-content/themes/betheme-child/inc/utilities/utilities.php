<?php

/** ============================================================================
 * utilities
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/16/19
 * Modified: 01/07/20 by SR
 * ========================================================================== */

/**
 * Get Theme Options from the theme-config file
 */
function themeConfig($val)
{
    $json_file = THEME_CONFIG_PATH;
    if (file_exists($json_file)) {
        $theme_json   = file_get_contents($json_file);
        $theme_config = json_decode($theme_json, true);
        $option = $theme_config[$val];
        return $option;
    } else {
        return false;
    }
}

function is_url($url)
{
    if (filter_var($url, FILTER_VALIDATE_URL) == true) {
        return true;
    } else {
        return false;
    }
}

/**
 * Better print_r function, wraps output in <pre> tags
 */
function betterPrint($val)
{
    echo '<pre>';
    print_r($val);
    echo '</pre>';
}

/**
 * Calculate the ratio between two numbers.
 *
 * @param int $num1 The first number.
 * @param int $num2 The second number.
 * @return string A string containing the ratio.
 */
function getRatio($num1, $num2, $divider = ':')
{
    for ($i = $num2; $i > 1; $i--) {
        if (($num1 % $i) == 0 && ($num2 % $i) == 0) {
            $num1 = $num1 / $i;
            $num2 = $num2 / $i;
        }
    }
    return $num1 . $divider . $num2;
}

function isPhoneNumber($str)
{
    $phone_regex = "/(?(DEFINE)(?'spacers'\s?\.?\-?))^\+?\d?(?P>spacers)((\(\d{3}\)?)|(\d{3}))(?P>spacers)(\d{3})(?P>spacers)(\d{4})/";
    preg_match($phone_regex, $str, $matches);

    if (!empty($matches)) {
        return true;
    } else {
        return false;
    }
}

/**
 * Function to format a phone number for a url
 */
function formatPhoneURL($number = '')
{
    if (empty($number) || !isPhoneNumber($number)) {
        return;
    }

    $number = preg_replace('/\s*\D/', '', $number);

    // get number length.
    $length = strlen($number);

    if ($length == 7) {
        $number = substr($number, 0, 3) . '-' . substr($number, 3, 4);
    } elseif ($length == 10) {
        $number = substr($number, 0, 3) . '-' . substr($number, 3, 3) . '-' . substr($number, 6, 4);
    } elseif ($length > 10) {
        $number = substr($number, 0, 1) . '-' . substr($number, 1, 3) . '-' . substr($number, 7, 4);
    }

    return $number;
}

/**
 * Load component partial file
 *
 * Loads a file from a specific compontent. The component must be located in the
 * components folder. If no file name is specified it defaults to template.
 *
 * @param string $component Name of folder where the partial is located.
 * (subfolder of components)
 * @param string $partial Name of file to load.
 * @uses get_template_part()
 */
function getComponent($component, $partial = 'template')
{
    if ($partial !== 'template') {
        $file = COMPONENT_PATH . '/' . $component . '/partials/' . $partial . '.php';
        $path = 'inc/components/' . $component . '/partials/' . $partial;
    } else {
        $file = COMPONENT_PATH . '/' . $component . '/partials/template.php';
        $path = 'inc/components/' . $component . '/partials/template';
    }

    if (file_exists($file)) {
        get_template_part($path);
    }
}

/**
 * Include component partial file
 *
 * Includes a file from a specific compontent. The component must be located in
 * the components folder and the file must be located in the partials sub folder.
 * To be used for adding functionality for a specific component
 *
 * @param string $component Name of folder where the partial is located.
 * (subfolder of components)
 * @uses include_once()
 */
function incComponent($component, $partial = 'setup')
{
    if ($partial !== 'setup') {
        $file = COMPONENT_PATH . '/' . $component . '/partials/' . $partial . '.php';
    } else {
        $file = COMPONENT_PATH . '/' . $component . '/setup.php';
    }

    if (file_exists($file)) {
        include_once $file;
    }
}

/**
 * Formats the string to title case
 *
 * @param string $string
 *
 * @return string
 */
function toTitleCase($string)
{
    $string = str_replace("_", " ", $string);
    $string = str_replace("-", " ", $string);
    /* Words that should be entirely lower-case */
    $articles_conjunctions_prepositions = array(
        'a', 'an', 'the',
        'and', 'but', 'or', 'nor',
        'if', 'then', 'else', 'when',
        'at', 'by', 'from', 'for', 'in',
        'off', 'on', 'out', 'over', 'to', 'into', 'with',
    );
    /* Words that should be entirely upper-case (need to be lower-case in this list!) */
    $acronyms_and_such = array(
        'asap', 'unhcr', 'wpse', 'wtf',
    );
    /* split title string into array of words */
    $words = explode(' ', mb_strtolower($string));
    /* iterate over words */
    foreach ($words as $position => $word) {
        /* re-capitalize acronyms */
        if (in_array($word, $acronyms_and_such)) {
            $words[$position] = mb_strtoupper($word);
            /* capitalize first letter of all other words, if... */
        } elseif (
            /* ...first word of the title string... */
            0 === $position ||
            /* ...or not in above lower-case list*/
            !in_array($word, $articles_conjunctions_prepositions)
        ) {
            $words[$position] = ucwords($word);
        }
    }
    /* re-combine word array */
    $string = implode(' ', $words);
    /* return title string in title case */
    return $string;
}
/**
 * Formats the string to Snake Case (snake_case)
 *
 * @param string $str
 *
 * @return string
 */
function toSnakeCase($str)
{
    $str = strtolower($str);
    $str = sanitize_title_with_dashes($str);
    $str = str_replace("-", "_", $str);
    return $str;
}
/**
 * Formats the string to Kebab Case (kebab-case)
 *
 * @param string $str
 *
 * @return string
 */
function toKebabCase($str)
{
    $str = strtolower($str);
    $str = sanitize_title_with_dashes($str);
    $str = str_replace("_", "-", $str);
    return $str;
}

/**
 * Returns a sanitized array or string of classes
 *
 * @param array $new_classes
 * @param mixed $additional_classes can be string or array or object
 * @param boolean $display Whether or not the value should be echoed or returned
 *
 * @return array|string|null
 */
function outputClasses($classes_1 = '', $classes_2 = '', $display = false)
{
    $classes = array();
    if (!empty($classes_1)) {
        if (is_array($classes_1)) {
            $classes = array_merge_recursive($classes, $classes_1);
        } elseif (is_object($classes_1)) {
            $classes_1 = (array) $classes_1;
            $classes = array_merge_recursive($classes, $classes_1);
        } else {
            $classes_1 = explode(' ', $classes_1);
            $classes   = array_merge_recursive($classes, $classes_1);
        }
    }
    if (!empty($classes_2)) {
        if (is_array($classes_2)) {
            $classes = array_merge_recursive($classes, $classes_2);
        } elseif (is_object($classes_2)) {
            $classes_2 = (array) $classes_2;
            $classes = array_merge_recursive($classes, $classes_2);
        } else {
            $classes_2 = explode(' ', $classes_2);
            $classes   = array_merge_recursive($classes, $classes_2);
        }
    }
    if (!empty($classes)) {
        $classes = array_unique($classes);
        foreach ($classes as $key => $value) {
            if (empty($value)) {
                unset($classes[$key]);
            } else {
                $classes[$key] = sanitize_html_class($value);
            }
        }
        if ($display == true) {
            $classes = implode(' ', $classes);
            return 'class="' . $classes . '"';
        } else {
            return $classes;
        }
    } else {
        return null;
    }
}

/**
 * Add html markup for inline styles from array
 *
 * @param array $styles
 * @param boolean $display Whether or not the value should be echoed or returned
 *
 * @return array|string|null
 */

function outputStyles($styles_1 = '', $styles_2 = '', $display = false)
{
    $styles = array();
    if (!empty($styles_1)) {
        if (is_array($styles_1)) {
            $styles = $styles_1;
        } elseif (is_object($styles_1)) {
            $styles_1 = (array) $styles_1;
            $styles = $styles_1;
        } else {
            $styles_1 = explode(' ', $styles_1);
            $styles   = $styles_1;
        }
    }
    if (!empty($styles_2)) {
        if (is_array($styles_2)) {
            $styles = parseArgs($styles, $styles_2);
        } elseif (is_object($styles_2)) {
            $styles_2 = (array) $styles_2;
            $styles = parseArgs($styles, $styles_2);
        } else {
            $styles_2 = explode(' ', $styles_2);
            $styles   = parseArgs($styles, $styles_2);
        }
    }
    if (!empty($styles)) {
        foreach ($styles as $key => $value) {
            if (empty($value)) {
                unset($styles[$key]);
            }
        }
        $styles     = array_unique($styles);
        $all_styles = '';

        if ($display == true) {
            foreach ($styles as $property => $value) {
                if ($property === 'background-image') {
                    $all_styles .= $property . ': url(' . $value . '); ';
                } else {
                    $all_styles .= $property . ': ' . $value . '; ';
                }
            }

            return 'style="' . trim($all_styles) . '"';
        } else {
            return $styles;
        }
    } else {
        return null;
    }
}

function unsetEmptyArrayValue($array)
{
    if (!empty($array) && is_array($array)) {
        foreach ($array as $key => $value) {
            if ($value === NULL || $value === '' || (is_array($value) && count($value) == 0)) {
                unset($array[$key]);
            } elseif (is_array($value) && count($value) >= 1) {
                $array[$key] = unsetEmptyArrayValue($array[$key]);
            }
        }
    }
    return $array;
}


function parseArgs($defaults = array(), $args = array())
{
    if (!empty($args)) {
        if (is_array($args)) {
            foreach ($args as $key => $value) {
                if ($key === 'class' && isset($defaults['class'])) {
                    $args['class'] = outputClasses($defaults['class'], $args['class']);
                } elseif ($key === 'style' && isset($defaults['style'])) {
                    $args['style'] = outputStyles($defaults['style'], $args['style']);
                } elseif (is_array($value) && array_key_exists($key, $defaults) && is_array($defaults[$key])) {
                    $args[$key] = parseArgs($defaults[$key], $args[$key]);
                }
            }
            $args = unsetEmptyArrayValue($args);
            $defaults = unsetEmptyArrayValue($defaults);
            $args = array_replace_recursive($defaults, $args);
            return unsetEmptyArrayValue($args);
        } else {
            return wp_parse_args($args, $defaults);
        }
    } else {
        return $defaults;
    }
}

/**
 * Sanitizes data attributes from an array and outputs them as a string
 *
 * @param array $data
 *
 * @return string
 */

function outputHTMLData($data = '')
{
    if (empty($data)) {
        return;
    }
    $empty_atts = array(
        'data-fancybox',
        'itemscope',
        'value',
        'allowfullscreen',
        'controls'
    );

    if (isset($data['tag'])) {
        unset($data['tag']);
    }

    $all_data = '';
    foreach ($data as $property => $value) {
        if (empty($value) && in_array($property, $empty_atts)) {
            $all_data .= ' ' . esc_attr($property);
        } elseif (!empty($value) && !is_array($value)) {
            if ('href' === $property) {
                $protocols = '';
                $url       = $value;
                if (isPhoneNumber($url)) {
                    $url       = 'tel:' . formatPhoneURL($url);
                    $protocols = 'tel';
                } elseif (is_email($value)) {
                    $protocols = 'mailto';
                    $url       = 'mailto:' . antispambot(sanitize_email($url));
                }
                $value = esc_url($url, $protocols);
            } else {
                $value = esc_attr__($value);
            }
            $all_data .= ' ' . esc_attr($property) . '="' . $value . '"';
        } elseif (!empty($value) && is_array($value)) {
            if ($property === 'style') {
                $all_data .= ' ' . outputStyles($value, '', true);
            } elseif ($property === 'class') {
                $all_data .= ' ' . outputClasses($value, '', true);
            } else {
                $all_data .= ' ' . esc_attr($property) . '="' . json_encode($value) . '"';
            }
        }
    }
    return $all_data;
}

function itemWrapperHTML($content = '', $args = array())
{

    $defaults = array(
        'tag'   => 'div',
        'id'    => '',
        'class' => '',
        'style' => ''
    );

    $options = parseArgs($defaults, $args);
    extract($options);

    $item = '';

    $item .= '<' . $tag . outputHTMLData($options) . '>' . $content . '</' . $tag . '>';

    return $item;
}

/**
 * Add classes to <body> tag based on the type of page
 *
 * @return string
 */

function outputContentClass()
{
    if (is_single()) {
        $content_class = 'post-page';
    } elseif (is_page()) {
        $content_class = 'content-page';
    } elseif (is_singular()) {
        $content_class = get_post_type() . '-page';
    } elseif (is_archive() || is_author() || is_category() || is_home() || is_tag()) {
        $content_class = 'archive-page';
    } elseif (is_404()) {
        $content_class = 'error-page';
    } elseif (is_search()) {
        $content_class = 'search-page';
    }
    return $content_class;
}

/**
 * Turn hex values into rgb values
 *
 * @param string $hex
 * @param string $opacity
 *
 * @return string rgba(r,g,b,a)
 */
function hexToRGBA($hex, $opacity)
{
    $hex = str_replace("#", "", $hex);

    if (strlen($hex) == 3) {
        $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
        $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
        $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
    } else {
        $r = hexdec(substr($hex, 0, 2));
        $g = hexdec(substr($hex, 2, 2));
        $b = hexdec(substr($hex, 4, 2));
    }

    $a = intval($opacity) * 0.01;

    $rgba = array($r, $g, $b, $a);

    $Final_Rgb_color = implode(", ", $rgba);

    return $Final_Rgb_color;
}
/**
 * Function to see if the current page is a posts page
 *
 * @return bool
 */
function isBlog()
{
    global $post;
    $posttype = get_post_type($post);
    return (((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ($posttype == 'post')) ? true : false;
}

/**
 * Function to get the slug of a post
 *
 * @param integer $post_id The id of post, defaults to current post
 *
 * @return string $slug The post slug
 */

function getSlug($post_id = null)
{
    $slug = '';
    $post = get_post($post_id);
    $object = get_queried_object();

    if (!empty($post)) {
        $slug   = $post->post_name;
    } elseif (!empty($object)) {
        $slug = $object->name;
    }
    return $slug;
}
/**
 * Get an item id from its slug (useful for shortcodes so the user does not have
 * to find the id manually)
 *
 * @param array $styles
 *
 * @return integer post id
 */

function getIdBySlug($slug, $type)
{
    $item = get_page_by_path($slug, object, $type);
    if ($item) {
        return $item->ID;
    } else {
        return null;
    }
}


/**
 * Function register a stylesheet
 *
 * @param string $file The name of the file with no extension
 * @param string $path Starting from the theme folder (not including the name of
 * the theme folder or the name of the file)
 * @param string $abs The absolute path to the theme
 * @param string $rel The relative path to the theme
 * @param bool $min Whether or not to use the minified version (if available)
 *
 */

function addStylesheet($name, $local = true, $rel = ASSETS_PATH_URI, $deps = array(), $ver = null, $path = '/css/', $abs = ASSETS_PATH)
{
    if ($local == true) {
        $file = '';
        if (defined(COMPRESS_CSS) && COMPRESS_CSS === true) {
            $file = $name . '.min';
        }

        $file = $file . '.css';

        $file_rel = $rel . $path . $file;

        if (!is_admin()) {
            if (!empty($abs)) {
                $file_abs = $abs . $path . $file;

                if (file_exists($file_abs)) {
                    $mod_time = filemtime($file_abs);
                    wp_register_style($name, $file_rel, $deps, $mod_time, 'all');
                    wp_enqueue_style($name);
                }
            } else {
                wp_register_style($name, $file_rel, $deps, $ver, 'all');
                wp_enqueue_style($name);
            }
        }
    } else {
        wp_register_style($name, $rel, $deps, $ver, 'all');
        wp_enqueue_style($name);
    }
}

function addScript($name, $local = true, $rel = ASSETS_PATH_URI, $deps = array('jquery'), $ver = null, $footer = true, $path = '/js/', $abs = ASSETS_PATH)
{
    if ($local == true) {
        $file = '';
        if (defined(COMPRESS_SCRIPTS) && COMPRESS_SCRIPTS === true) {
            $file = $name . '.min';
        }
        $file = $file . '.js';
        $file_rel = $rel . $path . $file;

        if (!is_admin()) {

            if (!empty($abs)) {
                $file_abs = $abs . $path . $file;
                if (file_exists($file_abs)) {
                    $mod_time = filemtime($file_abs);
                    wp_register_script($name, $file_rel, $deps, $mod_time, $footer);
                    wp_enqueue_script($name);
                }
            } else {
                wp_register_script($name, $rel, $deps, $ver, $footer);
                wp_enqueue_script($name);
            }
        }
    } else {
        wp_register_style($name, $rel, $deps, $ver, $footer);
        wp_enqueue_style($name);
    }
}

function itemType($id = '', $css = true)
{
    $item = get_post_type($id);

    if (!empty($item) || $item !== false) {
        if ($css) {
            $item = toKebabCase($item);
        } else {
            $item = toTitleCase($item);
        }
    }
}

/**
 * Function for making sure icons are ADA compliant
 *
 * @param string|array|object $class The icon classes
 * @param string              $label (Optional) The hidden label to add
 * @param bool                $list If this is a list item
 * @param array               $args Additional arguments
*/
function iconFont($class = '', $label = '', $list = false, $args = array())
{
    if (empty($class)) return;
    $icon_args = array(
        'tag'         => 'i',
        'aria-hidden' => 'true',
        'role'        => 'presentation',
        'class'       => outputClasses($class)
    );

    $atts = parseArgs($icon_args, $args);

    if (!empty($label)) {
        $label_args = array(
            'tag' => 'span',
            'class' => 'sr-only'
        );

        $label = esc_html__($label, THEME_SLUG);
        $label = itemWrapperHTML($label, $label_args);
    }

    $icon = itemWrapperHTML('', $atts);

    if ($list == true) {
        $list_args = array(
            'tag' => 'span',
            'class' => 'fa-li'
        );
        $icon = itemWrapperHTML($icon, $list_args);
        return $icon;
    } else {
        return $icon . $label;
    }
}


function metaItem($content = '', $itemProp = '')
{
    if (empty($content)) return;
    $atts = array(
        'itemprop' => $itemProp,
        'content'  => $content
    );
    return '<meta' . outputHTMLData($atts) . '>';
}

/**
 * WP Auto Post Link Title Attributes
 *
 * @link https://github.com/wpexplorer/wpex-auto-link-titles
 *
 * Description: Automatically adds link title attributes to links within posts
 * that don't have them.
 *
 * @author AJ Clarke
 * @version 1.2.0
 */
function wpex_auto_add_link_titles($content)
{

    // No need to do anything if there isn't any content or if DomDocument isn't supported
    if (empty($content) || !class_exists('DOMDocument')) {
        return $content;
    }

    // Define links array
    $links = array();

    // Create new dom object
    $dom = new DOMDocument;

    // Load html into the object
    $dom->loadHTML(utf8_decode($content));

    // Discard white space
    $dom->preserveWhiteSpace = false;

    // Loop through all content links
    foreach ($dom->getElementsByTagName('a') as $link) {

        // If the title attribute is already defined no need to do anything
        if ($link->getAttribute('title')) {
            continue;
        }

        // Get link text
        $link_text = $link->textContent;

        // If there isn't any link text (most probably an image) lets try and get it from the first child
        if (!$link_text && !empty($link->firstChild) && $link->firstChild->hasAttributes()) {

            // Get alt
            $alt = $link->firstChild->getAttribute('alt');

            // If no alt get image title
            $alt = $alt ? $alt : $link->firstChild->getAttribute('title');

            // Clean up alt (remove dashses and underscores which are common in WP)
            $alt = str_replace('-', ' ', $alt);
            $alt = str_replace('_', ' ', $alt);

            // Return cleaned up alt
            $link_text = $alt;
        }

        // Save links and link text in $links array
        if ($link_text) {
            $links[$link_text] = $link->getAttribute('href');
        }
    }

    // Loop through links array and update post content to add link titles
    if (!empty($links)) {
        foreach ($links as $text => $link) {
            if ($link && $text) {
                $text    = ($text); // Sanitize
                $text    = ucwords($text);  // Captilize words (looks better imo)
                $replace = $link . '" title="' . $text . '"'; // Add title to link
                $content = str_replace($link . '"', $replace, $content); // Replace post content
            }
        }
    }

    // Return post content
    return $content;
}

// Edit content on the front end
add_filter('the_content', 'wpex_auto_add_link_titles');

/**
 * Shortcode function to get and display child pages for a certain page
 * @param array $atts
 *
 * @return string
 */

function wpb_list_child_pages($atts)
{
    $atts = shortcode_atts(
        array(
            'id'                 => '',
        ),
        $atts
    );

    extract($atts);
    global $post;
    if (empty($id)) {
        $id = $post->ID;
    }

    $args = array(
        'depth'        => 1,
        'show_date'    => '',
        'date_format'  => get_option('date_format'),
        'child_of'     => $id,
        'exclude'      => '',
        'title_li'     => '',
        'echo'         => 0,
        'authors'      => '',
        'sort_column'  => 'menu_order, post_title',
        'link_before'  => '',
        'link_after'   => '',
        'item_spacing' => 'preserve',
        'walker'       => new WP_Bootstrap_Pagewalker,
    );

    if (is_page() && $post->post_parent)

        $childpages = wp_list_pages($args);
    else
        $childpages = wp_list_pages($args);

    if ($childpages) {

        $string = '<nav id="page-' . $id . '-subpages" class="page-list subpage-nav"><ul class="nav">' . $childpages . '</ul></nav>';
    }

    return $string;
}

add_shortcode('wpb_childpages', 'wpb_list_child_pages');

/**
 * Strip query from url
 */

function http_strip_query_param($url)
{
    $url = strtok($url, '?');

    return $url;
}
