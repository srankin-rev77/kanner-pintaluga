<?php

/** ============================================================================
 * Theme Setup
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/13/19
 * Modified: 01/09/20 by SR
 * ========================================================================== */


/**
 * Function to remove wordpress emoji for better performance
 *
 * @return void
 */
function removeEmoji()
{
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
}
add_action('init', 'removeEmoji');

function themeSetup()
{
    /**
     * Add support for core custom logo.
     *
     * @link https://codex.wordpress.org/Theme_Logo
     */
    add_theme_support('custom-logo', array(
        'height'      => 250,
        'width'       => 250,
        'flex-width'  => true,
        'flex-height' => true,
        'header-text' => array('site-title', 'site-description'),
    ));
    add_theme_support('automatic-feed-links');
    add_theme_support('title-tag');

    /**
     * Add theme support for selective refresh for widgets.
     */

    add_theme_support('customize-selective-refresh-widgets');
    /**
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support('html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

    add_theme_support('post-thumbnails');
}
add_action('after_setup_theme', 'themeSetup');


/**
 * Remove extra image sizes so that the site itsn't bogged down with THOUSANDS
 * of uncessary images
 *
 * @return void
 */
function removeExtraImageSizes()
{
    $sizes = get_intermediate_image_sizes();
    foreach ($sizes as $size) {
        if (!in_array($size, array('thumbnail', 'medium', 'large'))) {
            remove_image_size($size);
        }
    }

    return $sizes;
}
// Initialized as late as possible to prevent plugins from adding sizes
add_action('init', 'removeExtraImageSizes', 999);


/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function addBodyClasses($classes)
{
    $classes[] = getSlug(get_the_ID());
    $classes[] = outputContentClass();
    // Adds a class of hfeed to non-singular pages.
    if (!is_singular()) {
        $classes[] = 'hfeed';
    }

    $bodyClasses = outputClasses($classes);

    return $bodyClasses;
}
add_filter('body_class', 'addBodyClasses');

/**
 * Add a skip to content link for screen readers
 */

function skipLink()
{
    echo '<a class="skip-link sr-only" title="' . esc_attr('Skip to content', THEME_SLUG) . '" href="#Content">' . esc_html('Skip to content', THEME_SLUG) . '</a>';
}
add_action('mfn_hook_top', 'skipLink', 999);


/**
 * Function to convert HTML entities into characters
 *
 * @param string $content
 *
 * @return string The filtered content
 */
function filterTheContentInTheMainLoop($content)
{
    $entities = array(
        '•',
        'ñ',
        '“',
        '”',
        '‘',
        '’',
        '–',
        '‒',
        '‑',
        '‐',
        'Â ',
        'â€“',
        'â€”',
        'â€˜',
        'â€™',
        'â€œ',
        'â€'
    );
    $html = array(
        '&bull;',
        '&ntilde;',
        '&ldquo;',
        '&rdquo;',
        '&lsquo;',
        '&rsquo;',
        '&ndash;',
        '&#8210;',
        '&#8209;',
        '&#8208;',
        '',
        '&ndash;',
        '&ndash;',
        '&lsquo;',
        '&rsquo;',
        '&ldquo;',
        '&rdquo;',
    );
    $content = str_replace($entities, $html, $content);

    return $content;
}
add_filter('the_content', 'filterTheContentInTheMainLoop');
