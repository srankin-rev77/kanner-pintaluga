<?php

/** ============================================================================
 * theme-settings
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/16/19
 * Modified: 01/09/20 by SR
 * ========================================================================== */


/**
 * Register options pages for ACF (requires pro version)
 *
 * @return void
 */
function registerOptionsPages()
{

    // Check function exists.
    if (!function_exists('acf_add_options_page'))
        return;

    acf_add_options_page(array(
        'page_title' => 'General Theme Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug'  => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect'   => false,
        'autoload'   => true,
    ));

    acf_add_options_sub_page(array(
        'page_title'     => 'Theme Header Settings',
        'menu_title'    => 'Header',
        'parent_slug'    => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'     => 'Theme Footer Settings',
        'menu_title'    => 'Footer',
        'parent_slug'    => 'theme-general-settings',
    ));
    acf_add_options_sub_page(array(
        'page_title'     => 'Theme Sidebar Settings',
        'menu_title'    => 'Sidebars',
        'parent_slug'    => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'     => 'Practice Area Page Settings',
        'menu_title'    => 'Practice Areas',
        'parent_slug'    => 'theme-general-settings',
    ));

    acf_add_options_page(array(
        'page_title' => 'Company Info',
        'menu_title' => 'Company Info',
        'menu_slug'  => 'company-info',
        'capability' => 'edit_posts',
        'redirect'   => false,
        'autoload'   => true,
        'icon_url'   => 'dashicons-building',
    ));
}

// Hook into acf initialization.
add_action('acf/init', 'registerOptionsPages');


/**
 * Get a list of nav menus and add it to various fields that allow you to select
 * a menu
 */

function loadMenuOptions($field)
{
    $menus = wp_get_nav_menus();
    if (empty($menus)) return;

    $choices = array();

    foreach ($menus as $menu) {
        $key   = $menu->term_id;
        $value = $menu->name;
        $choices[$key] = $value;
    }

    $field['choices'] = $choices;

    return $field;
}

// acf_load_field - filter for every field
add_filter('acf/load_field/name=menu', 'loadMenuOptions');


/**
 * Get a list of active sidebars and add it to various fields that allow you to
 * select a widget area
 */
function loadWidgetAreaOptions($field)
{
    $widget_areas = $GLOBALS['wp_registered_sidebars'];
    if (empty($widget_areas)) return;

    $choices = array();

    foreach ($widget_areas as $widget_area) {
        $key   = $widget_area['id'];
        $value = $widget_area['name'];
        $choices[$key] = $value;
    }

    $field['choices'] = $choices;

    return $field;
}

// acf_load_field - filter for every field
add_filter('acf/load_field/name=widget_area', 'loadWidgetAreaOptions');

function loadLogoOptions($field)
{
    $logos = themeOptions('logo_variations');
    if (empty($logos) || !isset($logos)) return;

    $choices = array();

    foreach ($logos as $logo) {
        $key   = $logo['image']['id'];
        $value = $logo['name'];
        $choices[$key] = $value;
    }

    $field['choices'] = $choices;

    return $field;
}

// acf_load_field - filter for every field
add_filter('acf/load_field/name=logo_option', 'loadLogoOptions');

function acfContentItem($item = array())
{
    extract($item);
    $item_content = '';

    $wrapper_atts = array(
        'id'    => $id,
        'class' => array(
            'footer-widget'
        )
    );

    if (!empty($class)) {
        $wrapper_atts['class'] = outputClasses($wrapper_atts['class'], $class);
    }

    if (isset($tag) && !empty($class)) {
        $wrapper_atts['tag'] = $tag;
        if ($tag === 'a') {
            $wrapper_atts['title'] = $link['title'];
            if ($link['type'] === 'external') {
                $wrapper_atts['target'] = '_blank';
                $wrapper_atts['href'] = $link['external'];
            } elseif ($link['type'] === 'internal') {
                $wrapper_atts['href'] = get_permalink($link['internal']);
            } elseif ($link['type'] === 'anchor') {
                $wrapper_atts['href'] = sanitize_html_class($link['anchor']);
            }
        }
    }

    $wrapper_atts['class'][] = $acf_fc_layout;

    if ($acf_fc_layout === 'search') {

        $item_content = get_search_form(array('echo' => false));
    } elseif ($acf_fc_layout === 'logo') {

        $item_content = customLogoHTML($logo_option);
    } elseif ($acf_fc_layout === 'menu') {

        $item_content = bootpressNav($menu, $dropdown, $item);
    } elseif ($acf_fc_layout === 'image') {
        $item_content = displayImage($image);
    } elseif ($acf_fc_layout === 'copyright') {
        $text_atts = array();
        if (!empty($text_class)) {
            $text_atts['class'] = outputClasses($text_class);
        }
        if (isset($text_tag) && !empty($text_tag)) {
            $text_atts['tag'] = $text_tag;
            if ($text_tag === 'a') {
                $text_atts['title'] = $text_link['title'];
                if ($text_link['type'] === 'external') {
                    $text_atts['target'] = '_blank';
                    $text_atts['href'] = $text_link['external'];
                } elseif ($text_link['type'] === 'internal') {
                    $text_atts['href'] = get_permalink($text_link['internal']);
                } elseif ($text_link['type'] === 'anchor') {
                    $text_atts['href'] = sanitize_html_class($text_link['anchor']);
                }
            }
        }
        $item_content = itemWrapperHTML($additional_text, $text_atts);
    } elseif ($acf_fc_layout === 'company_info') {

        $layout_items = $item['info_to_display'];

        foreach ($layout_items as $layout_item) {
            extract($layout_item);
            $atts = array(
                'tag'   => $tag,
                'class' => $class,
                'id'    => $id,
                'title' => $title,
                'icon'  => $icon_class,
                'link'  => array(
                    'add'  => $add_link
                )
            );
            $company_info['add_' . $acf_fc_layout] = 1;
            $company_info[$acf_fc_layout] = $atts;
        }

        $item_content = displayLocationInfo($item['location'], $company_info);
    } elseif ($acf_fc_layout === 'social') {
        $args = array(
            'item_tag' => $item_tag,
        );
        $item_content = displaySocialProfiles($location, $args);
    }

    return bootpressItem($item_content, $wrapper_atts);
}

function acfColumnStyles($content, $styles = array())
{
    extract($styles);
    $classes = array(
        $class,
    );
    // foreach ($width as $key => $value) {
    //     if (empty($value) && $key === 'xs') {
    //         $classes[] = 'col-12';
    //     } elseif (!empty($value) && $key === 'xs') {
    //         $classes[] = 'col-' . $value;
    //     } elseif (!empty($value) && $key !== 'xs') {
    //         $classes[] = 'col-' . $key . '-' . $value;
    //     }
    // }
    $args = array(
        'column' => array(
            'id'    => $id,
            'class' => $class,
        ),
        'content_wrapper' => array(
            'class' => $inner_class,
        )
    );
    return bootpressCol($content, $args);
}
