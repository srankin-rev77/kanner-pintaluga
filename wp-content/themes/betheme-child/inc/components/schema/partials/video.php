<?php

function video_structure($args = array())
{
    $defaults = array(
        'host'  => '',
        'src'   => '',
        'url'   => '',
        'width' => '',
        'height' => '',
        'thumb' => '',
        'video' => array(
            'class' => array(
                'embed-responsive-item',
                'lazyload'
            ),
        ),
        'wrapper' => array(
            'class'     => array(
                'video-embed',
                'd-print-none',
                'embed-responsive'
            ),
            'itemprop'  => 'video',
            'itemscope' => '',
            'itemtype'  => 'http://schema.org/VideoObject',
        ),
        'meta'  => array(
            'tag'   => 'figcaption',
            'class' => 'figure-caption video-meta',
        ),
        'title' => array(
            'tag'   => 'h5',
            'class' => 'video-title',
            'itemprop' => 'name'
        ),
        'desc'  => array(
            'tag'   => 'p',
            'class' => 'video-description',
            'itemprop' => 'description'
        ),
        'date'  => '',
    );

    $atts = parseArgs($args, $defaults);

    extract($atts);

    if (empty($src)) {
        return;
    }

    $wrapper['class'] = 'type-' . $host;

    $wrapper['data-' . $host] = $id;

    if (!empty($width) && !empty($height)) {
        $wrapper['data-aspectratio'] = getRatio($width, $height, '/');
        $wrapper['aspectratio']      = (($height / $width) * 100);
        $wrapper['class'][] = 'embed-responsive-' . getRatio($width, $height, 'by');
    }

    if (!empty($thumb)) {
        $thumb = displayImage($thumb, 'full', array(), true);
    }



    $video_content = '';
    if ($host === 'youtube' || $host === 'vimeo') {
        if ($host === 'youtube') {
            $video['data-ytparams'] = 'modestbranding=1&playsinline=1';
            $video['data-youtube'] = $src;
        } elseif ($host === 'vimeo') {
            $video['data-vimeoparams'] = 'byline=0&responsive=1&title=0&autoplay=0&portrait=0';
            $video['data-vimeo'] = $src;
        }
        $icon = iconFont('fal fa-play-circle', 'Play Video');

        $play_btn = itemWrapperHTML($icon, array('tag' => 'button', 'class' => 'play-btn btn btn-link bg-transparent'));

        $video['class'] = outputClasses($video['class'], array('d-flex', 'flex-column', 'justify-content-center', 'align-items-center'));

        $video_content = $thumb . itemWrapperHTML($play_btn, $video);
    } elseif ($host === 'iframe') {
        $video['tag'] = 'iframe';
        $video['data-src'] = $video['src'];
        $video['allowfullscreen'] = '';
        $video['data-src'] = $video['src'];
        $video['frameborder'] = 0;

        $video_content = itemWrapperHTML('', $video);
    } else {
        $video['tag'] = 'video';
        $video['controls'] = '';

        $srcs = '';
        if (is_array($src)) {
            foreach ($src as $video_src) {
                $srcs .= '<source data-src="' . $video_src['url'] . '" type="' . $video_src['type'] . '">';
            }
        }
        $srcs .= __('Your browser does not support the video tag.', THEME_SLUG);
        $video_content = itemWrapperHTML($srcs, $video);
    }

    $video_html = '';

    $video_html .= '<figure ' . add_item_data($wrapper_atts) . '>';
    $video_html .= (!empty($thumb)) ? '<meta itemprop="thumbnailUrl" content="' . $thumb['url'] . '" />' : '';
    $video_html .= (!empty($url)) ? '<meta itemprop="contentURL" content="' . $url . '" />' : '';
    $video_html .= (!empty($url)) ? '<meta itemprop="embedURL" content="' . $url . '" />' : '';
    $video_html .= (!empty($date)) ? '<meta itemprop="uploadDate" content="' . $date . '" />' : '';
    $video_html .= (!empty($height)) ? '<meta itemprop="height" content="' . $height . '" />' : '';
    $video_html .= (!empty($width)) ? '<meta itemprop="width" content="' . $width . '" />' : '';
    $video_html .= $video;
    if (!empty($title) || !empty($desc)) {
        $video_html .= '<figcaption class="figure-caption video">';
        $video_html .= (!empty($title)) ? '<span class="font-weight-bold d-block" itemprop="name">' . $title . '</span>' : '';
        $video_html .= (!empty($desc)) ? '<span itemprop="description d-block">' . wp_trim_words($desc, 20) . '</span>' : '';
        $video_html .= '</figcaption>';
    }
    $video_html .= '</figure>';
    return $video_html;
}
