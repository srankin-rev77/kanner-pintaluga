<?php

/** ============================================================================
 * Template for blog post in the loop
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/12/19
 * Modified: 01/09/20 by SR
 * ========================================================================== */

$column = array(
    'column' => array(
        'class' => get_post_class('col-md-6 col-lg-4 mb-0 isotope-item')
    )
);


// Adding various classes to elements to use the Bootstrap card styles
$args = array(
    'wrapper'      => array(
        'class'     => array(
            'card',
            'bg-light',
        )
    ),
    'body'         => array(
        'class' => 'card-body'
    ),
    'add_image'    => 1,
    'image'        => array(
        'bg'       => 1,
        'attr' => array(
            'wrapper' => array(
                'class' => array(
                    'card-img',
                    'position-relative',
                    'embed-responsive',
                    'embed-responsive-3by2'
                ),
            )
        )
    ),
    'add_title'    => 1,
    'title'        => array(
        'tag'      => 'h4',
        'itemprop' => 'name headline',
        'class'    => 'card-title',
    ),
    'add_excerpt'  => 1,
    'excerpt'      => array(
        'tag'      => 'p',
        'itemprop' => 'description',
        'class'    => 'card-text',
    ),
    'add_link'     => 1,
    'link'         => array(
        'class'    => 'stretched-link btn btn-dark',
    ),
    'add_meta'     => 1,
    'meta'         => array(
        'outer_atts' => array(
            'class' => 'card-footer',
        ),
        'add_date'   => 1,
    )
);
$post = postGridItem(get_the_ID(), $args);
$post = vcCol($post, $column);
echo $post;
