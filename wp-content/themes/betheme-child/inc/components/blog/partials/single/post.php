<?php

/** ============================================================================
 * The content for a single post
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/12/19
 * Modified: 01/09/20 by SR
 * ========================================================================== */


global $post;
$post_slug    = $post->post_name;

$item         = get_post_type();
$id           = get_the_ID();

$single_post_nav = array(
    'hide-header'    => false,
    'hide-sticky'    => false,
    'in-same-term' => false,
);

$opts_single_post_nav = mfn_opts_get('prev-next-nav');
if (is_array($opts_single_post_nav)) {
    if (isset($opts_single_post_nav['hide-header'])) {
        $single_post_nav['hide-header'] = true;
    }
    if (isset($opts_single_post_nav['hide-sticky'])) {
        $single_post_nav['hide-sticky'] = true;
    }
    if (isset($opts_single_post_nav['in-same-term'])) {
        $single_post_nav['in-same-term'] = true;
    }
}

$post_prev = get_adjacent_post($single_post_nav['in-same-term'], '', true);
$post_next = get_adjacent_post($single_post_nav['in-same-term'], '', false);
$blog_page_id = get_option('page_for_posts');

// post classes

$classes = array();
if (!mfn_post_thumbnail(get_the_ID())) {
    $classes[] = 'no-img';
}
if (get_post_meta(get_the_ID(), 'mfn-post-hide-image', true)) {
    $classes[] = 'no-img';
}
if (post_password_required()) {
    $classes[] = 'no-img';
}
if (!mfn_opts_get('blog-title')) {
    $classes[] = 'no-title';
}

if (mfn_opts_get('share') == 'hide-mobile') {
    $classes[] = 'no-share-mobile';
} elseif (!mfn_opts_get('share')) {
    $classes[] = 'no-share';
}

if (mfn_opts_get('share-style')) {
    $classes[] = 'share-' . mfn_opts_get('share-style');
}
$classes      = get_post_class($classes);
$wrapper_classes = array(
    'main-content',
    $item . '-content',
    'col-ml-8'
);

// single post navigation | sticky
if (!$single_post_nav['hide-sticky']) {
    echo mfn_post_navigation_sticky($post_prev, 'prev', 'icon-left-open-big');
    echo mfn_post_navigation_sticky($post_next, 'next', 'icon-right-open-big');
}
if (!$single_post_nav['hide-header']) {
    echo mfn_post_navigation_header($post_prev, $post_next, $blog_page_id, $translate);
}
$wrapper_atts = array(
    'tag'      => 'article',
    'id'       => $post_slug . '-' . $item . '-content',
    'itemprop' => 'mainEntity',
    'role'     => 'main',
    'class'    => outputClasses($wrapper_classes, $classes),
);
$publisher_atts = array(
    'name' => array(
        'add' => 1,
    ),
    'phone' => array(
        'add' => 1,
    ),
    'email' => array(
        'add' => 1,
    ),
    'img'   => array(
        'add' => 1,
    )
);
$content = '';
$content .= metaItem('True', 'isFamilyFriendly') . $content;
$content .= publisherSchema($publisher_atts);
$content .= displayImage(get_post_thumbnail_id($id), 'large', array(
    'wrapper' => array(
        'class' => 'embed-responsive embed-responsive-16by9'
    ),
    'class' => 'embed-responsive-item',
    'data-parent-fit' => 'cover'
), '', 1);
$content .= itemWrapperHTML(wpautop(get_the_content($id)), array('class' => 'entry-content', 'itemprop' => 'mainEntityOfPage'));


$meta_args = array(
    'add_author'     => 0,
    'add_publisher'  => 0,
    'add_date'       => 1,
    'add_tags'       => 1,
    'add_cats'       => 1,
);

$content .= outputPostMeta($id, $meta_args);


$content = vcCol($content, array('wpb_wrapper' => $wrapper_atts, 'content_wrapper' => array('class' => 'align-items-center')));
$content = vcRow($content);
echo vcSection($content);
