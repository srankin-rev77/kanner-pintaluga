<?php

/** ============================================================================
 * setup
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/26/19
 * Modified: 01/10/20 by SR
 * ========================================================================== */


/**
 * Function to generate a menu toggle button
 *
 * Creates HTML for an ADA compatible menu toggle button based on Bootstrap v4
 * navbar
 *
 * @param string $target the css id of the menu to collapse
 * @param array $args
 *
 * @return string
 */
function togglerButton($target, $args = array())
{


    $defaults = array(
        'show_label' => 0,
        'text'       => array(
            'menu-open' => 'Close',
            'menu-close' => 'Menu'
        ),
        'label'      => 'Toggle Navigation',
        'atts'       => array(
            'tag'           => 'button',
            'aria-expanded' => 'false',
            'data-toggle'   => 'collapse',
            'aria-controls' => $target,
            'data-target'   => '#' . $target,
            'class'         => array(
                'navbar-toggler',
                'vc_col-auto',
                'mb-0'
            ),
        )
    );

    $options = parseArgs($defaults, $args);

    extract($options);

    if (isset($icon) && !empty($icon)) {
        $icon = iconFont($icon . ' navbar-toggler-icon');
    } else {
        $icon = '';
        for ($x = 1; $x <= 4; $x++) {
            $icon .= "<span class='navbar-toggler-icon-bar bar-$x'></span>";
        }
    }

    if ($atts['tag'] === 'button') $atts['type'] = 'button';

    $atts['aria-label'] = $label;

    $label_classes = array(
        'navbar-toggler-text'
    );

    if ($show_label == 0) $label_classes[] = 'sr-only';

    $btn_label = '';

    foreach ($text as $class => $value) {
        $label_classes[] = $class;
        $btn_label .= itemWrapperHTML($value, array('tag' => 'span', 'class' => $label_classes));
    }

    $icon = itemWrapperHTML($icon, array('tag' => 'span', 'class' => array('navbar-toggler-icon')));

    $output = itemWrapperHTML($icon . $btn_label, $atts);

    return $output;
}


/**
 * Function for generating a Bootstrap v4 compatible menu with a toggle button
 *
 * @param int|string|WP_Term $menu
 * @param bool $dropdown Whether or not this menu should be mobile compatible
 * @param array $args
 *
 * @uses Bootstrap_Navwalker
 */

function bootpressNav($menu, $dropdown = true, $args = array())
{
    $menu = wp_get_nav_menu_object($menu);
    $menu_id = $menu->menu_id;
    $menu_slug = $menu->menu_name;
    $id = '';
    if (!empty($menu_id)) {
        $id = $menu_id;
    } elseif (!empty($menu_slug)) {
        $menu_id = $menu_slug;
    } else {
        $menu_id = 'main';
    }
    $item_content = '';
    $defaults = array(
        'wrapper' => array(
            'id'    => 'menu-' . $menu_id,
            'class' => array()
        ),
        'menu_args'         => array(
            'menu'        => $menu,
            'echo'        => false,
            'container'   => false,
            'depth'       => 5,
            'fallback_cb' => 'Bootstrap_Navwalker::fallback',
            'walker'      => new Bootstrap_Navwalker(),
            'menu_class'   => array(
                'menu',
                'menu-main',
                'navbar-nav',
                'justify-content-' . themeConfig('desktop-bp') . '-end',
            ),

        )
    );

    if (isset($args['menu_class'])) {
        $args['menu_class'] = outputClasses($defaults['menu_class'], $args['menu_class']);
    }
    $options = parseArgs($defaults, $args);

    extract($options);

    $toggle_button_col_atts = array(
        'column' => array(
            'id'    => 'menu-toggle-col',
            'class' => array(
                'vc_col-auto',
                'd-' . themeConfig('desktop-bp') . '-none'
            ),
        ),
        'wpb_wrapper' => array(
            'class' => 'justify-content-center',
        ),
    );

    if ($dropdown == true) {
        $item_content .= vcCol(togglerButton('menu-' . $menu_id), $toggle_button_col_atts);
        $wrapper['class'][] = 'collapse';
        $wrapper['class'][] = 'navbar-collapse';
        $wrapper['class'][] = 'vc_col-' . themeConfig('desktop-bp') . '-auto';
        $wrapper['class'][] = 'align-items-' . themeConfig('desktop-bp') . '-stretch';
    }
    if (isset($menu_args['menu_class']) && is_array($menu_args['menu_class'])) {
        $menu_args['menu_class'] = implode(' ', $menu_args['menu_class']);
    }

    $item_content .= vcCol(
        wp_nav_menu(
            $menu_args
        ),
        array(
            'column' => $wrapper
        )
    );


    return $item_content;
}


/**
 * Function to replace mfn_nav_menu
 *
 */
if (!function_exists('kp_wp_nav_menu')) {
    function kp_wp_nav_menu()
    {
        $args = array();
        $menu = '';


        // split menu

        if (in_array(mfn_header_style(), array('header-split', 'header-split header-semi', 'header-below header-split'))) {
            mfn_wp_split_menu();
            return true;
        }

        // mega menu | custom walker

        $theme_disable = mfn_opts_get('theme-disable');
        if (!isset($theme_disable['mega-menu'])) {
            $args['walker'] = new bootstrap_Navwalker;
        }

        // custom menu

        if (mfn_ID() && is_single() && get_post_type() == 'post' && $custom_menu = mfn_opts_get('blog-single-menu')) {

            // theme options | single posts
            $menu = $custom_menu;
        } elseif (mfn_ID() && is_single() && get_post_type() == 'portfolio' && $custom_menu = mfn_opts_get('portfolio-single-menu')) {

            // theme options | single portfolio
            $menu = $custom_menu;
        } elseif ($custom_menu = get_post_meta(mfn_ID(), 'mfn-post-menu', true)) {

            // page options | page
            $menu = $custom_menu;
        } else {

            // default
            $args['theme_location'] = 'main-menu';
            $menu = 'main';
        }

        echo bootpressNav($menu, true, $args);

        // custom mobile menu
        mfn_wp_mobile_menu();
    }
}
