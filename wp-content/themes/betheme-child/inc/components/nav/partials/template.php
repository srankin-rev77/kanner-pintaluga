<?php

/** ============================================================================
 * template
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/26/19
 * Modified: 12/31/19 by SR
 * ========================================================================== */
$nav_bar_args = array(
    'container' => array(
        'tag' => 'nav',
        'id'  => 'menu',
        'role' => 'navigation',
        'aria-label' => 'Main Navigation',
        'class' => array(
            'full-width',
            'no-gutters',
            'bg-white',
            'main-navigation',
            'navbar',
            'navbar-light',
            'navbar-expand-' . themeConfig('desktop-bp'),

        )
    ),
    'row' => array(
        'class' => array(
            'no-gutters',
            'justify-content-between',
            'align-items-stretch',
            'flex-' . themeConfig('desktop-bp') . '-nowrap',
        )
    )
);

$overlay_menu = '';
$overlay_btn = '';
$logo = '';
$nav_bar_content = '';
$nav_content = '';
if (mfn_header_style(true) == 'header-overlay') {

    // overlay menu

    mfn_wp_overlay_menu();
    $overlay_menu .= ob_get_clean();
    $nav_bar_content .= itemWrapperHTML($overlay_menu, array('id' => 'Overlay'));

    // button

    $overlay_btn .= iconFont('icon-menu-fine open', 'Open Menu');
    $overlay_btn .= iconFont('icon-cancel-fine close', 'Close Menu');
    $nav_bar_content .= itemWrapperHTML($overlay_btn, array('tag' => 'button', 'class' => 'overlay-menu-toggle', 'role' => 'button', 'aria-label' => __('Toggle Navigation')));
}



if ($layoutID = mfn_layout_ID()) {

    $logo = get_post_meta($layoutID, 'mfn-post-logo-img', true);
    $logo = attachment_url_to_postid($logo);
} else {
    $logo = companyLogoID();
}

$logo = customLogoHTML($logo);
$logo = vcItem($logo, array('class' => 'navbar-brand m-0'));
$nav_bar_content .= vcCol($logo, array('column' => array('class' => 'vc_col-auto mb-0 flex-fill')));

ob_start();
if ((mfn_header_style(true) != 'header-overlay') && (mfn_opts_get('menu-style') != 'hide')) {

    // main menu
    if (in_array(mfn_header_style(), array('header-split', 'header-split header-semi', 'header-below header-split'))) {
        mfn_wp_split_menu();
        // responsive menu button
        $mb_class = '';
        if (mfn_opts_get('header-menu-mobile-sticky')) {
            $mb_class .= ' is-sticky';
        }

        $mobile_menu_btn .= iconFont('icon-menu-fine open', 'Open Menu');
        $menu_text = trim(mfn_opts_get('header-menu-text'));
        echo itemWrapperHTML($menu_text . $mobile_menu_btn, array('tag' => 'button', 'class' => array('responsive-menu-toggle', $mb_class), 'role' => 'button', 'aria-label' => __('Toggle Navigation')));
    } else {
        echo kp_wp_nav_menu();
    }
}
mfn_wp_secondary_menu();
$nav_bar_content .= ob_get_clean();
echo vcRow($nav_bar_content, $nav_bar_args);
