<?php

/** ============================================================================
 * Description
 * @package BootPress
 * @version <<version>>
 * -----
 * @author Sam Rankin <samrankin.dev@gmail.com>
 * @copyright Copyright (c) 2019 Your Company
 * -----
 * Created Date:  11-15-19
 * Last Modified: 12-3-19 at 9:27 am
 * Modified By:   Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * HISTORY:
 * Date    	By	Comments
 * --------	--	--------------------------------------------------------------
 * =========================================================================== */

$id = mfn_ID();
$slug = getSlug($id);
$post_type = get_post_type($id);
if ((mfn_opts_get('subheader') != 'all') && (!get_post_meta($id, 'mfn-post-hide-title', true)) && (get_post_meta($id, 'mfn-post-template', true) != 'intro')
) {

    $subheader_advanced = mfn_opts_get('subheader-advanced');
    if (!mfn_slider_isset() || (is_array($subheader_advanced) && isset($subheader_advanced['slider-show']))) {

        // subheader

        $subheader_options = mfn_opts_get('subheader');

        if (is_home() && !get_option('page_for_posts') && !mfn_opts_get('blog-page')) {
            $subheader_show = false;
        } elseif (is_array($subheader_options) && isset($subheader_options['hide-subheader'])) {
            $subheader_show = false;
        } elseif (get_post_meta($id, 'mfn-post-hide-title', true)) {
            $subheader_show = false;
        } else {
            $subheader_show = true;
        }

        // title

        if (is_array($subheader_options) && isset($subheader_options['hide-title'])) {
            $title_show = false;
        } else {
            $title_show = true;
        }

        // breadcrumbs

        if (is_array($subheader_options) && isset($subheader_options['hide-breadcrumbs'])) {
            $breadcrumbs_show = false;
        } else {
            $breadcrumbs_show = true;
        }

        if (is_array($subheader_advanced) && isset($subheader_advanced['breadcrumbs-link'])) {
            $breadcrumbs_link = 'has-link';
        } else {
            $breadcrumbs_link = 'no-link';
        }

        // output

        if ($subheader_show) {

            $breadcrumbs = $title_text = $title = $subheader_content = $xPos = $yPos = $img = $img_classes = '';

            $title_atts = array(
                'tag' => mfn_opts_get('subheader-title-tag', 'h1'),
                'id'  => toKebabCase($slug . '-' . $post_type . '-' . $id . '-title'),
                'class' => array(
                    'page-title'
                )
            );

            $link_atts = array(
                'tag' => 'a',
                'class' => array(
                    'page-title-link'
                )
            );

            $column_classes = array(
                'vc_col-12'
            );

            if (!is_single()) {
                $title_atts['class'][] = 'display-1';
                $column_classes[] = 'vc_col-ml-9';
                $column_classes[] = 'vc_col-lg-8';
            } else {
                $title_atts['class'][] = 'display-2';
            }

            if (get_field('header_text', $id)) {
                $title_text = get_field('header_text', $id);
                $title_text = __($title_text, THEME_SLUG);
            } elseif (is_author()) {
                $link_atts['href'] = get_author_posts_url(get_the_author_meta("ID"));
                $link_atts['title'] = get_the_author();
                $link_atts['rel'] = 'me';
                $title_text = itemWrapperHTML(get_the_author(), $link_atts);
                $title_text = __('Author Archives: ' . $title_text, THEME_SLUG);
            } elseif (is_singular('emd_employee')) {
                $link_atts['href'] = get_permalink();
                $link_atts['title'] = get_the_title();
                $link_atts['rel'] = 'me';
                $link_atts['class'][] = 'text-white';
                $args = array(
                    'id' => $id,
                    'name' => array(
                        'prefix'   => array(
                            'display' => 1,
                            'add' => 1,
                        ),
                        'first'   => array(
                            'display' => 1,
                            'add' => 1,
                        ),
                        'last'   => array(
                            'display' => 1,
                            'add' => 1,
                        ),
                        'suffix'   => array(
                            'display' => 1,
                            'add' => 1,
                        ),
                        'display' => 1
                    ),
                    'job'   => array(
                        'display' => 1,
                        'add' => 1,
                        'content' => get_post_meta($id, 'emd_employee_jobtitle', true),
                        'wrapper' => array(
                            'tag' => 'small',
                            'class' => array(
                                'd-block',
                                'h3',
                                'text-white'
                            )
                        )
                    ),
                );

                if (get_post_meta($id, 'emd_employee_jobtitle')) {
                    $args['job']['content'] = get_post_meta($id, 'emd_employee_jobtitle', true);
                    $args['job']['add'] = 1;
                }
                $title_text = itemWrapperHTML(personSchema($args), $link_atts);
            } elseif (isBlog()) {
                $page_for_posts = get_option('page_for_posts');
                $title_text = get_the_title($page_for_posts);
                if (is_single()) {
                    $title_text = get_the_title();
                }
            } elseif (is_search()) {
                if (trim($_GET['s'])) {
                    global $wp_query;
                    $total_results = $wp_query->found_posts;
                } else {
                    $total_results = 0;
                }

                $translate['search-results'] = mfn_opts_get('translate') ? mfn_opts_get('translate-search-results', 'results found for:') : __('results found for:', THEME_SLUG);
                $title_text = esc_html($total_results) . ' ' . esc_html($translate['search-results']) . ' ' . esc_html($_GET['s']);
            } elseif (is_404()) {
                $title_text = itemWrapperHTML('Error: 404', array('class' => array('d-block')));
                $title_text .= itemWrapperHTML('Page Not Found', array('class' => array('d-block')));
            } elseif (is_archive()) {

                $title_text = get_the_archive_title();
            } elseif (is_singular()) {
                $title_text = get_the_title();
            } else {
                $title_text = wp_kses(mfn_page_title(), mfn_allowed_html());
            }

            $title_text = __($title_text, THEME_SLUG);

            $title = itemWrapperHTML($title_text, $title_atts);

            if ($title_show) {
                $subheader_content .= vcItem($title, array(
                    'class' => 'subheader-title',
                ));
            }

            if ($breadcrumbs_show) {
                $subheader_content .= vcItem(mfn_breadcrumbs($breadcrumbs_link), array(
                    'class' => 'subheader-breadcrumbs',
                ));
            }
            $xPos = $yPos = $img = $img_classes = '';
            if (has_post_thumbnail($id)) {
                if (isBlog()) {
                    $img_id = get_post_thumbnail_id(get_option('page_for_posts'));
                } else {
                    $img_id = get_post_thumbnail_id($id);
                }
                $img_pos = '';
                if (function_exists('get_field')) {
                    if (get_field('background_image_horizontal_position', $id)) {
                        $xPos = get_field('background_image_horizontal_position', $id);
                    } else {
                        $xPos = 'c';
                    }
                    if (get_field('background_image_vertical_position', $id)) {
                        $yPos = get_field('background_image_vertical_position', $id);
                    } else {
                        $yPos = 'c';
                    }
                    $img_pos = 'bg-img-x' . $xPos . '-y' . $yPos;
                }

                $img_classes = array(
                    'header_image',
                    $img_pos
                );
            }
            $args = array(
                'outer' => array(
                    'id'  => 'Subheader',
                    'class' => array(
                        'header-page',
                        $post_type . '-header',
                        toKebabCase($slug . '-' . $post_type . '-' . $id . '-header'),
                        toKebabCase($post_type . '-header')
                    )
                ),
                'bg_img' => array(
                    'id' => $img_id,
                    'size' => 'full',
                    'attr' => array(
                        'alt' => get_the_title(),
                        'wrapper' => array(
                            'class' => $img_classes
                        )
                    )
                ),
            );

            $subheader = vcCol($subheader_content, array('column' => array(
                'class' => $column_classes,
            )));
            $subheader = vcRow($subheader, array('row' => array(
                'class' => array(
                    'justify-content-start'
                ),
            )));
            $subheader = vcSection($subheader, $args);
            echo $subheader;
        }
    }
}
