<?php

/** ============================================================================
 * action-bar
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/31/19
 * Modified: 12/31/19 by SR
 * ========================================================================== */

$action_bar_args = array(
    'container' => array(
        'id' => 'Action_bar',
        'class' => array(
            'full-width',
            'bg-dark'
        )
    ),
    'row' => array(
        'class' => array(
            'mb-0',
            'flex-nowrap',
            'justify-content-between',
            'align-items-center',
            'align-content-center'
        )
    )
);

$action_bar_content = '';

/**
 * Action Bar slogan area
 *
 * @package Betheme
 * @author Muffin group
 * @link https://muffingroup.com
 */

$title = '';
$header_slogan = mfn_opts_get('header-slogan');
$classes = array(
    'font-weight-bold',
    'text-uppercase',
    'm-0',
);
if ($header_slogan) {
    $title = wp_kses($header_slogan, mfn_allowed_html('desc'));
    $title = itemWrapperHTML($title, array('tag' => 'span', 'class' => 'd-block d-md-inline'));

    $args = array(
        'icon'  => array(
            'class' =>  'fas fa-phone text-white'
        ),
        'link'  => array(
            'add'  => 1
        ),
    );

    $phone = displayPhone('main', $args);
    $slogan = $title . ' ' . $phone;
    $slogan = itemWrapperHTML($slogan, array('tag' => 'p', 'class' => outputClasses($classes, 'slogan')));
    $slogan = vcItem($slogan, array('class' => 'slogan-col'));
    $action_bar_content .= vcCol($slogan, array('column' => array('class' => 'vc_col-auto top-bar-left mb-0')));
}
/**
 * @package Betheme
 * @author Muffin group
 * @link https://muffingroup.com
 */

$action_link = mfn_opts_get('header-action-link');

if ($action_link) {
    $action_options = mfn_opts_get('header-action-target');

    if (isset($action_options['target'])) {
        $action_target = 'blank';
    } else {
        $action_target = '';
    }

    if (isset($action_options['scroll'])) {
        $action_class = ' scroll';
    } else {
        $action_class = false;
    }

    $title = wp_kses(mfn_opts_get('header-action-title'), mfn_allowed_html('button'));

    $link_atts = array(
        'tag' => 'a',
        'href' => $action_link,
        'target' => $action_target,
        'title' => esc_attr__($title, THEME_SLUG),
        'class' => 'text-white'
    );

    $link = itemWrapperHTML($title, $link_atts);
    $link = itemWrapperHTML($title, array('tag' => 'p', 'class' => outputClasses($classes, 'action-link')));

    $link = vcItem($link, array('class' => 'action-link-col'));
    $action_bar_content .= vcCol($link, array('column' => array('class' => 'vc_col-auto top-bar-right mb-0')));
}

echo vcRow($action_bar_content, $action_bar_args);
