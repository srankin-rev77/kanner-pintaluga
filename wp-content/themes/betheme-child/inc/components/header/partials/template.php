<?php

/** ============================================================================
 * header-site
 * @package BootPress
 * @version <<version>>
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created:  10-5-19
 * Modified: 12-6-19 at 11:23 am by Sam Rankin <samrankin.dev@gmail.com>
 * =========================================================================== */


$class = $data_parallax = array();

$header_wrapper_atts = array(
    'id'  => 'Header_wrapper',
    'class' => array()
);

$header_atts = array(
    'tag' => 'Header',
    'id'  => 'masthead',
    'role' => 'banner'
);
echo '<div' . outputHTMLData($header_wrapper_atts) . '>';

if ('mhb' == mfn_header_style()) {
    do_action('mfn_header');
    if (mfn_header_style(true) == 'header-below') {
        echo mfn_slider();
    }
} else {
    if (mfn_header_style(true) != 'header-creative') {
        getComponent('header', 'top-bar');
    }
    if (mfn_header_style(true) != 'header-below') {
        echo mfn_slider();
    }
    getComponent('header', 'header-page');
}
echo '</div>';
if (get_post_meta(mfn_ID(), 'mfn-post-template', true) == 'intro') {
    get_template_part('includes/header', 'single-intro');
}
