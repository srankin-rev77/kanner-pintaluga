<?php

/**
 * @package Betheme
 * @author Muffin group
 * @link https://muffingroup.com
 */

$action_bar = mfn_opts_get('action-bar');
$action_link = mfn_opts_get('header-action-link');
$translate['wpml-no'] = mfn_opts_get('translate') ? mfn_opts_get('translate-wpml-no', 'No translations available for this page') : __('No translations available for this page', 'betheme');
$top_bar_args = array(
    'id' => 'Top_bar',
    'role' => 'banner',
    'class' => array(
        'loading'
    )

);
$top_bar_content = '';
ob_start();
getComponent('header', 'action-bar');
getComponent('nav');
$top_bar_content = ob_get_clean();
echo itemWrapperHTML($top_bar_content, $top_bar_args);
