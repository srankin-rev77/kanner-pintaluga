<?php

/** ============================================================================
 * Social Links Setup
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/12/19
 * Modified: 01/10/20 by SR
 * ========================================================================== */


/**
 * Function to generate a social profile link with ADA compatibility (screen
 * reader)
 *
 * @return string The html for the icon
 */

function socialProfileLink($profile = array())
{


    if (empty($profile)) {
        return;
    } else {
        extract($profile);
    }

    $company_name = toTitleCase(companyName() . ' ' . $title);
    $company_name = __($company_name, THEME_SLUG);
    $icon_classes = array(
        'social-profile-icon'
    );

    $link_atts = array(
        'tag'    => 'a',
        'href'   => $link,
        'title'  => $company_name,
        'target' => '_blank',
        'rel'    => 'nofollow',
        'class'  => array(
            'social-profile-link',
            toKebabCase($title),
        ),
    );


    $icon = '';

    // Build icon

    if ($icon_type === 'icon') {

        $icon_wrapper_atts = array(
            'tag'   => 'span',
            'class' => array(
                'social-profile-icon-wrapper',
                'embed-responsive',
                'embed-responsive-1by1',
            )
        );
        $icon_classes = outputClasses($icon_classes, $icon_class);
        $icon_classes[] = 'embed-responsive-item';
        $icon = iconFont($icon_classes, $company_name);
        $icon = itemWrapperHTML($icon, $icon_wrapper_atts);
    } elseif ($icon_type === 'image' && !empty($icon_image)) {

        $type = $icon_image['subtype'];

        $img_atts = array(
            'title'    => $company_name,
            'alt'      => $company_name,
            'class'    => array(
                'custom-icon-image'
            ),
            'role'     => 'image',
            'wrapper'  => $link_atts
        );

        $img_atts = parseArgs($icon_atts, $img_atts);

        if ($type === 'svg') {

            $meta = wp_get_attachment_metadata($icon_image['ID']);
            $path = $meta['file'];
            $uploads_dir = wp_get_upload_dir();
            $uploads_dir = $uploads_dir['basedir'];
            $icon = file_get_contents($uploads_dir . '/' . $path);
        } else {
            $icon = displayImage($img_id, 'full', $img_atts);
        }
    }

    // Wrap icon in a link


    $item = itemWrapperHTML($icon, $link_atts);

    return $item;
}


/**
 * Build a unordered list of social profiles based on chosen location
 *
 * @param string $location Defaults to the main location
 * @param array $args
 *
 * @return string the HTML for the social profiles
 */
function displaySocialProfiles($location = 'main', $args = array())
{

    $social_links = getLocationItem('social_links', $location);
    $social_links = $social_links['info'];

    $custom_social_links = getLocationItem($location, 'custom_social_links');
    $custom_social_links = $custom_social_links['info'];

    if (empty($social_links) && empty($custom_social_links)) {
        return;
    }

    $defaults = array(
        'tag'      => 'ul',
        'id'       => '',
        'item_tag' => 'li',
        'style'    => '',
        'class'    => array(
            'social-profiles',
            'list-inline'
        ),
    );

    $options = parseArgs($defaults, $args);

    $item_tag = $options['item_tag'];

    unset($options['item_tag']);

    $atts = $options;

    $profiles = '';

    if (!empty($social_links)) {

        foreach ($social_links as $social) {
            $title      = !empty($social['title']) ? $social['title'] : $social['acf_fc_layout'];

            $link_atts = array(
                'title'      => $title,
                'show_title' => $social['show_link_title'],
                'icon_type'  => $social['icon_type'],
                'icon_class' => $social['icon_class'],
                'icon_image' => $social['icon_image'],
                'link'       => $social['link'],
            );

            $link = socialProfileLink($link_atts);

            $profile_atts = array(
                'tag'   => 'li',
                'class' => array(
                    'social-profile',
                    'list-inline-item',
                    toKebabCase($title),
                )
            );

            $profile = itemWrapperHTML($link, $profile_atts);

            $profiles .= $profile;
        }
    }
    if (!empty($custom_social_links)) {

        foreach ($custom_social_links as $social) {
            $title      = !empty($social['title']) ? $social['title'] : $social['acf_fc_layout'];

            $link_atts = array(
                'title'      => $title,
                'show_title' => $social['show_title'],
                'icon_type'  => $social['icon_type'],
                'icon_class' => $social['icon_class'],
                'icon_image' => $social['icon_image'],
                'link'       => $social['link'],
            );

            $link = socialProfileLink($link_atts);

            $profile_atts = array(
                'tag'   => $options['item_tag'],
                'class' => array(
                    'social-profile',
                    'list-inline-item',
                    toKebabCase($title),
                )
            );

            $profile = itemWrapperHTML($link, $profile_atts);

            $profiles .= $profile;
        }
    }
    $profiles = itemWrapperHTML($profiles, $atts);

    return $profiles;
}

/**
 * Create a basic social share widget
 *
 * @return void
 */
function displaySocialShare()
{
    $social_links = '<ul class="social-icons share nav">';

    $id    = get_the_ID();
    $url   = get_permalink($id);
    $title = get_the_title($id);

    $list_item_classes = array(
        'nav-item',
    );

    $icon_wrapper_class = array(
        'nav-icon-wrapper',
        'embed-responsive',
        'embed-responsive-1by1',
    );

    $icon_classes = array(
        'nav-icon',
        'embed-responsive-item',
    );

    // -------------------------- Share on Facebook ------------------------- //

    $facebook_query = array(
        'u' => $url,
    );

    $facebook_url = add_query_arg($facebook_query, 'https://www.facebook.com/sharer.php');

    $facebook_atts = array(
        'href'    => $facebook_url,
        'onclick' => 'window.open(\'' . $facebook_url . '\',\'popup\',\'width=600,height=600\'); return false;',
        'target'  => 'popup',
        'title'   => 'Share this post on Facebook',
        'rel'     => 'nofollow',
        'class'   => 'nav-link',
    );

    $social_links .= '<li ' . outputClasses($list_item_classes, 'facebook', true) . '>';
    $social_links .= '<a ' . outputHTMLData($facebook_atts, '', true) . '>';
    $social_links .= '<span ' . outputClasses($icon_wrapper_class, '', true) . '>';
    $social_links .= '<i ' . outputClasses($icon_classes, 'fab fa-facebook-f', true) . '></i>';
    $social_links .= '</span>';
    $social_links .= '</a>';
    $social_links .= '</li>';

    // -------------------------- Share on Twitter -------------------------- //

    $twitter_query = array(
        'url'  => $url,
        'text' => rawurlencode($title),
    );

    $twitter_url = add_query_arg($twitter_query, 'https://twitter.com/intent/tweet');

    $twitter_atts = array(
        'href'    => $twitter_url,
        'onclick' => 'window.open(\'' . $twitter_url . '\',\'popup\',\'width=600,height=600\'); return false;',
        'target'  => 'popup',
        'title'   => 'Share this post on Twitter',
        'rel'     => 'nofollow',
        'class'   => 'nav-link',
    );

    $social_links .= '<li ' . outputClasses($list_item_classes, 'twitter', true) . '>';
    $social_links .= '<a ' . outputHTMLData($twitter_atts, '', true) . '>';
    $social_links .= '<span ' . outputClasses($icon_wrapper_class, '', true) . '>';
    $social_links .= '<i ' . outputClasses($icon_classes, 'fab fa-twitter', true) . '></i>';
    $social_links .= '</span>';
    $social_links .= '</a>';
    $social_links .= '</li>';

    // -------------------------- Share on LinkedIn ------------------------- //

    $linkedin_query = array(
        'url'  => $url,
        'text' => rawurlencode($title),
    );

    $linkedin_url = add_query_arg($linkedin_query, 'https://www.linkedin.com/shareArticle');

    $linkedin_atts = array(
        'href'    => $linkedin_url,
        'onclick' => 'window.open(\'' . $linkedin_url . '\',\'popup\',\'width=600,height=600\'); return false;',
        'target'  => 'popup',
        'title'   => 'Share this post on LinkedIn',
        'rel'     => 'nofollow',
        'class'   => 'nav-link',
    );

    $social_links .= '<li ' . outputClasses($list_item_classes, 'twitter', true) . '>';
    $social_links .= '<a ' . outputHTMLData($linkedin_atts, '', true) . '>';
    $social_links .= '<span ' . outputClasses($icon_wrapper_class, '', true) . '>';
    $social_links .= '<i ' . outputClasses($icon_classes, 'fab fa-linkedin-in', true) . '></i>';
    $social_links .= '</span>';
    $social_links .= '</a>';
    $social_links .= '</li>';

    // --------------------------- Share via Email -------------------------- //

    $msg = $title . "\n";
    $msg .= $url . "\n\n";
    if (has_excerpt()) {
        $msg .= get_the_excerpt();
    }
    $email_query = array(
        'url'     => $url,
        'subject' => rawurlencode('Check Out This Article From ' . get_bloginfo('title')),
        'body'    => rawurlencode($msg),
    );

    $email_url = add_query_arg($email_query, 'mailto:');

    $email_atts = array(
        'href'    => $email_url,
        'onclick' => 'window.open(\'' . $email_url . '\',\'popup\',\'width=600,height=600\'); return false;',
        'target'  => 'popup',
        'title'   => 'Share this post via email',
        'rel'     => 'nofollow',
        'class'   => 'nav-link',
    );

    // $social_links .= '<li ' . outputClasses($list_item_classes, 'facebook', true) . '>';
    // $social_links .= '<a ' . outputHTMLData($email_atts, '', true) . '>';
    // $social_links .= '<span ' . outputClasses($icon_wrapper_class, '', true) . '>';
    // $social_links .= '<i ' . outputClasses($icon_classes, 'fas fa-envelope', true) . '></i>';
    // $social_links .= '</span>';
    // $social_links .= '</a>';
    // $social_links .= '</li>';

    $social_links .= '</ul>';

    return $social_links;
}

function shortcodeSocialLinks($atts)
{
    $args = shortcode_atts(
        array('location' => 'main'),
        $atts
    );

    return displaySocialProfiles($args['location']);
}
add_shortcode('company_social', 'shortcodeSocialLinks');

function shortcodeSocialShare()
{
    return displaySocialShare();
}
add_shortcode('social_share', 'shortcodeSocialShare');
