<?php
function hoverBox($args = array())
{
    $defaults = array(
        'title'        => array(
            'tag'   => 'h4',
            'class' => array(
                'hover-box-title'
            ),
        ),
        'subtitle'     => array(
            'tag'   => 'p',
            'class' => array(
                'hover-box-text'
            ),
        ),
        'icon'         => '',
        'link'     => array(
            'tag'    => 'a',
            'class'  => array(
                'hover-box-link',
                'stretched-link'
            ),
        ),
        'image'        => array(
            'size'  => 'medium',
            'wrapper' => array(
                'class' => array(
                    'hover-box-image',
                ),
            ),
        ),
        'hover_effect' => 'effect-sadie',
        'atts'         => array(
            'tag'   => 'figure',
            'class' => array(
                'hover-box'
            ),
            'id'    => '',
        )
    );
    $options = parseArgs($defaults, $args);
    extract($options);

    $image_id = $image_size = $title_text = $subtitle_text = $title_html = $subtitle_html = '';

    if (isset($title['content']) && !empty($title['content'])) {
        $title_text = __($title['content'], THEME_SLUG);
        $image['alt'] = __($title['content'], THEME_SLUG);
        unset($title['content']);
    }

    if (isset($subtitle['content']) && !empty($subtitle['content'])) {
        $subtitle_text = $subtitle['content'];
        unset($subtitle['content']);
    }

    if (isset($image['id']) && !empty($image['id'])) {
        $image_id = $image['id'];
        unset($image['id']);
    }

    if (isset($image['size']) && !empty($image['size'])) {
        $image_size = $image['size'];
        unset($image['size']);
    }

    if (!empty($icon)) {
        $title_html .= iconFont($icon . ' hover-box-icon');
    }

    if (!empty($title_text)) {
        $title_text = itemWrapperHTML($title_text, array('tag' => 'span', 'class' => array('hover-box-title-text')));
        $title_html = itemWrapperHTML($title_html . $title_text, $title);
    }

    if (!empty($subtitle_text)) {
        $subtitle_html = itemWrapperHTML($subtitle_text, $subtitle);
    }

    if (!empty($image_id)) {
        $image = displayImage($image_id, $image_size, $image, true);
    }

    if (empty($link['title']) && isset($title_text)) {
        $link['title'] = $title_text;
    }

    if (!empty($link['href'])) {
        $url = itemWrapperHTML('<span class="sr-only">' . $title_text . '</span>', $link);
    }

    $atts['class'][] = $hover_effect;

    $content = itemWrapperHTML($title_html . $subtitle_html, array('class' => array('hover-box-content')));
    $content = itemWrapperHTML($content, array('class' => array('hover-box-content-wrapper-inner')));
    $content = itemWrapperHTML($content . $url, array('class' => array('hover-box-content-wrapper')));
    $hover_box = bootpressItem($image . $content, $atts);

    return $hover_box;
}
