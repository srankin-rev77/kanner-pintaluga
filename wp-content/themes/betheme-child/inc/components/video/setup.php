<?php

/** ============================================================================
 * Video Component Setup
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/12/19
 * Modified: 01/10/20 by SR
 * ========================================================================== */


/**
 * Function to build a youtube video embed with lazyloading
 */

function youtubeVideo($id = '', $class = '')
{
    if (empty($id)) {
        return;
    }

    if (is_int($id) || is_numeric($id)) {
        $api_url = 'https://www.googleapis.com/youtube/v3/videos?part=snippet&id=' . $id . '&key=' . YOUTUBE_KEY;
        $url = 'https://youtu.be/' . $id;
    } elseif (filter_var($id, FILTER_VALIDATE_URL) == true) {
        $url = $id;
        $id = parse_url($url);
        $id = $id['path'];
        $id = str_replace('/', '', $id);
        $api_url = 'https://www.googleapis.com/youtube/v3/videos?part=snippet&id=' . $id . '&key=' . YOUTUBE_KEY;
    }

    $json = json_decode(file_get_contents($api_url), true);

    $video = $json['items'][0]['snippet'];

    if (empty($video))  return;

    $thumbnails = $video['thumbnails'];
    $lrg_size   = end($thumbnails);

    $args = array(
        'host'  => 'youtube',
        'url'   => $url,
        'width' => $lrg_size['width'],
        'height' => $lrg_size['height'],
        'thumb' => $lrg_size['url'],
        'wrapper' => array(
            'class'     => $class,
        ),
        'caption'   => array(
            'content' => $video['description']
        ),
        'title' => array(
            'content' => $video['title']
        ),
        'desc'  => array(
            'content' => $video['description']
        ),
        'date'  => $video['publishedAt'],
    );

    return displayVideo($id, $args);
}

/**
 * Function to build a vimeo video embed with lazyloading
 */
function vimeoVideo($id = '', $class = '')
{
    if (empty($id)) {
        return;
    }

    if (is_int($id) || is_numeric($id)) {
        $api_url = 'https://vimeo.com/api/oembed.json?url=' . esc_url('https://vimeo.com/') . $id;
        $url = 'https://vimeo.com/' . $id;
    } elseif (filter_var($id, FILTER_VALIDATE_URL) == true) {
        $api_url = 'https://vimeo.com/api/oembed.json?url=' . esc_url($id);
        $url = $id;
        $id = parse_url($url);
        $id = $id['path'];
        $id = str_replace('/', '', $id);
    }

    $video = json_decode(file_get_contents($api_url), true);

    if (empty($video))  return;

    $args = array(
        'host'  => 'vimeo',
        'url'   => $url,
        'width' => $video['width'],
        'height' => $video['height'],
        'thumb' => $video['thumbnail_url'],
        'wrapper' => array(
            'class'     => $class,
        ),
        'caption'   => array(
            'content' => $video['description']
        ),
        'title' => array(
            'content' => $video['title']
        ),
        'desc'  => array(
            'content' => $video['description']
        ),
        'date'  => $video['upload_date'],
    );

    return displayVideo($id, $args);
}
