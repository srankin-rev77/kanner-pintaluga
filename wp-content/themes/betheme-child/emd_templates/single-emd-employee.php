<?php $real_post = $post;
$ent_attrs = get_option('employee_spotlight_attr_list');
$sidebar = '';
$id = get_the_ID();

$person_info = '';
$contact = '';
$contact_info = '';

$icon_atts = array(
	'list' => 1
);

if (get_post_meta($id, 'emd_employee_email')) {
	$email = get_post_meta($id, 'emd_employee_email', true);
	$icon_atts['class'] = 'fas fa-envelope';
	$contact_info .= formatEmailSchema($email, array('icon' => $icon_atts, 'wrapper' => array('tag' => 'li')));
}

if (get_post_meta($id, 'emd_employee_phone')) {
	$phone = get_post_meta($id, 'emd_employee_phone', true);
	$icon_atts['class'] = 'fas fa-phone-alt';
	$contact_info .= formatPhoneSchema($phone, array('icon' => $icon_atts, 'wrapper' => array('tag' => 'li')));
}

if (get_post_meta($id, 'emd_employee_mobile')) {
	$mobile = get_post_meta($id, 'emd_employee_mobile', true);
	$icon_atts['class'] = 'fas fa-mobile';
	$contact_info .= formatPhoneSchema($mobile, array('icon' => $icon_atts, 'wrapper' => array('tag' => 'li')));
}

if (get_post_meta($id, 'emd_employee_primary_address')) {
	$address = get_post_meta($id, 'emd_employee_primary_address', true);
	$icon_atts['class'] = 'fas fa-map-marker';
	$contact_info .= formatAddressSchema($address, array('icon' => $icon_atts, 'wrapper' => array('tag' => 'li')));
}

if (!empty($contact_info)) {
	$contact_info = itemWrapperHTML(
		$contact_info,
		array(
			'tag'   => 'ul',
			'class' => array(
				'fa-ul',
			)
		)
	);
}

$profile = array(
	'link_title' => '',
	'icon_type' => 'icon',
	'icon_class' => '',
	'link' => ''
);
$social_profiles = '';
if (get_post_meta($id, 'emd_employee_facebook')) {
	$profile['link_title'] = get_the_title() . ' Facebook';
	$profile['icon_class'] = 'fab fa-facebook-f';
	$profile['link'] = get_post_meta($id, 'emd_employee_facebook', true);
	$item = socialProfileLink($profile);
	$social_profiles .= itemWrapperHTML(
		$item,
		array(
			'tag'   => 'li',
			'class' => array(
				'social-profile',
				'list-inline-item',
				'facebook',
			)
		)
	);
}
if (get_post_meta($id, 'emd_employee_twitter')) {
	$profile['link_title'] = get_the_title() . ' Twitter';
	$profile['icon_class'] = 'fab fa-twitter';
	$profile['link'] = get_post_meta($id, 'emd_employee_twitter', true);
	$item = socialProfileLink($profile);
	$social_profiles .= itemWrapperHTML(
		$item,
		array(
			'tag'   => 'li',
			'class' => array(
				'social-profile',
				'list-inline-item',
				'twitter',
			)
		)
	);
}
if (get_post_meta($id, 'emd_employee_google')) {
	$profile['link_title'] = get_the_title() . ' Google Plus';
	$profile['icon_class'] = 'fab fa-google-plus';
	$profile['link'] = get_post_meta($id, 'emd_employee_google', true);
	$item = socialProfileLink($profile);
	$social_profiles .= itemWrapperHTML(
		$item,
		array(
			'tag'   => 'li',
			'class' => array(
				'social-profile',
				'list-inline-item',
				'google-plus',
			)
		)
	);
}
if (get_post_meta($id, 'emd_employee_linkedin')) {
	$profile['link_title'] = get_the_title() . ' LinkedIn';
	$profile['icon_class'] = 'fab fa-linkedin-in';
	$profile['link'] = get_post_meta($id, 'emd_employee_linkedin', true);
	$item = socialProfileLink($profile);
	$social_profiles .= itemWrapperHTML(
		$item,
		array(
			'tag'   => 'li',
			'class' => array(
				'social-profile',
				'list-inline-item',
				'linkedin',
			)
		)
	);
}
if (get_post_meta($id, 'emd_employee_github')) {
	$profile['link_title'] = get_the_title() . ' Github';
	$profile['icon_class'] = 'fab fa-github';
	$profile['link'] = get_post_meta($id, 'emd_employee_github', true);
	$item = socialProfileLink($profile);
	$social_profiles .= itemWrapperHTML(
		$item,
		array(
			'tag'   => 'li',
			'class' => array(
				'social-profile',
				'list-inline-item',
				'github',
			)
		)
	);
}
if (!empty($social_profiles)) {
	$contact_info .= itemWrapperHTML(
		$social_profiles,
		array(
			'tag'   => 'ul',
			'class' => array(
				'social-profiles',
				'list-inline',
				'small'
			)
		)
	);
}

if (!empty($contact_info)) {
	$title = itemWrapperHTML(iconFont('fas fa-address-card') . ' Contact Info', array(
		'tag' => 'h3',
		'class' => 'p font-weight-bold'
	));

	$sidebar .= vcItem($title . $contact_info, array('class' => 'contact-info'));
}

if (function_exists('get_field')) {
	if (have_rows('degrees')) {
		$items = '';
		// loop through the rows of data
		while (have_rows('degrees')) : the_row();

			// display a sub field value
			$item = get_sub_field('degree');

			$items .= itemWrapperHTML($item, array(
				'tag' => 'li'
			));

		endwhile;

		$items = itemWrapperHTML($items, array(
			'tag' => 'ul',
			'class' => 'list-unstyled mb-0'
		));

		$items = itemWrapperHTML(iconFont('fas fa-university') . __(' Education', THEME_SLUG), array(
			'tag' => 'h3',
			'class' => 'p font-weight-bold'
		)) .  $items;

		$sidebar .= vcItem($items, array('class' => 'education'));
	}
	if (have_rows('bar_admissions')) {
		$items = '';
		// loop through the rows of data
		while (have_rows('bar_admissions')) : the_row();

			// display a sub field value
			$item = get_sub_field('bar_admission');

			$items .= itemWrapperHTML($item, array(
				'tag' => 'li'
			));

		endwhile;

		$items = itemWrapperHTML($items, array(
			'tag' => 'ul',
			'class' => 'list-unstyled mb-0'
		));

		$items = itemWrapperHTML(iconFont('fas fa-balance-scale') . __(' Bar Admissions', THEME_SLUG), array(
			'tag' => 'h3',
			'class' => 'p font-weight-bold'
		)) .  $items;

		$sidebar .= vcItem($items, array('class' => 'bar-admissions'));
	}
	if (have_rows('memberships')) {
		$items = '';
		// loop through the rows of data
		while (have_rows('memberships')) : the_row();

			// display a sub field value
			$item = get_sub_field('membership');

			$items .= itemWrapperHTML($item, array(
				'tag' => 'li'
			));

		endwhile;

		$items = itemWrapperHTML($items, array(
			'tag' => 'ul',
			'class' => 'list-unstyled mb-0'
		));

		$items = itemWrapperHTML(iconFont('fas fa-id-card') . __(' Memberships &amp; Associations', THEME_SLUG), array(
			'tag' => 'h3',
			'class' => 'p font-weight-bold'
		)) .  $items;

		$sidebar .= vcItem($items, array('class' => 'memberships'));
	}
	// if (have_rows('associations')) {
	// 	$items = '';
	// 	// loop through the rows of data
	// 	while (have_rows('associations')) : the_row();

	// 		// display a sub field value
	// 		$item = get_sub_field('association');

	// 		$items .= itemWrapperHTML($item, array(
	// 			'tag' => 'li'
	// 		));

	// 	endwhile;

	// 	$items = itemWrapperHTML($items, array(
	// 		'tag' => 'ul',
	// 		'class' => 'list-unstyled mb-0'
	// 	));

	// 	$items = itemWrapperHTML(iconFont('fas fa-users-class') . ' Associations', array(
	// 		'tag' => 'h3',
	// 		'class' => 'p font-weight-bold'
	// 	)) .  $items;

	// 	$sidebar .= vcItem($items, array('class' => 'associations'));
	// }
	if (have_rows('areas_of_practice')) {
		$items = '';
		// loop through the rows of data
		while (have_rows('areas_of_practice')) : the_row();

			// display a sub field value
			$item = get_sub_field('area_of_practice');

			$items .= itemWrapperHTML($item, array(
				'tag' => 'li'
			));

		endwhile;

		$items = itemWrapperHTML($items, array(
			'tag' => 'ul',
			'class' => 'list-unstyled mb-0'
		));

		$items = itemWrapperHTML(iconFont('fas fa-gavel') . __(' Areas of Practice', THEME_SLUG), array(
			'tag' => 'h3',
			'class' => 'p font-weight-bold'
		)) .  $items;

		$sidebar .= vcItem($items, array('class' => 'areas-of-practice'));
	}
	if (get_field('additional_info')) {
		$additional_info = get_field('additional_info');
		$additional_info = preg_replace('/\<div class\=\"([^"]+)\"\>/m', '', $additional_info);
		$additional_info = preg_replace('/\<\/div\>/m', '', $additional_info);
		$sidebar .= vcItem($additional_info, array('class' => 'additional-info'));
	}
}



if (!empty($person_info)) {
	$sidebar .= vcItem($person_info, array('class' => 'person-info'));
}

$right_sidebar = vcCol($sidebar, array(
	'column' => array(
		'tag'   => 'aside',
		'id'    => '',
		'class' => array(
			'vc_col-md-4',
			'align-self-start'
		),
	),
	'content_wrapper' => array(
		'class' => 'bg-light',
	)
));

$left_sidebar = '';
if (get_post_meta($id, 'emd_employee_photo')) {
	$img = get_post_meta($id, 'emd_employee_photo', true);
	$img = displayImage($img, 'full', array(
		'class' => 'embed-responsive-item user-img',
		'wrapper' => array(
			'class' => array(
				'embed-responsive',
				'embed-responsive-2by3'
			)
		)
	));
	$img = vcItem($img, array('class' => 'profile-img'));
	$left_sidebar = vcCol($img, array(
		'column' => array(
			'class' => array(
				'vc_col-lg-2',
				'align-self-start'
			),
		),
	));
}

$content = $left_sidebar;

$bio = get_the_content();
$bio = preg_replace('/\<div class\=\"([^"]+)\"\>/m', '', $bio);
$bio = preg_replace('/\<\/div\>/m', '', $bio);
$bio = wpautop($bio);

$content .= vcCol($bio, array(
	'column' => array(
		'class' => array(
			'vc_col-md-8',
			'vc_col-lg-6',
			'flex-fill'
		),
	)
));

$content .= $right_sidebar;

$employee = vcRow($content);
echo vcSection($employee, array('outer' => array(

	'id'    => 'employee-bio',
)));
