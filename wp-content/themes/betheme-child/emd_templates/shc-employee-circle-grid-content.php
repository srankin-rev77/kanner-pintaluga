<?php global $employee_circle_grid_count, $employee_circle_grid_filter, $employee_circle_grid_set_list;
$real_post = $post;
$ent_attrs = get_option('employee_spotlight_attr_list');
$id = get_the_ID();
$defaults = array(
	'id' => '',
	'type' => 'post',
	'name' => array(
		'add' => 0,
		'display' => 0,
		'prefix' => array(
			'display' => 1,
			'add' => 1,
			'content'  => '',
			'atts'   => array(
				'tag' => 'span',
				'class' => 'user-prefix',
				'itemprop' => 'honorificPrefix'
			)
		),
		'first'  => array(
			'display' => 1,
			'add' => 1,
			'content'  => '',
			'atts'   => array(
				'tag' => 'span',
				'class' => 'name first',
				'itemprop' => 'givenName'
			)
		),
		'last'   => array(
			'display' => 1,
			'add' => 1,
			'content'  => '',
			'atts'   => array(
				'tag' => 'span',
				'class' => 'name last',
				'itemprop' => 'familyName'
			)
		),
		'suffix' => array(
			'display' => 1,
			'add' => 1,
			'content'  => '',
			'atts'   => array(
				'tag' => 'span',
				'class' => 'user-suffix',
				'itemprop' => 'honorificSuffix'
			)
		),
		'atts'   => array(
			'tag' => 'span',
			'class' => 'user-name'
		)
	),
	'phone' => array(
		'link' => array('add' => true),
		'display' => 0,
		'add' => 0,
		'content'  => '',
		'atts'   => array(
			'tag' => 'span',
			'class' => 'user-phone',
			'itemprop' => 'telephone'
		)
	),
	'email' => array(
		'link' => array('add' => true),
		'display' => 0,
		'add' => 0,
		'content'  => '',
		'atts'   => array(
			'tag' => 'span',
			'class' => 'user-email',
			'itemprop' => 'email'
		)
	),
	'img'   => array(
		'display' => 0,
		'add' => 0,
		'content'  => '',
		'size' => 'medium',
		'atts'   => array(
			'class' => 'user-img'
		),
		'bg' => 0
	),
	'job'   => array(
		'display' => 0,
		'add' => 0,
		'content'  => '',
		'atts'   => array(
			'tag' => 'span',
			'class' => 'user-job',
			'itemprop' => 'jobTitle'
		)
	),
	'item' => array(
		'class' => array(
			'person'
		),
		'itemscope' => '',
		'itemtype'  => 'http://schema.org/Person',
	),
	'link' => array(
		'display' => 0,
		'add' => 0,
		'content' => '',
		'atts'   => array(
			'tag' => 'a',
			'class' => 'user-link stretched-link',
			'itemprop' => 'url',
			'href' => ''
		)

	)
);
$args = array(
	'id' => $id,
	'type' => 'post',
	'name' => array(
		'display' => 1,
		'add' => 1,
		'first'   => array(
			'display' => 1,
			'add' => 1,
		),
		'last'   => array(
			'display' => 1,
			'add' => 1,
		),
		'atts' => array(
			'tag' => 'h2',
			'class' => 'text-center mds-mt-1 h4'
		),
	),
	'img'   => array(
		'display' => 1,
		'atts'   => array(
			'wrapper' => array(
				'class' => array(
					'embed-responsive',
					'embed-responsive-3by4',
					'position-relative'
				)
			)
		),

		'bg' => 1
	),
	'job'   => array(
		'display' => 1,
		'wrapper' => array(
			'tag' => 'p',
			'class' => 'text-center'
		)
	),
	'link' => array(
		'display' => 1,
		'add' => 1,
	)
);
if (get_post_meta($id, 'emd_employee_photo')) {
	$sval = get_post_meta($id, 'emd_employee_photo');
	$thumb = wp_get_attachment_image_src(get_post_meta($id, 'emd_employee_photo'), 'full');
	$args['img']['content'] = $sval[0];
	$args['img']['add'] = 1;
}
if (get_post_meta($id, 'emd_employee_jobtitle')) {
	$args['job']['content'] = get_post_meta($id, 'emd_employee_jobtitle', true);
	$args['job']['add'] = 1;
}

$person = personSchema($args);
$person = vcItem($person);
$person = vcCol($person, array(
	'column' => array(
		'class' => array(
			'vc_col-ms-6',
			'vc_col-md-4',
			'vc_col-ml-3'
		)
	),
));


echo $person;
