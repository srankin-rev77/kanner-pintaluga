<?php

/** ============================================================================
 * footer
 * @package K&P Attorney
 * @version <<version>>
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created:  12-9-19
 * Modified: 12-10-19 at 6:53 pm by Sam Rankin <samrankin.dev@gmail.com>
 * =========================================================================== */

/**
 * The template for displaying the footer.
 *
 * @package Betheme
 * @author Muffin group
 * @link https://muffingroup.com
 */
$site_footer_styles = array(
	'tag'   => 'footer',
	'id'    => 'Footer',
	'role'  => 'contentinfo',
	'class' => array('site-footer', 'bg-dark')
);

$back_to_top_class = mfn_opts_get('back-top-top');

if ($back_to_top_class == 'hide') {
	$back_to_top_position = false;
} elseif (strpos($back_to_top_class, 'sticky') !== false) {
	$back_to_top_position = 'body';
} elseif (mfn_opts_get('footer-hide') == 1) {
	$back_to_top_position = 'footer';
} else {
	$back_to_top_position = 'copyright';
}

$back_to_top_atts = array(
	'tag' => 'a',
	'title' => __('Back to top', THEME_SLUG),
	'class' => array(
		'btn',
		'btn-primary',
		'button_js',
		'in_' . $back_to_top_position
	),
	'id' => 'back_to_top'
);
$back_to_top = itemWrapperHTML(iconFont('fal fa-chevron-up') . '<span class="sr-only">' . __('Back to top', THEME_SLUG) . '</span>', $back_to_top_atts);

$footer_copy = esc_html__('&copy; ' . date('Y') . ' ' . get_bloginfo('name') . '. All Rights Reserved. <a target="_blank" rel="nofollow" href="https://muffingroup.com" title="Muffin Group">Muffin group</a>', THEME_SLUG);

do_action('mfn_hook_content_after');

?>

</main><!-- #main -->
</div><!-- #primary -->
</div><!-- #content -->

<?php

if ('hide' != mfn_opts_get('footer-style')) {

	ob_start();

	if ($footer_call_to_action = mfn_opts_get('footer-call-to-action')) {

		$container_atts = array(
			'container' => array(
				'id' => 'footer-cta'
			)
		);
		$footer_cta = vcItem(do_shortcode($footer_call_to_action));
		$footer_cta = vcCol($footer_cta);
		$footer_cta = vcRow($footer_cta);
		echo $footer_cta;
	}

	$sidebars_count = 0;
	for ($i = 1; $i <= 5; $i++) {
		if (is_active_sidebar('footer-area-' . $i)) {
			$sidebars_count++;
		}
	}

	if ($sidebars_count > 0) {

		$container_atts = array(
			'container' => array(
				'id' => 'footer-main'
			)
		);

		$column_class = array(
			'footer-col'
		);

		$col_content = '';
		$columns = '';
		if ($footer_layout = mfn_opts_get('footer-layout')) {

			// Theme Options

			$footer_layout 	= explode(';', $footer_layout);
			$layout = $footer_layout[$i];
			switch ($layout) {
				case 'one-second':
					$column_class[] = 'col-' . themeConfig('desktop-bp') . '-2';
					break;
				case 'one-third':
					$column_class[] = 'col-' . themeConfig('desktop-bp') . '-4';
					break;
				case 'two-third':
					$column_class[] = 'col-' . themeConfig('desktop-bp') . '-8';
					break;
				case 'one-fourth':
					$column_class[] = 'col-' . themeConfig('desktop-bp') . '-3';
					break;
				case 'one-fifth':
					$column_class[] = 'col-' . themeConfig('desktop-bp') . '-20';
					break;
				default:
					$column_class[] = 'col-12';
			}

			for ($i = 1; $i <= $footer_cols; $i++) {
				if (is_active_sidebar('footer-area-' . $i)) {
					ob_start();
					dynamic_sidebar('footer-area-' . $i);
					$col_content = ob_get_clean();
					$columns .= vcCol($col_content, array('column' => array('class' => $column_class)));
				}
			}
		} else {

			// default with equal width

			$sidebar_class = '';
			switch ($sidebars_count) {
				case 2:
					$column_class[] = 'vc_col-' . themeConfig('desktop-bp') . '-6';
					break;
				case 3:
					$column_class[] = 'vc_col-' . themeConfig('desktop-bp') . '-4';
					break;
				case 4:
					$column_class[] = 'vc_col-' . themeConfig('desktop-bp') . '-3';
					break;
				case 5:
					$column_class[] = 'vc_col-' . themeConfig('desktop-bp') . '-20';
					break;
				default:
					$column_class[] = 'col-12';
			}

			for ($i = 1; $i <= 5; $i++) {
				if (is_active_sidebar('footer-area-' . $i)) {
					ob_start();
					dynamic_sidebar('footer-area-' . $i);
					$col_content = ob_get_clean();
					$columns .= vcCol($col_content, array('column' => array('class' => $column_class)));
				}
			}
		}

		echo vcRow($columns, $container_atts);
	}
	if (mfn_opts_get('footer-hide') != 1) : $copyright = '';
		$container_atts = array(
			'container' => array(
				'id' => 'footer-copyright'
			)
		);
		if ($back_to_top_position == 'copyright') {
			$copyright = vcItem($back_to_top, array('class' => array('back-top-top')));
		}

		if (mfn_opts_get('footer-copy')) {
			$footer_copy = do_shortcode(mfn_opts_get('footer-copy'));
			$footer_copy = str_replace('{Year}', date('Y'), $footer_copy);
		}
		$footer_copy = itemWrapperHTML($footer_copy, array('tag' => 'p', 'class' => array('text-center')));
		$footer_copy = vcItem($footer_copy, array('class' => array('copyright')));

		$copyright = vcCol($footer_copy);
		echo vcRow($copyright, $container_atts);

	endif;

	if ($back_to_top_position == 'footer') {
		echo $back_to_top;
	}
	$footer = ob_get_clean();
	echo vcSection($footer, array('outer' => $site_footer_styles));
}

if (mfn_opts_get('responsive-mobile-menu')) {
	get_template_part('includes/header', 'side-slide');
}
if ($back_to_top_position == 'body') {
	echo $back_to_top;
}

if (mfn_opts_get('popup-contact-form')) : ?>
<div id="popup_contact">
    <a class="button button_js" title="<?php esc_attr_e('Show Contact Form'); ?>" href="#"><i aria-hidden="true" class="<?php echo esc_attr(mfn_opts_get('popup-contact-form-icon', 'icon-mail-line')); ?>"></i></a>
    <div class="popup_contact_wrapper">
        <?php echo do_shortcode(mfn_opts_get('popup-contact-form')); ?>
        <span aria-hidden="true" class="arrow"></span>
    </div>
</div>
<?php endif; ?>

</div><!-- #wrapper -->
<?php do_action('mfn_hook_bottom'); ?>
<!-- W3TC-include-css -->
<!-- W3TC-include-js-head -->
<?php wp_footer(); ?>

</body>

</html>
