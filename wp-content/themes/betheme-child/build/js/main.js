/** ============================================================================
 * Main Theme JS
 * Project: K&P Attorney
 * Version: 1.0.0
 * Author:  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/12/19
 * Modified: 01/09/20 by SR
 * ========================================================================== */

const desktopBP = 1280;
const windowWidth = verge.viewportW();
const windowHeight = verge.viewportH();

jQuery(function($) {
    $.fn.extend({
        sameHeight: function(options) {
            const settings = $.extend(
                    {
                        breakpoint: desktopBP
                    },
                    options
                ),
                elem = $(this);
            let elementHeights = elem
                    .map(function() {
                        return elem.outerHeight();
                    })
                    .get(),
                minHeight = Math.max.apply(null, elementHeights);

            if (windowWidth > settings.breakpoint) {
                elem.css('min-height', minHeight);
            }
            $(window).resize(function() {
                let heights = elem
                        .map(function() {
                            return elem.outerHeight();
                        })
                        .get(),
                    min = Math.max.apply(null, heights);
                if (windowWidth > settings.breakpoint) {
                    elem.css('min-height', min);
                } else {
                    elem.css('min-height', '0px');
                }
            });
        },
        makeFullHeight: function() {
            $(this).css('min-height', windowHeight);
            $(window).on('resize', function() {
                $(this).css('min-height', windowHeight);
            });
        },
        sameWidth: function(options) {
            let settings = $.extend(
                    {
                        breakpoint: desktopBP
                    },
                    options
                ),
                elem = $(this),
                elementWidths = elem
                    .map(function() {
                        return elem.outerWidth();
                    })
                    .get(),
                minWidth = Math.max.apply(null, elementWidths);

            if (windowWidth > settings.breakpoint) {
                elem.css('min-width', minWidth);
            }
            $(window).resize(function() {
                let width = elem
                        .map(function() {
                            return elem.outerWidth();
                        })
                        .get(),
                    min = Math.max.apply(null, width);
                if (windowWidth > settings.breakpoint) {
                    elem.css('min-width', min);
                } else {
                    elem.css('min-width', '0px');
                }
            });
        },
        toTitleCase: function() {
            return $(this).each(function() {
                let ignore = 'and,the,in,with,an,or,at,of,a,to,for'.split(',');
                let theTitle = $(this).text();
                let split = theTitle.split(' ');

                for (let x = 0; x < split.length; x++) {
                    if (x > 0) {
                        if (ignore.indexOf(split[x].toLowerCase()) < 0) {
                            split[x] = split[x].replace(/\w\S*/g, function(
                                txt
                            ) {
                                return (
                                    txt.charAt(0).toUpperCase() +
                                    txt.substr(1).toLowerCase()
                                );
                            });
                        }
                    } else {
                        split[x] = split[x].replace(/\w\S*/g, function(txt) {
                            return (
                                txt.charAt(0).toUpperCase() +
                                txt.substr(1).toLowerCase()
                            );
                        });
                    }
                }
                let title = split.join(' ');
                $(this).text(title);
            });
        }
    });

    function themeJS() {
        $('.checkbox').each(function() {
            var check = $(this).find('input[type="checkbox"]');
            $(this)
                .addClass('custom-control')
                .addClass('custom-checkbox')
                .prepend(check);
            $(this)
                .children('input')
                .addClass('custom-control-input');
            $(this)
                .children('label')
                .addClass('custom-control-label');
        });
        $('.radio').each(function() {
            var radio = $(this).find('input[type="radio"]');
            $(this)
                .addClass('custom-control')
                .addClass('custom-radio')
                .prepend(radio);
            $(this)
                .children('input')
                .addClass('custom-control-input');
            $(this)
                .children('label')
                .addClass('custom-control-label');
        });
        $('.modal').each(function() {
            $(this).appendTo('body');
        });
        $('.custom-control-label').each(function() {
            $(this)
                .wrapInner('<span class="custom-control-label-text"></span>')
                .prepend('<span class="custom-control-icon"></span>');
        });
        $('.section-full-height').makeFullHeight();
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover({
            trigger: 'focus'
        });
        bsCustomFileInput.init();
        $('.full-height-section').makeFullHeight();
        $('.video-wrapper').each(function() {
            var pb = $(this).attr('aspectratio');
            console.log(pb);
            if (typeof pb !== typeof undefined && pb !== false) {
                $(this).css('padding-bottom', pb);
            }
        });
    }
    $(document).ready(function() {
        themeJS();
    });
    window.addEventListener(
        'load',
        function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener(
                    'submit',
                    function(event) {
                        if (form.checkValidity() == false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    },
                    false
                );
            });
        },
        false
    );
});

function logElementEvent(eventName, element) {
    console.log(Date.now(), eventName, element.getAttribute('data-src'));
}
var callback_enter = function(element) {
    logElementEvent('🔑 ENTERED', element);
};
var callback_exit = function(element) {
    logElementEvent('🚪 EXITED', element);
};
var callback_reveal = function(element) {
    logElementEvent('👁️ REVEALED', element);
};
var callback_loaded = function(element) {
    logElementEvent('👍 LOADED', element);
};
var callback_error = function(element) {
    logElementEvent('💀 ERROR', element);
    element.src = 'https://via.placeholder.com/440x560/?text=Error+Placeholder';
};
var callback_finish = function() {
    logElementEvent('✔️ FINISHED', document.documentElement);
};

window.lazyLoadOptions = {
    elements_selector: '.lazyload',
    threshold: 0,
    // Assign the callbacks defined above
    callback_enter: callback_enter,
    callback_exit: callback_exit,
    callback_reveal: callback_reveal,
    callback_loaded: callback_loaded,
    callback_error: callback_error,
    callback_finish: callback_finish,
    use_native: true
};
window.addEventListener(
    'LazyLoad::Initialized',
    function(event) {
        window.lazyLoadInstance = event.detail.instance;
    },
    false
);
