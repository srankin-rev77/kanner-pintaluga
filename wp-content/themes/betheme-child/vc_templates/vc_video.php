<?php
if (!defined('ABSPATH')) {
	die('-1');
}

/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * @var $link
 * @var $el_class
 * @var $el_id
 * @var $css
 * @var $css_animation
 * @var $el_width
 * @var $el_aspect
 * @var $align
 * Shortcode class
 * @var $this WPBakeryShortCode_VC_Video
 */

$atts = vc_map_get_attributes($this->getShortcode(), $atts);

extract($atts);

if ('' === $link) {
	return null;
}

$elem = $this->settings['base'];

$item_args = array();

$el_class = $this->getExtraClass($el_class);

$item_classes = array(
	toKebabCase($elem),
	$this->getCSSAnimation($css_animation)
);

$item_classes = outputClasses($item_classes, $el_class);

/** @var WP_Embed $wp_embed */
global $wp_embed;
$embed = '';
$host = parse_url($link);
$host = $host['host'];

if ($host === 'youtube.com') {
	$embed = youtubeVideo($link);
} elseif ($host === 'vimeo.com') {
	$embed = vimeoVideo($link);
} elseif (is_object($wp_embed)) {
	$embed = $wp_embed->run_shortcode('[embed]' . $link . '[/embed]');
} else {
	$embed = displayVideo($link);
}

$item_classes = vcClasses($item_classes, $elem, $atts);
$item_args['class'] = outputClasses($item_classes);

echo bootpressItem($embed, $item_args);
