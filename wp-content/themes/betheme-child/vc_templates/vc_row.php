<?php
if (!defined('ABSPATH')) {
	die('-1');
}

/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $full_width
 * @var $full_height
 * @var $equal_height
 * @var $columns_placement
 * @var $content_placement
 * @var $parallax
 * @var $parallax_image
 * @var $css
 * @var $el_id
 * @var $video_bg
 * @var $video_bg_url
 * @var $video_bg_parallax
 * @var $parallax_speed_bg
 * @var $parallax_speed_video
 * @var $content - shortcode content
 * @var $css_animation
 * Shortcode class
 * @var WPBakeryShortCode_Vc_Row $this
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$elem = $this->settings['base'];
$bg_img_id = '';
$img_attributes = $outer_attributes = array();

$outer_classes = array(
	'container',
	$el_class
);

$inner_classes = array(
	'vc_row',
	'wpb_row',
	// deprecated
	'vc_row-fluid',
	vcCustomClass($css),
);

if (!empty($inner_class)) {
	$inner_classes = outputClasses($inner_classes, $inner_class);
}

if ('yes' === $disable_element) {
	if (vc_is_page_editable()) {
		$outer_classes[] = 'd-none';
	} else {
		return '';
	}
}

if (
	vc_shortcode_custom_css_has_property($css, array(
		'background',
		'background-image',
	)) || $video_bg || $parallax
) {
	$outer_classes[] = 'bg-image';
	$bg_img_id = vcGetBGImageID($css);
}

// if (!empty($atts['gap'])) {
// 	$inner_classes[] = 'vc_column-gap-' . $atts['gap'];
// }

if (!empty($atts['rtl_reverse'])) {
	$inner_classes[] = 'flex-row-reverse';
}

// build attributes for wrapper
if (!empty($el_id)) {
	$outer_attributes['id'] = $el_id;
}
if (!empty($full_width)) {
	$outer_attributes['data-vc-full-width'] = 'true';
	$outer_attributes['data-vc-full-width-init'] = 'false';
	$outer_classes[] = 'full-width';
	$outer_classes[] = 'container-fluid';
	if ('stretch_row_content' === $full_width) {
	} elseif ('stretch_row_content_no_spaces' === $full_width) {
		$outer_classes[] = 'no-gutters';
		$inner_classes[] = 'no-gutters';
	}
}

if (!empty($full_height)) {
	$inner_classes[] = 'full-height';
	if (!empty($columns_placement)) {
		$flex_row = true;
		$inner_classes[] = 'vc_row-o-columns-' . $columns_placement;
		if ('stretch' === $columns_placement) {
			$inner_classes[] = 'align-items-stretch';
		}
	}
}

if (!empty($equal_height)) {
	$flex_row = true;
	$inner_classes[] = 'vc_row-o-equal-height';
}

if (!empty($content_placement)) {
	$flex_row = true;
	$inner_classes[] = 'vc_row-o-content-' . $content_placement;
}

if (!empty($flex_row)) {
	$inner_classes[] = 'vc_row-flex';
}

$has_video_bg = (!empty($video_bg) && !empty($video_bg_url) && vc_extract_youtube_id($video_bg_url));

$parallax_speed = $parallax_speed_bg;
if ($has_video_bg) {
	$parallax = $video_bg_parallax;
	$parallax_speed = $parallax_speed_video;
	$parallax_image = $video_bg_url;
	$inner_classes[] = 'vc_video-bg-outer';
	wp_enqueue_script('vc_youtube_iframe_api_js');
}

if (!empty($parallax)) {
	wp_enqueue_script('vc_jquery_skrollr_js');
	$outer_attributes['data-vc-parallax'] = $parallax_speed; // parallax speed
	$outer_classes[] = 'vc_general';
	$outer_classes[] = 'vc_parallax';
	$outer_classes[] = 'vc_parallax-' . $parallax;
	if (false !== strpos($parallax, 'fade')) {
		$outer_classes[] = 'js-vc_parallax-o-fade';
		$outer_attributes['data-vc-parallax-o-fade'] = 'on';
	} elseif (false !== strpos($parallax, 'fixed')) {
		$outer_classes[]  = 'js-vc_parallax-o-fixed';
	}
}

if (!empty($parallax_image)) {
	if ($has_video_bg) {
		$parallax_image_src = $parallax_image;
	} else {
		$parallax_image_id = preg_replace('/[^\d]/', '', $parallax_image);
		$parallax_image_src = wp_get_attachment_image_src($parallax_image_id, 'full');
		$bg_img_id = $parallax_image_id;
		if (!empty($parallax_image_src[0])) {
			$parallax_image_src = $parallax_image_src[0];
		}
	}
	$outer_attributes['data-vc-parallax-image'] = $parallax_image_src;
}
if (!$parallax && $has_video_bg) {
	$outer_attributes['data-vc-video-bg'] = $video_bg_url;
}
$outer_classes = vcClasses($outer_classes, $elem, $atts);
$outer_attributes['class'] = outputClasses($outer_classes);
$inner_attributes['class'] = outputClasses($inner_classes);
$content = wpb_js_remove_wpautop($content);
$output = vcRow(
	$content,
	array(
		'container' => $outer_attributes,
		'row' => $inner_attributes,
		'bg_img' => array(
			'id' => $bg_img_id,
			'attr' => $img_attributes
		)
	)
);
echo $output;
