<?php

/** ============================================================================
 * vc_single_image
 * @package K&P Attorney
 * @version 1.0.0
 * @author  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/16/19
 * Modified: 12/20/19 by SR
 * ========================================================================== */

if (!defined('ABSPATH')) {
	die('-1');
}

/**
 * Shortcode attsibutes
 * @var $atts
 * @var $title
 * @var $source
 * @var $image
 * @var $custom_src
 * @var $onclick
 * @var $img_size
 * @var $external_img_size
 * @var $caption
 * @var $img_link_large
 * @var $link
 * @var $img_link_target
 * @var $alignment
 * @var $el_class
 * @var $el_id
 * @var $css_animation
 * @var $style
 * @var $external_style
 * @var $border_color
 * @var $css
 * Shortcode class
 * @var WPBakeryShortCode_Vc_Single_image $this
 */

$atts = vc_map_get_attributes($this->getShortcode(), $atts);

extract($atts);

$elem = $this->settings['base'];

$img_atts = array(
	'data-parent-fit' => 'cover'
);

$default_src = vc_asset_url('vc/no_image.png');

$wrapper_classes = array(
	$elem,
	'wpb_content_element',
	vc_shortcode_custom_css_class($css)
);

$img_atts = $wrapper_atts = $img_wrapper = $link_atts = array();

$el_class = $this->getExtraClass($el_class);

if (!empty($el_class)) {
	$wrapper_classes = outputClasses($wrapper_classes, $el_class);
}

$css_animation = $this->getCSSAnimation($css_animation);

if (!empty($css_animation)) {
	$wrapper_classes = outputClasses($wrapper_classes, $css_animation);
}

if (!empty($el_id)) {
	$wrapper_atts['id'] = $el_id;
}

if ('external_link' === $source) {
	$style = $external_style;
	$border_color = $external_border_color;
}

$wrapper_classes[] = $style;
$wrapper_classes[] = $border_color;

$vc_classes = vcClasses($wrapper_classes, $elem, $atts);
$wrapper_classes = outputClasses($wrapper_classes, $vc_classes);
$wrapper_atts['class'] = $wrapper_classes;

// ========================================================================== //
// ============================== Image Source ============================== //
// ========================================================================== //

$img = false;
$bg = false;
if ($is_bg === 'yes') {
	$img_wrapper['class'][] = 'embed-responsive';
	$img_wrapper['class'][] = 'embed-responsive-' . $image_ratio;
	$img_atts['class'][] = 'embed-responsive-item';
}
switch ($source) {
	case 'media_library':
	case 'featured_image':
		$img_id = $image;

		if ('featured_image' === $source) {
			$post_id = get_the_ID();
			if ($post_id && has_post_thumbnail($post_id)) {
				$img = get_post_thumbnail_id($post_id);
			} else {
				$img = 0;
			}
		} else {
			$img = $image;
		}

		// set rectangular
		if (preg_match('/_circle_2$/', $style)) {
			$img_wrapper['class'][] = 'embed-responsive';
			$img_wrapper['class'][] = 'embed-responsive-1by1';
			$img_atts['class'][] = 'embed-responsive-item';
			$img_atts['class'][] = 'rounded-circle';
		}

		if ('featured_image' === $source) {
			if (!$img && 'page' === vc_manager()->mode()) {
				return;
			}
		}

		break;

	case 'external_link':
		$dimensions = vc_extract_dimensions($external_img_size);
		$hwstring = $dimensions ? image_hwstring($dimensions[0], $dimensions[1]) : '';

		$custom_src = $custom_src ? esc_attr($custom_src) : $default_src;
		$img = $custom_src;
		break;

	default:
		$img = false;
		$img_id = false;
}

if (!$img) {
	$img = $default_src;
}

// ========================================================================== //
// =============================== Image Link =============================== //
// ========================================================================== //

if (!empty($link)) {
	$link_atts['tag'] = 'a';
	if (!empty($img_link_target)) {
		$link_atts['target'] = $img_link_target;
	}

	// backward compatibility. since 4.6
	if (empty($onclick) && isset($img_link_large) && 'yes' === $img_link_large) {
		$onclick = 'img_link_large';
	} elseif (empty($atts['onclick']) && (!isset($atts['img_link_large']) || 'yes' !== $atts['img_link_large'])) {
		$onclick = 'custom_link';
	}

	switch ($onclick) {
		case 'img_link_large':
			if ('external_link' === $source) {
				$link_atts['href'] = $custom_src;
			} else {
				$link = wp_get_attachment_image_src($image, 'full');
				$link_atts['href'] = $link[0];
			}

			break;

		case 'link_image':

			$link_atts['class'][] = 'fancybox';
			$link_atts['data-fancybox'] = '';

			// backward compatibility
			if (!vc_has_class('fancybox', $el_class) && 'external_link' === $source) {
				$link_atts['href'] = $custom_src;
			} elseif (!vc_has_class('fancybox', $el_class)) {
				$link = wp_get_attachment_image_src($image, 'full');
				$link_atts['href'] = $link[0];
			}

			break;

		case 'custom_link':
			// $link is already defined
			break;

		case 'zoom':
			wp_enqueue_script('vc_image_zoom');

			if ('external_link' === $source) {
				$large_img_src = $custom_src;
			} else {
				$large_img_src = wp_get_attachment_image_src($image, 'full');
				if ($large_img_src) {
					$large_img_src = $large_img_src[0];
				}
			}

			$img_atts['data-vc-zoom'] = $large_img_src;

			break;
	}

	$img_wrapper = parseArgs($img_wrapper, $link_atts);
}

if (in_array($source, array('media_library', 'featured_image'), true)) {
	//$img_id = apply_filters('wpml_object_id', $img_id, 'attachment');
	$post = wp_get_attachment($image, $img_size);
	$caption = $post['caption'];
} else {
	if ('external_link' === $source) {
		$add_caption = 'yes';
	}
}

if ('yes' === $add_caption && '' !== $caption) {
	$img_atts['caption']['content'] = $caption;
}

if (!empty($title)) {
	$img_atts['block_title']['content'] = $title;
}

$img_atts['wrapper'] = $img_wrapper;


$img = displayImage($img, $img_size, $img_atts);
$img = itemWrapperHTML($img, $wrapper_atts);
$output = $img;

// @codingStandardsIgnoreLine
echo $output;
