<?php
if (!defined('ABSPATH')) {
	die('-1');
}

/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $css
 * @var $el_id
 * @var $equal_height
 * @var $content_placement
 * @var $content - shortcode content
 * Shortcode class
 * @var WPBakeryShortCode_Vc_Row_Inner $this
 */
$el_class = $equal_height = $content_placement = $css = $el_id = '';
$disable_element = '';
$output = $after_output = '';
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$el_class = $this->getExtraClass($el_class);

$container_attributes = array();
$css_classes = array(
	'vc_row',
	'wpb_row',
	// deprecated
	'vc_inner',
	'vc_row-fluid',
	vc_shortcode_custom_css_class($css),
);

$container_class = array(
	'container',
	'container-fluid',
	'full-width',
	$el_class
);

if(!empty($inner_class)){
	$css_classes = outputClasses($css_classes, $inner_class);
}

if ('yes' === $disable_element) {
	if (vc_is_page_editable()) {
		$container_class = outputClasses($container_class, 'vc_hidden-lg vc_hidden-xs vc_hidden-sm vc_hidden-md d-none');
	} else {
		return '';
	}
}

if ( vc_shortcode_custom_css_has_property( $css, array(
	'border',
	'background',
) ) ) {
	$container_class[] = 'has-bg';
	$container_class[] = 'bg-image';
}

// if ( ! empty( $atts['gap'] ) ) {
// 	$css_classes[] = 'vc_column-gap-' . $atts['gap'];
// }

if (!empty($equal_height)) {
	$flex_row = true;
	$css_classes[] = 'vc_row-o-equal-height';
}

if (!empty($atts['rtl_reverse'])) {
	$css_classes[] = 'flex-row-reverse';
}

if (!empty($content_placement)) {
	$flex_row = true;
	$css_classes[] = 'vc_row-o-content-' . $content_placement;
}

if (!empty($flex_row)) {
	$css_classes[] = 'vc_row-flex';
}

$wrapper_attributes = array();
// build attributes for wrapper
if (!empty($el_id)) {
	$container_attributes['id'] = $el_id;
}

$container_class = vcClasses($container_class, 'vc_row', $atts);
$wrapper_attributes['class'] = outputClasses($css_classes);
$container_attributes['class'] = outputClasses($container_class);
$output .= '<div' . outputHTMLData($container_attributes) . '>';
$output .= '<div' . outputHTMLData($wrapper_attributes) . '>';
$output .= wpb_js_remove_wpautop($content);
$output .= '</div>';
$output .= '</div>';
//$output .= $after_output;

return $output;
