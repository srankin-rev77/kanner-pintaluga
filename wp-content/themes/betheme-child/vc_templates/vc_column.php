<?php
if (!defined('ABSPATH')) {
	die('-1');
}

/**
 * Shortcode attributes
 * @var $atts
 * @var $el_id
 * @var $el_class
 * @var $width
 * @var $css
 * @var $offset
 * @var $content - shortcode content
 * @var $css_animation
 * Shortcode class
 * @var WPBakeryShortCode_Vc_Column $this
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$elem = $this->settings['base'];
$bg_img_id = '';
$img_attributes = $outer_attributes = array();
$width = vcColWidthToSpan($width);
$width = vc_column_offset_class_merge($offset, $width);

$outer_classes = array(
	$elem,
	'wpb_column',
	'vc_column_container',
	$width,
);

if (!empty($el_class)) {
	$el_class = $this->getExtraClass($el_class);
	$outer_classes = outputClasses($outer_classes, $el_class);
}
if (!empty($css_animation)) {
	$css_animation = $this->getCSSAnimation($css_animation);
	$outer_classes = outputClasses($outer_classes, $css_animation);
}

$inner_attributes = array();
$inner_classes = array(
	'vc_column-inner',
	vcCustomClass($css)
);

if (!empty($inner_class)) {
	$inner_classes = outputClasses($inner_classes, $inner_class);
}

if (
	vc_shortcode_custom_css_has_property($css, array(
		'background',
		'background-image',
	)) || $video_bg || $parallax
) {
	$outer_classes[] = 'bg-image';
	$bg_img_id = vcGetBGImageID($css);
}

if (!empty($el_id)) {
	$outer_attributes['id'] = $el_id;
}
$has_video_bg = (!empty($video_bg) && !empty($video_bg_url) && vc_extract_youtube_id($video_bg_url));

$parallax_speed = $parallax_speed_bg;
if ($has_video_bg) {
	$parallax = $video_bg_parallax;
	$parallax_speed = $parallax_speed_video;
	$parallax_image = $video_bg_url;
	$outer_classes[] = 'vc_video-bg-outer';
	wp_enqueue_script('vc_youtube_iframe_api_js');
}

if (!empty($parallax)) {
	wp_enqueue_script('vc_jquery_skrollr_js');
	$outer_attributes['data-vc-parallax'] = $parallax_speed; // parallax speed
	$outer_classes[] = 'vc_general';
	$outer_classes[] = 'vc_parallax';
	$outer_classes[] = 'vc_parallax-' . $parallax;
	if (false !== strpos($parallax, 'fade')) {
		$outer_classes[] = 'js-vc_parallax-o-fade';
		$outer_attributes['data-vc-parallax-o-fade'] = 'on';
	} elseif (false !== strpos($parallax, 'fixed')) {
		$outer_classes[]  = 'js-vc_parallax-o-fixed';
	}
}

if (!empty($parallax_image)) {
	if ($has_video_bg) {
		$parallax_image_src = $parallax_image;
	} else {
		$parallax_image_id = preg_replace('/[^\d]/', '', $parallax_image);
		$parallax_image_src = wp_get_attachment_image_src($parallax_image_id, 'full');
		$bg_img_id = $parallax_image_id;
		if (!empty($parallax_image_src[0])) {
			$parallax_image_src = $parallax_image_src[0];
		}
	}
	$outer_attributes['data-vc-parallax-image'] = $parallax_image_src;
}
if (!$parallax && $has_video_bg) {
	$outer_attributes['data-vc-video-bg'] = $video_bg_url;
}
$outer_classes = vcClasses($outer_classes, $elem, $atts);
$outer_attributes['class'] = outputClasses($outer_classes);
$inner_attributes['class'] = outputClasses($inner_classes);
$args = array(
	'column' => $outer_attributes,
	'content_wrapper' => $inner_attributes,
	'wpb_wrapper' => array(
		'class' => outputClasses($content_wrapper_class),
	),
	'bg_img' => array(
		'id' => $bg_img_id,
		'attr' => $img_attributes
	)
);

echo vcCol(wpb_js_remove_wpautop($content), $args);
