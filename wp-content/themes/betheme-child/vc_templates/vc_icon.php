<?php
if (!defined('ABSPATH')) {
	die('-1');
}
/**
 * Shortcode attributes
 * @var $atts
 * @var $type
 * @var $icon_fontawesome
 * @var $icon_openiconic
 * @var $icon_typicons
 * @var $icon_entypo
 * @var $icon_linecons
 * @var $color
 * @var $custom_color
 * @var $background_style
 * @var $background_color
 * @var $custom_background_color
 * @var $size
 * @var $align
 * @var $el_class
 * @var $el_id
 * @var $link
 * @var $css_animation
 * @var $css
 * Shortcode class
 * @var WPBakeryShortCode_Vc_Icon $this
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);

extract($atts);

$elem = $this->settings['base'];

$el_class = $this->getExtraClass($el_class);

$wrapper_atts = $icons_atts = $link_atts = array();

$wrapper_classes = array(
	'vc_icon_element',
	'vc_icon_element-outer',
	'vc_icon_element-align-' . esc_attr($align)
);

$inner_classes = array(
	'vc_icon_element-inner',
);

$vc_inner_atts = array(
	'vc_icon_element-color-' => $color,
	'vc_icon_element-size-' => $size,
	'vc_icon_element-style-' => $background_style,
	'vc_icon_element-background-color-' => $background_color
);

foreach ($vc_inner_atts as $key => $value) {
	if (!empty($value)) {
		$inner_classes[] = $key . esc_attr($value);
	}
}

$icon_classes = array(
	'vc_icon_element-icon'
);

$icon = isset(${'icon_' . $type}) ? esc_attr(${'icon_' . $type}) : 'fa fa-adjust';
$icon = outputClasses($icon_classes, $icon);
$icon[] = 'text-' . $align;
$icon[] = 'd-block';



if (!empty($el_class)) {
	$wrapper_classes = outputClasses($wrapper_classes, $el_class);
}

$css_animation = $this->getCSSAnimation($css_animation);

if (!empty($css_animation)) {
	$wrapper_classes = outputClasses($wrapper_classes, $css_animation);
}

if (!empty($el_id)) {
	$wrapper_atts['id'] = $el_id;
}

// Enqueue needed icon font.
vc_icon_element_fonts_enqueue($type);

$url = vc_build_link($link);
$has_style = false;
if (strlen($background_style) > 0) {
	$has_style = true;
	if (false !== strpos($background_style, 'outline')) {
		$wrapper_classes[] = 'vc_icon_element-outline'; // if we use outline style it is border in css
	} else {
		$wrapper_classes[] = 'vc_icon_element-background';
	}
}

if ('custom' === $background_color) {
	if (false !== strpos($background_style, 'outline')) {
		$wrapper_atts['style']['border-color'] = $custom_background_color;
	} else {
		$wrapper_atts['style']['background-color'] = $custom_background_color;
	}
}
if (!empty($link)) {
	$link_atts['tag'] = 'a';
	$link     = trim($link);
	$link     = ('||' === $link) ? '' : $link;
	$link     = vc_build_link($link);
	if (strlen($link['url']) > 0) {
		$use_link      = true;
		$link['href']  = apply_filters('vc_btn_a_href', $link['url']);
		$link['title'] = apply_filters('vc_btn_a_title', $link['title']);
		unset($link['url']);
		$link_atts = array_merge($link_atts, $link);
	}
}

if ('custom' === $color) {
	$icons_atts['style']['color'] = esc_attr($custom_color) . ' !important';
}

$wrapper_vc_classes = vcClasses($wrapper_classes, $elem, $atts);
$wrapper_classes = outputClasses($wrapper_classes, $wrapper_vc_classes);
$wrapper_atts['class'] = $wrapper_classes;

$inner_vc_classes = vcClasses($inner_classes, $elem, $atts);
$inner_classes = outputClasses($inner_classes, $inner_vc_classes);
$inner_atts['class'] = $inner_classes;

if ($has_style) {
	$wrapper_classes[] = 'vc_icon_element-have-style';
	$inner_classes[] = 'vc_icon_element-have-style-inner';
}

$icon = iconFont($icon, $icons_atts);

if (strlen($link) > 0 && strlen($url['url']) > 0) {
	$icon = itemWrapperHTML($icon, $link_atts);
}
$icon = itemWrapperHTML($icon, $inner_atts);
$icon = itemWrapperHTML($icon, $wrapper_atts);
return $icon;
