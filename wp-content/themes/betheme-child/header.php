<?php

/**
 * The Header for our theme.
 *
 * @package Betheme
 * @author Muffin group
 * @link https://muffingroup.com
 */
?>
<!DOCTYPE html>
<?php
if ($_GET && key_exists('mfn-rtl', $_GET)) :
	echo '<html class="no-js" lang="ar" dir="rtl">';
else :
?>
<html <?php language_attributes(); ?> class="no-js<?php echo esc_attr(mfn_user_os()); ?>" <?php mfn_tag_schema(); ?>>
    <?php endif; ?>

    <head>
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>" charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>">
        <link rel="profile" href="https://gmpg.org/xfn/11">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <?php do_action('mfn_hook_top'); ?>
        <?php get_template_part('includes/header', 'sliding-area'); ?>

        <?php if (mfn_header_style(true) == 'header-creative') { ?>
        <?php get_template_part('includes/header', 'creative'); ?>
        <?php } ?>

        <div id="wrapper" class="site">
            <?php getComponent('header'); ?>
            <?php do_action('mfn_hook_content_before'); ?>
            <div id="Content" class="site-content flex-fill">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main">
