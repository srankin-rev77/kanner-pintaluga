<?php

/**
 * The main template file.
 *
 * @package Betheme
 * @author Muffin group
 * @link https://muffingroup.com
 */

get_header();

// class

$blog_classes	= array();
$section_class = array();

// class | layout

if ($_GET && key_exists('mfn-b', $_GET)) {
	$blog_layout = esc_html($_GET['mfn-b']); // demo
} else {
	$blog_layout = mfn_opts_get('blog-layout', 'classic');
}
$blog_classes[] = $blog_layout;

// layout | masonry tiles

if ($blog_layout == 'masonry tiles') {
	$blog_layout = 'masonry';
}

// class | columns

if ($_GET && key_exists('mfn-bc', $_GET)) {
	$blog_classes[] = 'col-' . esc_html($_GET['mfn-bc']); // demo
} else {
	$blog_classes[] = 'col-' . mfn_opts_get('blog-columns', 3);
}

// full width
if ($_GET && key_exists('mfn-bfw', $_GET)) {
	$section_class[] = 'full-width'; // demo
}
if (mfn_opts_get('blog-full-width') && ($blog_layout == 'masonry')) {
	$section_class[] = 'full-width';
}

$section_class = implode(' ', $section_class);

// isotope

if ($_GET && key_exists('mfn-iso', $_GET)) {
	$isotope = true;
} elseif (mfn_opts_get('blog-isotope')) {
	$isotope = true;
} else {
	$isotope = false;
}

if ($isotope || ($blog_layout == 'masonry')) {
	$blog_classes[] = 'isotope';
}

// load more

$load_more = mfn_opts_get('blog-load-more');

// translate

$translate['filter'] = mfn_opts_get('translate') ? mfn_opts_get('translate-filter', 'Filter by') : __('Filter by', 'betheme');
$translate['tags'] = mfn_opts_get('translate') ? mfn_opts_get('translate-tags', 'Tags') : __('Tags', 'betheme');
$translate['authors'] = mfn_opts_get('translate') ? mfn_opts_get('translate-authors', 'Authors') : __('Authors', 'betheme');
$translate['all'] = mfn_opts_get('translate') ? mfn_opts_get('translate-all', 'Show all') : __('Show all', 'betheme');
$translate['categories'] = mfn_opts_get('translate') ? mfn_opts_get('translate-categories', 'Categories') : __('Categories', 'betheme');
$translate['item-all'] = mfn_opts_get('translate') ? mfn_opts_get('translate-item-all', 'All') : __('All', 'betheme');
?>

<div id="Content">
    <div class="content_wrapper clearfix">

        <div class="sections_group">

            <div class="extra_content">
                <?php
				if (get_option('page_for_posts') || mfn_opts_get('blog-page')) {

					if (category_description()) {

						echo '<div class="section the_content category_description">';
						echo '<div class="section_wrapper">';
						echo '<div class="the_content_wrapper">';
						echo category_description();
						echo '</div>';
						echo '</div>';
						echo '</div>';
					} else {

						$mfn_builder = new Mfn_Builder_Front(mfn_ID(), true);
						$mfn_builder->show();
					}
				}
				?>
            </div>

            <?php if (($filters = mfn_opts_get('blog-filters')) && (is_home() || is_category() || is_tag() || is_author())) : ?>

            <div class="section section-filters">
                <div class="container">

                    <?php
						$filters_class = '';

						if ($isotope) {
							$filters_class .= ' isotope-filters';
						}

						if ($filters != 1) {
							$filters_class .= ' only ' . $filters;
						}


						?>

                    <!-- #Filters -->
                    <div id="Filters" class="row <?php echo esc_attr($filters_class); ?>">

                        <?php
							$cancel_atts = array(
								'tag' => 'button',
								'type' => 'button',
								'role' => 'button',
								'class' => 'btn btn-link',
								'data-rel' => '*',
							);
							$cancel = itemWrapperHTML(iconFont('fal fa-times', 'Cancel'), $cancel_atts);
							$dropdown_btns = '';
							$label_atts = array(
								'tag' => 'span',
								'class' => 'btn btn-link',
							);
							$button_atts = array(
								'tag' => 'button',
								'type' => 'button',
								'role' => 'button',
								'class' => 'btn btn-link',
								'data-toggle' => 'collapse',
								'aria-expanded' => 'false',

							);
							$button_icon = '';
							$button_text = '';
							$button_text = esc_html($translate['filter']);
							$dropdown_btns .= itemWrapperHTML($button_text, $label_atts);
							$button_atts['data-target'] = '#filter-categories';
							$button_atts['aria-controls'] = 'filter-categories';
							$button_icon = iconFont('fas fa-folders');
							$button_text = esc_html($translate['categories']);
							$dropdown_btns .= itemWrapperHTML($button_icon . $button_text, $button_atts);
							$button_atts['data-target'] = '#filter-tags';
							$button_atts['aria-controls'] = 'filter-tags';
							$button_icon = iconFont('fas fa-tags');
							$button_text = esc_html($translate['tags']);
							$dropdown_btns .= itemWrapperHTML($button_icon . $button_text, $button_atts);
							$button_atts['data-target'] = '#filter-authors';
							$button_atts['aria-controls'] = 'filter-authors';
							$button_icon = iconFont('fas fa-user');
							$button_text = esc_html($translate['authors']);
							$dropdown_btns .= itemWrapperHTML($button_icon . $button_text, $button_atts);
							$dropdown_btns .= $cancel;
							$dropdown_btns = itemWrapperHTML($dropdown_btns);
							echo $dropdown_btns;

							$filters = '';
							$filters_wrapper = array(
								'class' => 'filters collapse',
								'data-parent' => '#filter-buttons'
							);
							$filters_atts = array(
								'class' => 'btn-group',
								'role' => 'group',
							);
							$button_atts = array(
								'tag' => 'button',
								'type' => 'button',
								'role' => 'button',
								'class' => 'btn btn-dark',
								'data-rel' => '*',
							);

							$all_items = itemWrapperHTML($translate['item-all'], $button_atts);
							$cancel = itemWrapperHTML(iconFont('fal fa-times', 'Cancel'), $button_atts);

							$cat_filters = '';
							$filters_wrapper['id'] = 'filter-categories';
							$filters_atts['aria-label'] = 'Filter Posts by Categories';
							$filters_atts['class'] = $filters_atts['class'] . ' categories';
							$cat_filters .= $all_items;
							if ($categories = get_categories()) {
								$exclude = mfn_get_excluded_categories();
								foreach ($categories as $category) {
									if ($exclude && in_array($category->slug, $exclude)) {
										continue;
									}
									$button_atts['data-rel'] = '.cat-' . esc_attr($category->slug);
									$cat_filters .= itemWrapperHTML($category->name, $button_atts);
								}
							}
							$cat_filters .= $cancel;
							$cat_filters = itemWrapperHTML($cat_filters, $filters_atts);
							$filters .= itemWrapperHTML($cat_filters, $filters_wrapper);



							$tag_filters = '';
							$filters_wrapper['id'] = 'filter-tags';
							$filters_atts['aria-label'] = 'Filter Posts by Tags';
							$filters_atts['class'] = $filters_atts['class'] . ' tags';
							$tag_filters .= $all_items;
							if ($tags = get_tags()) {
								foreach ($tags as $tag) {
									$button_atts['data-rel'] = '.tag-' . esc_attr($tag->slug);
									$tag_filters .= itemWrapperHTML($tag->name, $button_atts);
								}
							}
							$tag_filters .= $cancel;
							$tag_filters = itemWrapperHTML($tag_filters, $filters_atts);
							$filters .= itemWrapperHTML($tag_filters, $filters_wrapper);


							$filters_wrapper['id'] = 'filter-authors';
							$filters_atts['aria-label'] = 'Filter Posts by Authors';
							$filters_atts['class'] = $filters_atts['class'] . ' authors';
							$author_filters .= $all_items;
							$authors = mfn_get_authors();
							if (is_array($authors)) {
								foreach ($authors as $auth) {
									$button_atts['data-rel'] = '.author-' . esc_attr(mfn_slug($auth->data->user_login));
									$author_filters .= itemWrapperHTML($auth->data->display_name, $button_atts);
								}
							}

							$author_filters .= $cancel;
							$author_filters = itemWrapperHTML($author_filters, $filters_atts);
							$filters .= itemWrapperHTML($author_filters, $filters_wrapper);
							$filters = itemWrapperHTML($filters, array('id' => 'filter-buttons'));
							echo $filters;
							?>

                    </div>

                </div>
            </div>

            <?php endif; ?>

            <div class="section <?php echo esc_attr($section_class); ?>">
                <div class="section_wrapper clearfix">
                    <?php if (have_posts()) { ?>
                    <div class="vc_section">
                        <div class="container">
                            <div class="vc_row">
                                <div class="blog_wrapper isotope_wrapper w-100">
                                    <div class="posts_group lm_wrapper w-100 mw-100 <?php echo esc_attr(implode(' ', $blog_classes)); ?>">
                                        <?php while (have_posts()) {
												the_post();
												getComponent('blog', '/loop/' . get_post_type());
											} ?>
                                    </div>
                                </div>
                                <div class="wpb_content_element blog-pagination">
                                    <?php if (function_exists('mfn_pagination')) { ?>
                                    <?php echo mfn_pagination(false, $load_more); ?>
                                    <?php } else { ?>
                                    <div class="nav-next"><?php next_posts_link(__('&larr; Older Entries', THEME_SLUG)) ?></div>
                                    <div class="nav-previous"><?php previous_posts_link(__('Newer Entries &rarr;', THEME_SLUG)) ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>

    <?php get_footer();
