"use strict";

function _typeof(obj) {
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
        _typeof = function _typeof(obj) {
            return typeof obj;
        };
    } else {
        _typeof = function _typeof(obj) {
            return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
        };
    }
    return _typeof(obj);
}
/** ============================================================================
 * Main Theme JS
 * Project: K&P Attorney
 * Version: 1.0.0
 * Author:  Sam Rankin <samrankin.dev@gmail.com>
 * -----
 * Created : 12/12/19
 * Modified: 01/09/20 by SR
 * ========================================================================== */
var desktopBP = 1280;
var windowWidth = verge.viewportW();
var windowHeight = verge.viewportH();
jQuery(function ($) {
    $.fn.extend({
        sameHeight: function sameHeight(options) {
            var settings = $.extend({
                    breakpoint: desktopBP
                }, options),
                elem = $(this);
            var elementHeights = elem.map(function () {
                    return elem.outerHeight();
                }).get(),
                minHeight = Math.max.apply(null, elementHeights);
            if (windowWidth > settings.breakpoint) {
                elem.css('min-height', minHeight);
            }
            $(window).resize(function () {
                var heights = elem.map(function () {
                        return elem.outerHeight();
                    }).get(),
                    min = Math.max.apply(null, heights);
                if (windowWidth > settings.breakpoint) {
                    elem.css('min-height', min);
                } else {
                    elem.css('min-height', '0px');
                }
            });
        },
        makeFullHeight: function makeFullHeight() {
            $(this).css('min-height', windowHeight);
            $(window).on('resize', function () {
                $(this).css('min-height', windowHeight);
            });
        },
        sameWidth: function sameWidth(options) {
            var settings = $.extend({
                    breakpoint: desktopBP
                }, options),
                elem = $(this),
                elementWidths = elem.map(function () {
                    return elem.outerWidth();
                }).get(),
                minWidth = Math.max.apply(null, elementWidths);
            if (windowWidth > settings.breakpoint) {
                elem.css('min-width', minWidth);
            }
            $(window).resize(function () {
                var width = elem.map(function () {
                        return elem.outerWidth();
                    }).get(),
                    min = Math.max.apply(null, width);
                if (windowWidth > settings.breakpoint) {
                    elem.css('min-width', min);
                } else {
                    elem.css('min-width', '0px');
                }
            });
        },
        toTitleCase: function toTitleCase() {
            return $(this).each(function () {
                var ignore = 'and,the,in,with,an,or,at,of,a,to,for'.split(',');
                var theTitle = $(this).text();
                var split = theTitle.split(' ');
                for (var x = 0; x < split.length; x++) {
                    if (x > 0) {
                        if (ignore.indexOf(split[x].toLowerCase()) < 0) {
                            split[x] = split[x].replace(/\w\S*/g, function (txt) {
                                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                            });
                        }
                    } else {
                        split[x] = split[x].replace(/\w\S*/g, function (txt) {
                            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                        });
                    }
                }
                var title = split.join(' ');
                $(this).text(title);
            });
        }
    });
    
    function themeJS() {
        $('.checkbox').each(function () {
            var check = $(this).find('input[type="checkbox"]');
            $(this).addClass('custom-control').addClass('custom-checkbox').prepend(check);
            $(this).children('input').addClass('custom-control-input');
            $(this).children('label').addClass('custom-control-label');
        });
        $('.radio').each(function () {
            var radio = $(this).find('input[type="radio"]');
            $(this).addClass('custom-control').addClass('custom-radio').prepend(radio);
            $(this).children('input').addClass('custom-control-input');
            $(this).children('label').addClass('custom-control-label');
        });
        $('.modal').each(function () {
            $(this).appendTo('body');
        });
        $('.custom-control-label').each(function () {
            $(this).wrapInner('<span class="custom-control-label-text"></span>').prepend('<span class="custom-control-icon"></span>');
        });
        $('.section-full-height').makeFullHeight();
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover({
            trigger: 'focus'
        });
        bsCustomFileInput.init();
        $('.full-height-section').makeFullHeight();
        $('.video-wrapper').each(function () {
            var pb = $(this).attr('aspectratio');
            console.log(pb);
            if (_typeof(pb) !== (typeof undefined === "undefined" ? "undefined" : _typeof(undefined)) && pb !== false) {
                $(this).css('padding-bottom', pb);
            }
        });
    }
    $(document).ready(function () {
        themeJS();
    });
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation'); // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() == false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
});

function logElementEvent(eventName, element) {
    console.log(Date.now(), eventName, element.getAttribute('data-src'));
}
var callback_enter = function callback_enter(element) {
    logElementEvent('🔑 ENTERED', element);
};
var callback_exit = function callback_exit(element) {
    logElementEvent('🚪 EXITED', element);
};
var callback_reveal = function callback_reveal(element) {
    logElementEvent('👁️ REVEALED', element);
};
var callback_loaded = function callback_loaded(element) {
    logElementEvent('👍 LOADED', element);
};
var callback_error = function callback_error(element) {
    logElementEvent('💀 ERROR', element);
    element.src = 'https://via.placeholder.com/440x560/?text=Error+Placeholder';
};
var callback_finish = function callback_finish() {
    logElementEvent('✔️ FINISHED', document.documentElement);
};
window.lazyLoadOptions = {
    elements_selector: '.lazyload',
    threshold: 0,
    // Assign the callbacks defined above
    callback_enter: callback_enter,
    callback_exit: callback_exit,
    callback_reveal: callback_reveal,
    callback_loaded: callback_loaded,
    callback_error: callback_error,
    callback_finish: callback_finish,
    use_native: true
};
window.addEventListener('LazyLoad::Initialized', function (event) {
    window.lazyLoadInstance = event.detail.instance;
}, false);
"use strict";
jQuery(function ($) {
    $(document).ready(function () {
        $('.owl-carousel').each(function () {
            var owl = $(this);
            var optionsJSON = $(this).data('owlcarousel'),
                args = jQuery.parseJSON(JSON.stringify(optionsJSON));
            args.slideBy = 'page';
            args.dotData = true;
            args.navText = ['<i class="fal fa-angle-left" aria-hidden="true" role="presentation"></i> <span class="sr-only">Previous</span>', '<i class="fal fa-angle-right" aria-hidden="true" role="presentation"></i> <span class="sr-only">Next</span>'];
            
            function navButtons(event) {
                var element = '#' + event.target.id,
                    carousel = document.getElementById(event.target.id),
                    prevBtn = $(element).find('.owl-prev'),
                    nextBtn = $(element).find('.owl-next'),
                    carouselHeight = verge.rectangle(carousel).height;
                $(element).prepend(prevBtn);
                $(prevBtn).addClass('owl-btn').css('top', carouselHeight / 2);
                $(nextBtn).addClass('owl-btn').css('top', carouselHeight / 2).insertBefore($(element).find('.owl-dots'));
            }
            args.onInitialized = navButtons;
            owl.owlCarousel(args);
        });
    });
});
"use strict";
jQuery(function ($) {
    $.fn.extend({
        cssTransform: function cssTransform(property, value) {
            $(this).css({
                '-webkit-transform': '' + property + '(' + value + ')',
                '-moz-transform': '' + property + '(' + value + ')',
                '-ms-transform': '' + property + '(' + value + ')',
                '-o-transform': '' + property + '(' + value + ')',
                transform: '' + property + '(' + value + ')'
            });
        }
    });
    $('.hover-box.effect-lily').each(function () {
        var textHeight = $(this).find('.hover-box-text').outerHeight(true);
        $(this).find('.hover-box-title').cssTransform('translateY', textHeight + 'px');
        $(this).find('.hover-box-text').cssTransform('translateY', textHeight + 'px');
    });
});